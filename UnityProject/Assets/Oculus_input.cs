using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oculus_input : MonoBehaviour
{

    public GameObject table;
    public GameObject mobile;
    public GameObject picture;
    public GameObject tableword;
    public GameObject mobileword;
    public GameObject pictureword;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Button A pressed- Show mobile
        if (OVRInput.Get(OVRInput.Button.One))
        {
            mobile.SetActive(true);
            mobileword.SetActive(true);
        }
        // Button B pressed- Show picture
        if (OVRInput.Get(OVRInput.Touch.Two))
        {
            picture.SetActive(true);
            pictureword.SetActive(true);
        }
        // Button X pressed- Show table
        if (OVRInput.Get(OVRInput.RawButton.X))
        {
            table.SetActive(true);
            tableword.SetActive(true);
        }
        // Button Y pressed- Hide model
        if (OVRInput.Get(OVRInput.RawButton.Y))
        {
            table.SetActive(false);
            tableword.SetActive(false);
            mobile.SetActive(false);
            mobileword.SetActive(false);
            picture.SetActive(false);
            pictureword.SetActive(false);
        }
    }
}
