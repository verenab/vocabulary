using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingUp : MonoBehaviour
{

    //public GameObject applicationPanel;

    public GameObject mobilesettting;
    public GameObject framesettting;
    public GameObject tablesettting;

    public GameObject setuppanel;
    public GameObject startpanel;

    public GameObject settingBack;
    public GameObject saveBack;

    public GameObject learnBack;
    public GameObject backBack;

    public GameObject mobilebox;
    public GameObject framebox;
    public GameObject tablebox;

   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void showBouindingBox() {
        mobilesettting.SetActive(true);
        framesettting.SetActive(true);
        tablesettting.SetActive(true);
        settingBack.SetActive(true);
        saveBack.SetActive(false);
       
    }
    public void hideBouindingBox()
    {
        mobilesettting.SetActive(false);
        framesettting.SetActive(false);
        tablesettting.SetActive(false);
        setuppanel.SetActive(false);
        startpanel.SetActive(true);
        settingBack.SetActive(false);
        saveBack.SetActive(false);
    }

    public void backtoSetup()
    { 
        setuppanel.SetActive(true);
        startpanel.SetActive(false);
        learnBack.SetActive(false);
        backBack.SetActive(false);
        mobilesettting.SetActive(true);
        framesettting.SetActive(true);
        tablesettting.SetActive(true);
        settingBack.SetActive(true);
        saveBack.SetActive(false);
    }

    public void startLearning()
    {

        learnBack.SetActive(false);
        backBack.SetActive(false);
        mobilesettting.SetActive(true);
        framesettting.SetActive(true);
        tablesettting.SetActive(true);
        mobilebox.SetActive(true);
        framebox.SetActive(true);
        tablebox.SetActive(true);
        //applicationPanel.SetActive(false);
        
    }
}
