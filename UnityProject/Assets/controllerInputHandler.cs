using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class controllerInputHandler : MonoBehaviour
{

    public GameObject testObj;
    OVRInput.Controller controller = OVRInput.Controller.RTouch;
    bool colored = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //change color of testObj with grab-button
        if(OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger, controller))
        {
            if (!colored)
            {
                testObj.transform.GetComponent<Renderer>().material.color = Color.red;
                colored = true;
            }
            else
            {
                testObj.transform.GetComponent<Renderer>().material.color = Color.blue;
                colored = false;
            }
        }
    }
}
