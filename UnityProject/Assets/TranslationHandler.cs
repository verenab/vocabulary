using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TranslationHandler : MonoBehaviour
{

    Transform previousChild;
    String previousTranslationString;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    //str[] values contains object that should be translated and language
    public void ShowTranslation(string[] values)
    {

        //access object and language from utterance
        var objectString = values[0];
        var languageString = values[1];
        if (string.IsNullOrEmpty(objectString)) return;
        if (string.IsNullOrEmpty(languageString)) return;

        foreach (Transform child in transform) // iterate through all children to find object that should be translated
        {
            Debug.Log("object-child: " + child.name);
            if (child.name.IndexOf(objectString, StringComparison.OrdinalIgnoreCase) != -1) // if the name exists
            {

                previousChild = child;
                //activate visualization
                GameObject visualization = child.GetChild(0).gameObject;
                Debug.Log("vis child: " + visualization.name);
                visualization.SetActive(true);


                //activate translation
                var translationString = objectString + "_translation_" + languageString;
                child.Find(translationString).gameObject.SetActive(true);
                previousTranslationString = translationString;

                return;
            }
        }


    }

    public void HideTranslation(string[] values)
    {
        Debug.Log("hide!!!!!!!");

        //deactivate visualization
        GameObject visualization = previousChild.GetChild(0).gameObject;
        Debug.Log("vis child: " + visualization.name);
        visualization.SetActive(false);


        //deactivate translation
        previousChild.Find(previousTranslationString).gameObject.SetActive(false);

        
    }
}
