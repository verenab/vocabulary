using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scaledIn : MonoBehaviour
{

    public float finalScale;
 
    private float scaleSpeed = 0.0001f;
    private float speed;
    

    // Start is called before the first frame update
    void Start()
    {  
        

    }

    
    // Update is called once per frame
    void Update()
    {

        
            if (transform.localScale.y <= finalScale)
            {
                speed += Time.deltaTime * 1.0f;
                var newScale = Mathf.Lerp(scaleSpeed, finalScale, speed);
                
                    transform.localScale = new Vector3(transform.localScale.x, newScale, transform.localScale.z);
            }

    }
}
