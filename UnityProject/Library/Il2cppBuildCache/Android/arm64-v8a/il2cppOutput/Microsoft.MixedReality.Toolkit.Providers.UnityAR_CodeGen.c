﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.Experimental.UnityAR.UnityARCameraSettings::.ctor(Microsoft.MixedReality.Toolkit.CameraSystem.IMixedRealityCameraSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.CameraSystem.BaseCameraSettingsProfile)
extern void UnityARCameraSettings__ctor_m3A77AB31E95CE7D821B85606718E51C281B0F75F (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Experimental.UnityAR.UnityARCameraSettings::ReadProfile()
extern void UnityARCameraSettings_ReadProfile_m574C6E167BF2D6256AAC2148450CDF58D1505F41 (void);
// 0x00000003 System.Boolean Microsoft.MixedReality.Toolkit.Experimental.UnityAR.UnityARCameraSettings::get_IsOpaque()
extern void UnityARCameraSettings_get_IsOpaque_mBA0726658F038788714B6C915D5271B310623A1E (void);
// 0x00000004 Microsoft.MixedReality.Toolkit.Experimental.UnityAR.UnityARCameraSettingsProfile Microsoft.MixedReality.Toolkit.Experimental.UnityAR.UnityARCameraSettings::get_SettingsProfile()
extern void UnityARCameraSettings_get_SettingsProfile_m4D7E02868359C9D7F7CED8D082EE0A47A4638366 (void);
// 0x00000005 Microsoft.MixedReality.Toolkit.Experimental.UnityAR.ArTrackedPose Microsoft.MixedReality.Toolkit.Experimental.UnityAR.UnityARCameraSettingsProfile::get_PoseSource()
extern void UnityARCameraSettingsProfile_get_PoseSource_m9200C2FED8464F4F43C8EF3126F93CCC00F2F046 (void);
// 0x00000006 Microsoft.MixedReality.Toolkit.Experimental.UnityAR.ArTrackingType Microsoft.MixedReality.Toolkit.Experimental.UnityAR.UnityARCameraSettingsProfile::get_TrackingType()
extern void UnityARCameraSettingsProfile_get_TrackingType_m03DF33F4B4C03AFF0A0623852D8176CCEC973432 (void);
// 0x00000007 Microsoft.MixedReality.Toolkit.Experimental.UnityAR.ArUpdateType Microsoft.MixedReality.Toolkit.Experimental.UnityAR.UnityARCameraSettingsProfile::get_UpdateType()
extern void UnityARCameraSettingsProfile_get_UpdateType_m1985535963A1F7F5571069875B67663CBDF3CA28 (void);
// 0x00000008 System.Void Microsoft.MixedReality.Toolkit.Experimental.UnityAR.UnityARCameraSettingsProfile::.ctor()
extern void UnityARCameraSettingsProfile__ctor_mCE4505CC45CC86CAE47A901AD7F03211704B3523 (void);
static Il2CppMethodPointer s_methodPointers[8] = 
{
	UnityARCameraSettings__ctor_m3A77AB31E95CE7D821B85606718E51C281B0F75F,
	UnityARCameraSettings_ReadProfile_m574C6E167BF2D6256AAC2148450CDF58D1505F41,
	UnityARCameraSettings_get_IsOpaque_mBA0726658F038788714B6C915D5271B310623A1E,
	UnityARCameraSettings_get_SettingsProfile_m4D7E02868359C9D7F7CED8D082EE0A47A4638366,
	UnityARCameraSettingsProfile_get_PoseSource_m9200C2FED8464F4F43C8EF3126F93CCC00F2F046,
	UnityARCameraSettingsProfile_get_TrackingType_m03DF33F4B4C03AFF0A0623852D8176CCEC973432,
	UnityARCameraSettingsProfile_get_UpdateType_m1985535963A1F7F5571069875B67663CBDF3CA28,
	UnityARCameraSettingsProfile__ctor_mCE4505CC45CC86CAE47A901AD7F03211704B3523,
};
static const int32_t s_InvokerIndices[8] = 
{
	976,
	4406,
	4369,
	4337,
	4307,
	4307,
	4307,
	4406,
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Providers_UnityAR_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Providers_UnityAR_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Providers_UnityAR_CodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Providers.UnityAR.dll",
	8,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Microsoft_MixedReality_Toolkit_Providers_UnityAR_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
