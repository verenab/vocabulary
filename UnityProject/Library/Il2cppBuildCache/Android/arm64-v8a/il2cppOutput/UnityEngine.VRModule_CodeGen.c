﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean UnityEngine.XR.XRSettings::get_showDeviceView()
extern void XRSettings_get_showDeviceView_mCC1B3C94F32DE036471D10B2E75840D8A3AE0CDB (void);
// 0x00000002 System.Void UnityEngine.XR.XRSettings::set_showDeviceView(System.Boolean)
extern void XRSettings_set_showDeviceView_mA6BAEC79FBDD01F977A931DE1B2BDA4AFADD6E55 (void);
// 0x00000003 System.Void UnityEngine.XR.XRSettings::set_eyeTextureResolutionScale(System.Single)
extern void XRSettings_set_eyeTextureResolutionScale_mBABE036EB59B20AF02A26771F34BB35453C806D2 (void);
// 0x00000004 System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureWidth()
extern void XRSettings_get_eyeTextureWidth_m6202CB8B350531730FAFBBC6CF64EECCA3CBD860 (void);
// 0x00000005 System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureHeight()
extern void XRSettings_get_eyeTextureHeight_m045874DF2D8935D59582C65D8EA9A0A3D96D091A (void);
// 0x00000006 System.Single UnityEngine.XR.XRSettings::get_renderViewportScale()
extern void XRSettings_get_renderViewportScale_m7611DFAB2B3914ABAB79D1BF1A61909DD91A9538 (void);
// 0x00000007 System.Void UnityEngine.XR.XRSettings::set_renderViewportScale(System.Single)
extern void XRSettings_set_renderViewportScale_m0558AC1D03B1B3D848A85A8506FA2A558B950FB1 (void);
// 0x00000008 System.Single UnityEngine.XR.XRSettings::get_renderViewportScaleInternal()
extern void XRSettings_get_renderViewportScaleInternal_m99A2E45DE86E39CAD2795A32424B3FF99A10261C (void);
// 0x00000009 System.Void UnityEngine.XR.XRSettings::set_renderViewportScaleInternal(System.Single)
extern void XRSettings_set_renderViewportScaleInternal_mB0B72F1CCCDCB3E7684264797111CE635DBA8F89 (void);
// 0x0000000A System.Single UnityEngine.XR.XRDevice::get_refreshRate()
extern void XRDevice_get_refreshRate_mD4F76121803499E672DBAF04FEC026C04CBB9D30 (void);
// 0x0000000B System.Void UnityEngine.XR.XRDevice::InvokeDeviceLoaded(System.String)
extern void XRDevice_InvokeDeviceLoaded_m3BDF6825A2A56E4923D4E6593C7BA2949B6A3581 (void);
// 0x0000000C System.Void UnityEngine.XR.XRDevice::.cctor()
extern void XRDevice__cctor_mC83C1293819B81E68EC72D01A5CC107DFE29B98C (void);
static Il2CppMethodPointer s_methodPointers[12] = 
{
	XRSettings_get_showDeviceView_mCC1B3C94F32DE036471D10B2E75840D8A3AE0CDB,
	XRSettings_set_showDeviceView_mA6BAEC79FBDD01F977A931DE1B2BDA4AFADD6E55,
	XRSettings_set_eyeTextureResolutionScale_mBABE036EB59B20AF02A26771F34BB35453C806D2,
	XRSettings_get_eyeTextureWidth_m6202CB8B350531730FAFBBC6CF64EECCA3CBD860,
	XRSettings_get_eyeTextureHeight_m045874DF2D8935D59582C65D8EA9A0A3D96D091A,
	XRSettings_get_renderViewportScale_m7611DFAB2B3914ABAB79D1BF1A61909DD91A9538,
	XRSettings_set_renderViewportScale_m0558AC1D03B1B3D848A85A8506FA2A558B950FB1,
	XRSettings_get_renderViewportScaleInternal_m99A2E45DE86E39CAD2795A32424B3FF99A10261C,
	XRSettings_set_renderViewportScaleInternal_mB0B72F1CCCDCB3E7684264797111CE635DBA8F89,
	XRDevice_get_refreshRate_mD4F76121803499E672DBAF04FEC026C04CBB9D30,
	XRDevice_InvokeDeviceLoaded_m3BDF6825A2A56E4923D4E6593C7BA2949B6A3581,
	XRDevice__cctor_mC83C1293819B81E68EC72D01A5CC107DFE29B98C,
};
static const int32_t s_InvokerIndices[12] = 
{
	7603,
	7509,
	7512,
	7582,
	7582,
	7606,
	7512,
	7606,
	7512,
	7606,
	7505,
	7613,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_VRModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_VRModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VRModule_CodeGenModule = 
{
	"UnityEngine.VRModule.dll",
	12,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_VRModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
