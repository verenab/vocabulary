﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct GenericVirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct InterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct InterfaceActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct InterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Action`1<OVRCameraRig>
struct Action_1_t75B6371B869E36A42B0DFFC8D8B2630F1D659A6C;
// Microsoft.MixedReality.Toolkit.BaseDataProvider`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem>
struct BaseDataProvider_1_t1D6E5ED6946EB7AF9582569B8384D8800F0BBE34;
// Microsoft.MixedReality.Toolkit.BaseDataProvider`1<System.Object>
struct BaseDataProvider_1_t664C3F8898C3B28F4040012FA554BEA276D3A25C;
// System.Collections.Generic.Dictionary`2<System.ByteEnum,System.Object>
struct Dictionary_2_tF6533D4775708EB95178ECDC1D347094A5009988;
// System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>
struct Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11;
// System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,System.UInt32>
struct Dictionary_2_t665131D7AD1ECCB861C41FC1A9F7B01D5386FDC3;
// System.Collections.Generic.Dictionary`2<UnityEngine.XR.InputDevice,Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController>
struct Dictionary_2_t761F6D6973076571DFB4BC441C37BE195205B815;
// System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Int32Enum>
struct Dictionary_2_tFABA756D73CAA65D26DBC9CE4B49A893F6812BB1;
// System.Collections.Generic.Dictionary`2<System.Int32Enum,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>
struct Dictionary_2_t9C45AE0B9705F160EE5D2856D9908DD6FE2A8069;
// System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>
struct Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9;
// System.Collections.Generic.Dictionary`2<OVRSkeleton/BoneId,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint>
struct Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>
struct IEnumerable_1_t52C6C80DC406BD7ADB7404CA90A30624D19AAFEA;
// System.Collections.Generic.IEqualityComparer`1<Microsoft.MixedReality.Toolkit.Utilities.Handedness>
struct IEqualityComparer_1_tD9FCB7A9B96E6E0C1F83FE535CD017D5C796FF50;
// System.Collections.Generic.IEqualityComparer`1<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint>
struct IEqualityComparer_1_tADEF3F3F25409A452827374A4A77C141AE07C4F9;
// System.Collections.Generic.IEqualityComparer`1<OVRSkeleton/BoneId>
struct IEqualityComparer_1_t53A61A6345C7DEADDBFF79C4BAA0097C0DE27008;
// System.Collections.Generic.IList`1<OVRBone>
struct IList_1_t8D7A00864A4D391797D30043CF012669774274C1;
// System.Collections.Generic.IList`1<OVRBoneCapsule>
struct IList_1_t9902F972AFF069803DF269EF8B09079D59FB39DC;
// System.Collections.Generic.Dictionary`2/KeyCollection<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>
struct KeyCollection_t4364B94A66CFE2E74A1717F524C9F0E63BEE3945;
// System.Collections.Generic.Dictionary`2/KeyCollection<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>
struct KeyCollection_tEB9E906EDDB4EB4C4595CFF97A6F85158A061796;
// System.Collections.Generic.Dictionary`2/KeyCollection<OVRSkeleton/BoneId,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint>
struct KeyCollection_t3A7450BDD102F4EE9B3BD0E3D1385B5F194E1FE1;
// System.Collections.Generic.List`1<System.Globalization.CultureInfo>
struct List_1_tB93032867275F6C802C3659FC027FF6FB1372359;
// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice>
struct List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F;
// System.Collections.Generic.List`1<UnityEngine.XR.InputDeviceCharacteristics>
struct List_1_tD812850D83CCFFD34E9A310E2AE62B198E513F2C;
// System.Collections.Generic.List`1<OVRBone>
struct List_1_t8958585D92650E004D74521A8D0B3CE051FA276F;
// System.Collections.Generic.List`1<OVRBoneCapsule>
struct List_1_t2757DE18D8F3AB63917B2846D8F68D853749967C;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>
struct List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.ByteEnum,System.Object>
struct ValueCollection_t9CDC52A109ED7F9848B919DD1D1117BBC2F03DCB;
// System.Collections.Generic.Dictionary`2/ValueCollection<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>
struct ValueCollection_t676F47F68B8CA40F279CA8C5502E572600166614;
// System.Collections.Generic.Dictionary`2/ValueCollection<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>
struct ValueCollection_t15BAF950AB2A665DD7977B23B903E811C6608F37;
// System.Collections.Generic.Dictionary`2/ValueCollection<OVRSkeleton/BoneId,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint>
struct ValueCollection_t8C3BB3E2E9F1D3B7AD8296CD84EECF9ED6A1EEA0;
// System.Collections.Generic.Dictionary`2/Entry<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>[]
struct EntryU5BU5D_t60C548F097D715A0033CFBACBA03951CBEE6589C;
// System.Collections.Generic.Dictionary`2/Entry<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>[]
struct EntryU5BU5D_t26EDEF441C6EA241A1EAF751F05A2B916439BEE0;
// System.Collections.Generic.Dictionary`2/Entry<OVRSkeleton/BoneId,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint>[]
struct EntryU5BU5D_t5D1307BE15170CE1061C617B0EFDA12829E831F4;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputDataProviderConfiguration[]
struct MixedRealityInputDataProviderConfigurationU5BU5D_tACE18ABAB93C0C35F5703C34BFCEE34C58FE2107;
// Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[]
struct MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375;
// OVRControllerHelper[]
struct OVRControllerHelperU5BU5D_tC1041712E5487B448EA158DCEAE5D8D1F8996DD9;
// OVRHand[]
struct OVRHandU5BU5D_tDE7CB5D0BAC901AB98623D26B8A07D4DAA25C6DF;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand[]
struct OculusHandU5BU5D_tD71732579D9064F889A6CC0FA69D461E1993A9AB;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// Microsoft.MixedReality.Toolkit.Input.BaseInputDeviceManager/PointerConfig[]
struct PointerConfigU5BU5D_tD4B506A11EDD45B4BB995B8B1ABC7932AEF3DC82;
// OVRPlugin/Bone[]
struct BoneU5BU5D_t1288E05740DDAC86F76F4CD2019C35A17E72C981;
// OVRPlugin/BoneCapsule[]
struct BoneCapsuleU5BU5D_tB4D9AC5CF59206C13FBD0A3725C45C1F6EBCE295;
// OVRPlugin/Quatf[]
struct QuatfU5BU5D_t652B5A0EAB7922962EBF168B1FC099D8873D11D5;
// OVRPlugin/TrackingConfidence[]
struct TrackingConfidenceU5BU5D_t568C8F700A41BD4F365DD5A0155BEB20A3289322;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition
struct ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5;
// Microsoft.MixedReality.Toolkit.Input.BaseController
struct BaseController_t838F2F1E5179222D8747EBA84A91579901943F76;
// Microsoft.MixedReality.Toolkit.Input.BaseHand
struct BaseHand_t44436345C300F7B303FBC825541C6EF243DCCE1D;
// Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile
struct BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3;
// Microsoft.MixedReality.Toolkit.BaseService
struct BaseService_t793754DF90682F7505A073585FC34F790A7CD7DB;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController
struct GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4;
// Microsoft.MixedReality.Toolkit.Input.IHandRay
struct IHandRay_t41074953FF96FBC4C3A036961EF85AFBD89008D5;
// Microsoft.MixedReality.Toolkit.Input.IMixedRealityControllerVisualizer
struct IMixedRealityControllerVisualizer_tDFC9509CECB8C143D97D302243D1EDF8FFC4A086;
// Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource
struct IMixedRealityInputSource_t64F47F8276A5C458499BF997886819249AA1EFD0;
// Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSourceDefinition
struct IMixedRealityInputSourceDefinition_t13ACA02FBB4328A061DE95F4012129506B235F7B;
// Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem
struct IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1;
// Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer
struct IMixedRealityPointer_tDDEF9E208B33248BDF4F101FCD2C1ED7C48919CE;
// Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar
struct IMixedRealityServiceRegistrar_t279804348C19FFBAC1249ADF9359629F9AC1C873;
// Microsoft.MixedReality.Toolkit.Input.IMixedRealityTeleportPointer
struct IMixedRealityTeleportPointer_t47473BCF99340AE6AF86C114AC0249289D6EC21D;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// Microsoft.MixedReality.Toolkit.Input.MixedRealityControllerMappingProfile
struct MixedRealityControllerMappingProfile_tA162449AD6794B68CC303CB68AC0EDE1B8BC72D8;
// Microsoft.MixedReality.Toolkit.Input.MixedRealityControllerVisualizationProfile
struct MixedRealityControllerVisualizationProfile_t422A4125E237779F487A8BD82D7670928B885105;
// Microsoft.MixedReality.Toolkit.Input.MixedRealityGesturesProfile
struct MixedRealityGesturesProfile_t344E8703F2DA838589B447BE14DC1C446D171395;
// Microsoft.MixedReality.Toolkit.Input.MixedRealityHandTrackingProfile
struct MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8;
// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputActionRulesProfile
struct MixedRealityInputActionRulesProfile_tF2708C51AAF3894A860028C79DB05A44D6EDD64F;
// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputActionsProfile
struct MixedRealityInputActionsProfile_tDA21846ABEF425362A21B447C04C8C199392AC82;
// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile
struct MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C;
// Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping
struct MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505;
// Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerProfile
struct MixedRealityPointerProfile_t51E556B24A06CDA2552A8B38F3874757C70128FE;
// Microsoft.MixedReality.Toolkit.Input.MixedRealitySpeechCommandsProfile
struct MixedRealitySpeechCommandsProfile_tD8FD268D341E7DD3B3858B34A2CB2D902AA35DED;
// OVRBone
struct OVRBone_tF9C031559703607035E455CE642549C722BBF081;
// OVRCameraRig
struct OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517;
// OVRControllerHelper
struct OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6;
// OVRHand
struct OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A;
// OVRSkeleton
struct OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand
struct OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4;
// Microsoft.MixedReality.Toolkit.Input.OculusTouchControllerDefinition
struct OculusTouchControllerDefinition_t9C81F5CDBD5908C5D1CE58FB90A4375457C7F831;
// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager
struct OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C;
// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile
struct OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7;
// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKTouchController
struct OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED;
// System.String
struct String_t;
// Microsoft.MixedReality.Toolkit.Utilities.SystemType
struct SystemType_t5A8091D125CA72F271FAB6EE68A869F3628DBED5;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager
struct XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// OVRSkeleton/IOVRSkeletonDataProvider
struct IOVRSkeletonDataProvider_tC7337A6B5BA23BEAF1294F2F00EAD10801B046BA;

IL2CPP_EXTERN_C RuntimeClass* ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Handedness_t144F166D3615E0DB2843B937FF620ED9F92680BD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_1_t897D4F5EF6A9A5E4C3A8253F1BCE615BD0BBC9C1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_t3933D6BE2BD9674342F4135CE97CF489D5D1A47D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IMixedRealityInputSource_t64F47F8276A5C458499BF997886819249AA1EFD0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IMixedRealityPointer_tDDEF9E208B33248BDF4F101FCD2C1ED7C48919CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IOVRSkeletonDataProvider_tC7337A6B5BA23BEAF1294F2F00EAD10801B046BA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MixedRealityPlayspace_tEEDF7A044A9E8E252CF39E2F7C51AB77FC70367A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OVRManager_t6088CD544814E5A64EEB830D130A6816E79D0036_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OVRPlugin_t33529050E1318B8BC645C7451E1A3C8C4E500DCF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OculusTouchControllerDefinition_t9C81F5CDBD5908C5D1CE58FB90A4375457C7F831_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral5694A94BF0FB5DA2B661CD06A0F34DF40E5BE995;
IL2CPP_EXTERN_C String_t* _stringLiteral5E59A9F9DA37A05B58E7A1FCC2DE7D33725F73CA;
IL2CPP_EXTERN_C String_t* _stringLiteralA8ABEE8078E902ABD0AEA109AB9441E0A0845281;
IL2CPP_EXTERN_C const RuntimeMethod* BaseDataProvider_1_get_Service_mF32AD96A346149DFD23B70B4300F9D47A232E778_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisOVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC_mDA071F49B497F5BEA47FB7193E04836BAD026E1A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisOVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A_m5D03A208D3426756262B26B81993BB82D62D1CBA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mBF9F9FCB9B086F9959CD47F46FEF82DB67C01A98_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mD183155545F86332FE7DF071F13B274F6D514344_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Clear_mA0FFB84584221F5822A86A7C30FAAC00546DE437_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m0620BD3B7D5DE1DB5488BA648F10C4AFF085DA44_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m4DC00BAE7DFA4CF7ECDD2E576E31E5FA31F36DE7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Remove_m533F4D4B5A32EB3E23F19ADD7C3FB48775D61675_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m6CC999BE14BA489631964C386680A89AEEB456DA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m91A28CD3479611BC6DE4868B185BC381A2114D78_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_mE4FFA1E538E65BD642AE31936056434FE06E5306_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m11C00E81E12AE2761EDA2BB312BBE57AA4E8C696_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m8A8D33B5A8E9011511505D1AB8698F63E2622E51_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mB3D1F54DCBB3BAAA711763CA6512DEE2A954228C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Count_mB586D4F1BB640E1344BE62A6FAFC9EBFEBA058F2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_m130BCA3A6BF1FB071A48981A972E9E42C03BB173_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_m5A745DC8015EA0953A57CDB2866344D2AF45CC9E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Values_mC6AAA65393E9DC180F2A7E33AE230C3C0B3043CB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m899DD9BD808EDEA1D10982AD411594DEC9DE218A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m3E5463E197FDA243351F081A7D6B6E56A1DF8746_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mF796BF6B5102FFD669E3C1AB8837158CE4269E47_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m6C1D5E2616E3F0772514421F05D8507D5316FCB1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisOVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517_m6146EEB03DE9A96D5EEC6C066EB360E747B2C297_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentsInChildren_TisOVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6_mFADE4B7CBB98CF80A59C147FAD9C274B3118265F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mABE9872DD40A96E02EB4939FFF85A51EA450C6A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m757D094B08518BB620126F91954AE3B3F54918B5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_GetValueOrDefault_mBECC58FEFD1088EC070D9F9A892ECD1D8BBF2A0F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1__ctor_m402A94AC9070B345C95919DCFFFF627252B3E67A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_m52F33C6963C9A76A14F54B4B2C30CEF580DB1EB7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_m7455E879CFAAE682AE3786D4D2B1F65C8AA23921_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisOVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517_mCD0D2DA6EF2EE7BAF6D2611A748177C0E8894A31_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XRSDKDeviceManager_IsLoaderActive_TisOculusLoader_tD15B17A95554127ED764AFC39FE3D0E720C8BFEB_m7749693AFBB25485FF31EB6A35889D936236F91A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_0_0_0_var;
struct BoneCapsule_t482B1E40C3A6FD1C90BAF5B3A24719B196071B07 ;
struct Bone_tFB367C47EBF89DA78C7AA2A09C8D24427723B500 ;
struct Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D ;

struct IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7;
struct MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375;
struct OVRControllerHelperU5BU5D_tC1041712E5487B448EA158DCEAE5D8D1F8996DD9;
struct OVRHandU5BU5D_tDE7CB5D0BAC901AB98623D26B8A07D4DAA25C6DF;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t7E156B3FC801778A285B65B1116B3991AA9EEF25 
{
public:

public:
};


// System.Object


// System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>
struct Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t60C548F097D715A0033CFBACBA03951CBEE6589C* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t4364B94A66CFE2E74A1717F524C9F0E63BEE3945 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t676F47F68B8CA40F279CA8C5502E572600166614 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11, ___entries_1)); }
	inline EntryU5BU5D_t60C548F097D715A0033CFBACBA03951CBEE6589C* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t60C548F097D715A0033CFBACBA03951CBEE6589C** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t60C548F097D715A0033CFBACBA03951CBEE6589C* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11, ___keys_7)); }
	inline KeyCollection_t4364B94A66CFE2E74A1717F524C9F0E63BEE3945 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t4364B94A66CFE2E74A1717F524C9F0E63BEE3945 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t4364B94A66CFE2E74A1717F524C9F0E63BEE3945 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11, ___values_8)); }
	inline ValueCollection_t676F47F68B8CA40F279CA8C5502E572600166614 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t676F47F68B8CA40F279CA8C5502E572600166614 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t676F47F68B8CA40F279CA8C5502E572600166614 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>
struct Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t26EDEF441C6EA241A1EAF751F05A2B916439BEE0* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tEB9E906EDDB4EB4C4595CFF97A6F85158A061796 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t15BAF950AB2A665DD7977B23B903E811C6608F37 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9, ___entries_1)); }
	inline EntryU5BU5D_t26EDEF441C6EA241A1EAF751F05A2B916439BEE0* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t26EDEF441C6EA241A1EAF751F05A2B916439BEE0** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t26EDEF441C6EA241A1EAF751F05A2B916439BEE0* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9, ___keys_7)); }
	inline KeyCollection_tEB9E906EDDB4EB4C4595CFF97A6F85158A061796 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tEB9E906EDDB4EB4C4595CFF97A6F85158A061796 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tEB9E906EDDB4EB4C4595CFF97A6F85158A061796 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9, ___values_8)); }
	inline ValueCollection_t15BAF950AB2A665DD7977B23B903E811C6608F37 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t15BAF950AB2A665DD7977B23B903E811C6608F37 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t15BAF950AB2A665DD7977B23B903E811C6608F37 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2<OVRSkeleton/BoneId,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint>
struct Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t5D1307BE15170CE1061C617B0EFDA12829E831F4* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t3A7450BDD102F4EE9B3BD0E3D1385B5F194E1FE1 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t8C3BB3E2E9F1D3B7AD8296CD84EECF9ED6A1EEA0 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B, ___entries_1)); }
	inline EntryU5BU5D_t5D1307BE15170CE1061C617B0EFDA12829E831F4* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t5D1307BE15170CE1061C617B0EFDA12829E831F4** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t5D1307BE15170CE1061C617B0EFDA12829E831F4* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B, ___keys_7)); }
	inline KeyCollection_t3A7450BDD102F4EE9B3BD0E3D1385B5F194E1FE1 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t3A7450BDD102F4EE9B3BD0E3D1385B5F194E1FE1 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t3A7450BDD102F4EE9B3BD0E3D1385B5F194E1FE1 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B, ___values_8)); }
	inline ValueCollection_t8C3BB3E2E9F1D3B7AD8296CD84EECF9ED6A1EEA0 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t8C3BB3E2E9F1D3B7AD8296CD84EECF9ED6A1EEA0 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t8C3BB3E2E9F1D3B7AD8296CD84EECF9ED6A1EEA0 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>
struct List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	OculusHandU5BU5D_tD71732579D9064F889A6CC0FA69D461E1993A9AB* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7, ____items_1)); }
	inline OculusHandU5BU5D_tD71732579D9064F889A6CC0FA69D461E1993A9AB* get__items_1() const { return ____items_1; }
	inline OculusHandU5BU5D_tD71732579D9064F889A6CC0FA69D461E1993A9AB** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(OculusHandU5BU5D_tD71732579D9064F889A6CC0FA69D461E1993A9AB* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	OculusHandU5BU5D_tD71732579D9064F889A6CC0FA69D461E1993A9AB* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7_StaticFields, ____emptyArray_5)); }
	inline OculusHandU5BU5D_tD71732579D9064F889A6CC0FA69D461E1993A9AB* get__emptyArray_5() const { return ____emptyArray_5; }
	inline OculusHandU5BU5D_tD71732579D9064F889A6CC0FA69D461E1993A9AB** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(OculusHandU5BU5D_tD71732579D9064F889A6CC0FA69D461E1993A9AB* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2/ValueCollection<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>
struct ValueCollection_t676F47F68B8CA40F279CA8C5502E572600166614  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection::dictionary
	Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(ValueCollection_t676F47F68B8CA40F279CA8C5502E572600166614, ___dictionary_0)); }
	inline Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>
struct Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6, ___list_0)); }
	inline List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7 * get_list_0() const { return ___list_0; }
	inline List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6, ___current_3)); }
	inline OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * get_current_3() const { return ___current_3; }
	inline OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// UnityEngine.XR.InputFeatureUsage`1<System.Boolean>
struct InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40 
{
public:
	// System.String UnityEngine.XR.InputFeatureUsage`1::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
};
#endif
// Native definition for COM marshalling of UnityEngine.XR.InputFeatureUsage`1
#ifndef InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
#define InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com_define
struct InputFeatureUsage_1_t0883EAB3AD99A1D218140E4C4D1FD0A2AC401FA1_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
};
#endif

// System.Nullable`1<System.Boolean>
struct Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<System.Int32>
struct Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.XR.InputDevice
struct InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E 
{
public:
	// System.UInt64 UnityEngine.XR.InputDevice::m_DeviceId
	uint64_t ___m_DeviceId_0;
	// System.Boolean UnityEngine.XR.InputDevice::m_Initialized
	bool ___m_Initialized_1;

public:
	inline static int32_t get_offset_of_m_DeviceId_0() { return static_cast<int32_t>(offsetof(InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E, ___m_DeviceId_0)); }
	inline uint64_t get_m_DeviceId_0() const { return ___m_DeviceId_0; }
	inline uint64_t* get_address_of_m_DeviceId_0() { return &___m_DeviceId_0; }
	inline void set_m_DeviceId_0(uint64_t value)
	{
		___m_DeviceId_0 = value;
	}

	inline static int32_t get_offset_of_m_Initialized_1() { return static_cast<int32_t>(offsetof(InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E, ___m_Initialized_1)); }
	inline bool get_m_Initialized_1() const { return ___m_Initialized_1; }
	inline bool* get_address_of_m_Initialized_1() { return &___m_Initialized_1; }
	inline void set_m_Initialized_1(bool value)
	{
		___m_Initialized_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.XR.InputDevice
struct InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_marshaled_pinvoke
{
	uint64_t ___m_DeviceId_0;
	int32_t ___m_Initialized_1;
};
// Native definition for COM marshalling of UnityEngine.XR.InputDevice
struct InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E_marshaled_com
{
	uint64_t ___m_DeviceId_0;
	int32_t ___m_Initialized_1;
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt16
struct UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD, ___m_value_0)); }
	inline uint16_t get_m_value_0() const { return ___m_value_0; }
	inline uint16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint16_t value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// OVRPlugin/Quatf
struct Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D 
{
public:
	// System.Single OVRPlugin/Quatf::x
	float ___x_0;
	// System.Single OVRPlugin/Quatf::y
	float ___y_1;
	// System.Single OVRPlugin/Quatf::z
	float ___z_2;
	// System.Single OVRPlugin/Quatf::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D_StaticFields
{
public:
	// OVRPlugin/Quatf OVRPlugin/Quatf::identity
	Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D  ___identity_4;

public:
	inline static int32_t get_offset_of_identity_4() { return static_cast<int32_t>(offsetof(Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D_StaticFields, ___identity_4)); }
	inline Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D  get_identity_4() const { return ___identity_4; }
	inline Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D * get_address_of_identity_4() { return &___identity_4; }
	inline void set_identity_4(Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D  value)
	{
		___identity_4 = value;
	}
};


// OVRPlugin/Vector3f
struct Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417 
{
public:
	// System.Single OVRPlugin/Vector3f::x
	float ___x_0;
	// System.Single OVRPlugin/Vector3f::y
	float ___y_1;
	// System.Single OVRPlugin/Vector3f::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

struct Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417_StaticFields
{
public:
	// OVRPlugin/Vector3f OVRPlugin/Vector3f::zero
	Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417  ___zero_3;

public:
	inline static int32_t get_offset_of_zero_3() { return static_cast<int32_t>(offsetof(Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417_StaticFields, ___zero_3)); }
	inline Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417  get_zero_3() const { return ___zero_3; }
	inline Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417 * get_address_of_zero_3() { return &___zero_3; }
	inline void set_zero_3(Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417  value)
	{
		___zero_3 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Utilities.AxisType
struct AxisType_t06BEA5A77481B1945FBA16B2191769565854AE6C 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.Utilities.AxisType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisType_t06BEA5A77481B1945FBA16B2191769565854AE6C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.BaseService
struct BaseService_t793754DF90682F7505A073585FC34F790A7CD7DB  : public RuntimeObject
{
public:
	// System.String Microsoft.MixedReality.Toolkit.BaseService::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;
	// System.UInt32 Microsoft.MixedReality.Toolkit.BaseService::<Priority>k__BackingField
	uint32_t ___U3CPriorityU3Ek__BackingField_2;
	// Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile Microsoft.MixedReality.Toolkit.BaseService::<ConfigurationProfile>k__BackingField
	BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3 * ___U3CConfigurationProfileU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> Microsoft.MixedReality.Toolkit.BaseService::isInitialized
	Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  ___isInitialized_4;
	// System.Nullable`1<System.Boolean> Microsoft.MixedReality.Toolkit.BaseService::isEnabled
	Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  ___isEnabled_5;
	// System.Nullable`1<System.Boolean> Microsoft.MixedReality.Toolkit.BaseService::isMarkedDestroyed
	Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  ___isMarkedDestroyed_6;
	// System.Boolean Microsoft.MixedReality.Toolkit.BaseService::disposed
	bool ___disposed_7;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BaseService_t793754DF90682F7505A073585FC34F790A7CD7DB, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPriorityU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BaseService_t793754DF90682F7505A073585FC34F790A7CD7DB, ___U3CPriorityU3Ek__BackingField_2)); }
	inline uint32_t get_U3CPriorityU3Ek__BackingField_2() const { return ___U3CPriorityU3Ek__BackingField_2; }
	inline uint32_t* get_address_of_U3CPriorityU3Ek__BackingField_2() { return &___U3CPriorityU3Ek__BackingField_2; }
	inline void set_U3CPriorityU3Ek__BackingField_2(uint32_t value)
	{
		___U3CPriorityU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CConfigurationProfileU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BaseService_t793754DF90682F7505A073585FC34F790A7CD7DB, ___U3CConfigurationProfileU3Ek__BackingField_3)); }
	inline BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3 * get_U3CConfigurationProfileU3Ek__BackingField_3() const { return ___U3CConfigurationProfileU3Ek__BackingField_3; }
	inline BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3 ** get_address_of_U3CConfigurationProfileU3Ek__BackingField_3() { return &___U3CConfigurationProfileU3Ek__BackingField_3; }
	inline void set_U3CConfigurationProfileU3Ek__BackingField_3(BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3 * value)
	{
		___U3CConfigurationProfileU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CConfigurationProfileU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_isInitialized_4() { return static_cast<int32_t>(offsetof(BaseService_t793754DF90682F7505A073585FC34F790A7CD7DB, ___isInitialized_4)); }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  get_isInitialized_4() const { return ___isInitialized_4; }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * get_address_of_isInitialized_4() { return &___isInitialized_4; }
	inline void set_isInitialized_4(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  value)
	{
		___isInitialized_4 = value;
	}

	inline static int32_t get_offset_of_isEnabled_5() { return static_cast<int32_t>(offsetof(BaseService_t793754DF90682F7505A073585FC34F790A7CD7DB, ___isEnabled_5)); }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  get_isEnabled_5() const { return ___isEnabled_5; }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * get_address_of_isEnabled_5() { return &___isEnabled_5; }
	inline void set_isEnabled_5(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  value)
	{
		___isEnabled_5 = value;
	}

	inline static int32_t get_offset_of_isMarkedDestroyed_6() { return static_cast<int32_t>(offsetof(BaseService_t793754DF90682F7505A073585FC34F790A7CD7DB, ___isMarkedDestroyed_6)); }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  get_isMarkedDestroyed_6() const { return ___isMarkedDestroyed_6; }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * get_address_of_isMarkedDestroyed_6() { return &___isMarkedDestroyed_6; }
	inline void set_isMarkedDestroyed_6(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  value)
	{
		___isMarkedDestroyed_6 = value;
	}

	inline static int32_t get_offset_of_disposed_7() { return static_cast<int32_t>(offsetof(BaseService_t793754DF90682F7505A073585FC34F790A7CD7DB, ___disposed_7)); }
	inline bool get_disposed_7() const { return ___disposed_7; }
	inline bool* get_address_of_disposed_7() { return &___disposed_7; }
	inline void set_disposed_7(bool value)
	{
		___disposed_7 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.ByteEnum
struct ByteEnum_t39285A9D8C7F88982FF718C04A9FA1AE64827307 
{
public:
	// System.Byte System.ByteEnum::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ByteEnum_t39285A9D8C7F88982FF718C04A9FA1AE64827307, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Input.DeviceInputType
struct DeviceInputType_t794529FEA7F965E851091BB4B8EE5E0F15A49910 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.Input.DeviceInputType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeviceInputType_t794529FEA7F965E851091BB4B8EE5E0F15A49910, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Utilities.Handedness
struct Handedness_t144F166D3615E0DB2843B937FF620ED9F92680BD 
{
public:
	// System.Byte Microsoft.MixedReality.Toolkit.Utilities.Handedness::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Handedness_t144F166D3615E0DB2843B937FF620ED9F92680BD, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.InputDeviceCharacteristics
struct InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64 
{
public:
	// System.UInt32 UnityEngine.XR.InputDeviceCharacteristics::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Input.InputSourceType
struct InputSourceType_t04050C0A6B2B0A2964FA96DDA8B37E3A04C48D86 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.Input.InputSourceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputSourceType_t04050C0A6B2B0A2964FA96DDA8B37E3A04C48D86, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Int32Enum
struct Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.KeyCode
struct KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Profiling.LowLevel.MarkerFlags
struct MarkerFlags_t4A8B5185BAD24803CE9A57187867CB93451AA9E8 
{
public:
	// System.UInt16 Unity.Profiling.LowLevel.MarkerFlags::value__
	uint16_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MarkerFlags_t4A8B5185BAD24803CE9A57187867CB93451AA9E8, ___value___2)); }
	inline uint16_t get_value___2() const { return ___value___2; }
	inline uint16_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint16_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.MixedRealityCapability
struct MixedRealityCapability_t6A601C030EF6EAFDE8A65029641E1E7993516156 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.MixedRealityCapability::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MixedRealityCapability_t6A601C030EF6EAFDE8A65029641E1E7993516156, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose
struct MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 
{
public:
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_1;
	// UnityEngine.Quaternion Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose::rotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation_2;

public:
	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669, ___position_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_1() const { return ___position_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669, ___rotation_2)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_rotation_2() const { return ___rotation_2; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___rotation_2 = value;
	}
};

struct MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669_StaticFields
{
public:
	// Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose::<ZeroIdentity>k__BackingField
	MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  ___U3CZeroIdentityU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CZeroIdentityU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669_StaticFields, ___U3CZeroIdentityU3Ek__BackingField_0)); }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  get_U3CZeroIdentityU3Ek__BackingField_0() const { return ___U3CZeroIdentityU3Ek__BackingField_0; }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * get_address_of_U3CZeroIdentityU3Ek__BackingField_0() { return &___U3CZeroIdentityU3Ek__BackingField_0; }
	inline void set_U3CZeroIdentityU3Ek__BackingField_0(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  value)
	{
		___U3CZeroIdentityU3Ek__BackingField_0 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Unity.XR.Oculus.OculusUsages
struct OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77  : public RuntimeObject
{
public:

public:
};

struct OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_StaticFields
{
public:
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> Unity.XR.Oculus.OculusUsages::volumeUp
	InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  ___volumeUp_0;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> Unity.XR.Oculus.OculusUsages::volumeDown
	InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  ___volumeDown_1;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> Unity.XR.Oculus.OculusUsages::thumbrest
	InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  ___thumbrest_2;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> Unity.XR.Oculus.OculusUsages::indexTouch
	InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  ___indexTouch_3;
	// UnityEngine.XR.InputFeatureUsage`1<System.Boolean> Unity.XR.Oculus.OculusUsages::thumbTouch
	InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  ___thumbTouch_4;

public:
	inline static int32_t get_offset_of_volumeUp_0() { return static_cast<int32_t>(offsetof(OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_StaticFields, ___volumeUp_0)); }
	inline InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  get_volumeUp_0() const { return ___volumeUp_0; }
	inline InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40 * get_address_of_volumeUp_0() { return &___volumeUp_0; }
	inline void set_volumeUp_0(InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  value)
	{
		___volumeUp_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___volumeUp_0))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_volumeDown_1() { return static_cast<int32_t>(offsetof(OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_StaticFields, ___volumeDown_1)); }
	inline InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  get_volumeDown_1() const { return ___volumeDown_1; }
	inline InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40 * get_address_of_volumeDown_1() { return &___volumeDown_1; }
	inline void set_volumeDown_1(InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  value)
	{
		___volumeDown_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___volumeDown_1))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_thumbrest_2() { return static_cast<int32_t>(offsetof(OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_StaticFields, ___thumbrest_2)); }
	inline InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  get_thumbrest_2() const { return ___thumbrest_2; }
	inline InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40 * get_address_of_thumbrest_2() { return &___thumbrest_2; }
	inline void set_thumbrest_2(InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  value)
	{
		___thumbrest_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___thumbrest_2))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_indexTouch_3() { return static_cast<int32_t>(offsetof(OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_StaticFields, ___indexTouch_3)); }
	inline InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  get_indexTouch_3() const { return ___indexTouch_3; }
	inline InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40 * get_address_of_indexTouch_3() { return &___indexTouch_3; }
	inline void set_indexTouch_3(InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  value)
	{
		___indexTouch_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___indexTouch_3))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_thumbTouch_4() { return static_cast<int32_t>(offsetof(OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_StaticFields, ___thumbTouch_4)); }
	inline InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  get_thumbTouch_4() const { return ___thumbTouch_4; }
	inline InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40 * get_address_of_thumbTouch_4() { return &___thumbTouch_4; }
	inline void set_thumbTouch_4(InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  value)
	{
		___thumbTouch_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___thumbTouch_4))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
	}
};


// Unity.Profiling.ProfilerMarker
struct ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 
{
public:
	// System.IntPtr Unity.Profiling.ProfilerMarker::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Utilities.SupportedApplicationModes
struct SupportedApplicationModes_t82FC507BDAC8CF8492EADA4B94AF728759466408 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.Utilities.SupportedApplicationModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SupportedApplicationModes_t82FC507BDAC8CF8492EADA4B94AF728759466408, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Input.SupportedControllerType
struct SupportedControllerType_tBFD06B4C412CE5A758BBCA684A9E5B6BAC192354 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.Input.SupportedControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SupportedControllerType_tBFD06B4C412CE5A758BBCA684A9E5B6BAC192354, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint
struct TrackedHandJoint_t620F91E48DF5EDC1E7C044E57C73415B32654AC6 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackedHandJoint_t620F91E48DF5EDC1E7C044E57C73415B32654AC6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.TrackingState
struct TrackingState_t977CCA762FF5628456851C8FC25A4CFAE4330B8A 
{
public:
	// System.Int32 Microsoft.MixedReality.Toolkit.TrackingState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingState_t977CCA762FF5628456851C8FC25A4CFAE4330B8A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRControllerHelper/ControllerType
struct ControllerType_tE10849E6651CF56D5E166AE111F12866B5E7AF22 
{
public:
	// System.Int32 OVRControllerHelper/ControllerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerType_tE10849E6651CF56D5E166AE111F12866B5E7AF22, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRHand/Hand
struct Hand_t1DDDE8E88FE43B0552E010F6755CA5AE5078EBD4 
{
public:
	// System.Int32 OVRHand/Hand::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Hand_t1DDDE8E88FE43B0552E010F6755CA5AE5078EBD4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRHand/HandFinger
struct HandFinger_tEDE6395DC7A01EA2212801493894BB09AEAB0F4C 
{
public:
	// System.Int32 OVRHand/HandFinger::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HandFinger_tEDE6395DC7A01EA2212801493894BB09AEAB0F4C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRHand/TrackingConfidence
struct TrackingConfidence_t05F8DCB45E675624F2884D85FA56DDBA1665E9E3 
{
public:
	// System.Int32 OVRHand/TrackingConfidence::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingConfidence_t05F8DCB45E675624F2884D85FA56DDBA1665E9E3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRInput/Controller
struct Controller_tEEDA108639533B73057BAE8B95FE21725355C48F 
{
public:
	// System.Int32 OVRInput/Controller::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Controller_tEEDA108639533B73057BAE8B95FE21725355C48F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRManager/FixedFoveatedRenderingLevel
struct FixedFoveatedRenderingLevel_t6B5398EA323AC0E8EC80D155DFD698989A2BF9F8 
{
public:
	// System.Int32 OVRManager/FixedFoveatedRenderingLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FixedFoveatedRenderingLevel_t6B5398EA323AC0E8EC80D155DFD698989A2BF9F8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRPlugin/HandFingerPinch
struct HandFingerPinch_t34ACF5B486E328BD7E2DBE8E74807ACA9FAF31CE 
{
public:
	// System.Int32 OVRPlugin/HandFingerPinch::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HandFingerPinch_t34ACF5B486E328BD7E2DBE8E74807ACA9FAF31CE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRPlugin/HandStatus
struct HandStatus_t3CD43284B7DC75BB49345FE221FDB6046EFC9B65 
{
public:
	// System.Int32 OVRPlugin/HandStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HandStatus_t3CD43284B7DC75BB49345FE221FDB6046EFC9B65, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRPlugin/Posef
struct Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952 
{
public:
	// OVRPlugin/Quatf OVRPlugin/Posef::Orientation
	Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D  ___Orientation_0;
	// OVRPlugin/Vector3f OVRPlugin/Posef::Position
	Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417  ___Position_1;

public:
	inline static int32_t get_offset_of_Orientation_0() { return static_cast<int32_t>(offsetof(Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952, ___Orientation_0)); }
	inline Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D  get_Orientation_0() const { return ___Orientation_0; }
	inline Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D * get_address_of_Orientation_0() { return &___Orientation_0; }
	inline void set_Orientation_0(Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D  value)
	{
		___Orientation_0 = value;
	}

	inline static int32_t get_offset_of_Position_1() { return static_cast<int32_t>(offsetof(Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952, ___Position_1)); }
	inline Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417  get_Position_1() const { return ___Position_1; }
	inline Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417 * get_address_of_Position_1() { return &___Position_1; }
	inline void set_Position_1(Vector3f_tF74D3A692AC7064BFF4EBEC1E704723CA2905417  value)
	{
		___Position_1 = value;
	}
};

struct Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952_StaticFields
{
public:
	// OVRPlugin/Posef OVRPlugin/Posef::identity
	Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  ___identity_2;

public:
	inline static int32_t get_offset_of_identity_2() { return static_cast<int32_t>(offsetof(Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952_StaticFields, ___identity_2)); }
	inline Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  get_identity_2() const { return ___identity_2; }
	inline Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952 * get_address_of_identity_2() { return &___identity_2; }
	inline void set_identity_2(Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  value)
	{
		___identity_2 = value;
	}
};


// OVRPlugin/SkeletonType
struct SkeletonType_tA6B9642BF519E11B92A7A71113FCB5D3412201AF 
{
public:
	// System.Int32 OVRPlugin/SkeletonType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SkeletonType_tA6B9642BF519E11B92A7A71113FCB5D3412201AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRPlugin/TrackingConfidence
struct TrackingConfidence_t33FDA3D2EA5B7D420BF70D7F0586ACBB4CDCF506 
{
public:
	// System.Int32 OVRPlugin/TrackingConfidence::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingConfidence_t33FDA3D2EA5B7D420BF70D7F0586ACBB4CDCF506, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRSkeleton/BoneId
struct BoneId_tDF329B86B125C570AD6B5D3B4D9060B62B5A2024 
{
public:
	// System.Int32 OVRSkeleton/BoneId::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BoneId_tDF329B86B125C570AD6B5D3B4D9060B62B5A2024, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRSkeleton/SkeletonType
struct SkeletonType_tC89352585315EE6489FBE98C0DA06DC97233B231 
{
public:
	// System.Int32 OVRSkeleton/SkeletonType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SkeletonType_tC89352585315EE6489FBE98C0DA06DC97233B231, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Profiling.ProfilerMarker/AutoScope
struct AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D 
{
public:
	// System.IntPtr Unity.Profiling.ProfilerMarker/AutoScope::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};


// Microsoft.MixedReality.Toolkit.BaseDataProvider`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem>
struct BaseDataProvider_1_t1D6E5ED6946EB7AF9582569B8384D8800F0BBE34  : public BaseService_t793754DF90682F7505A073585FC34F790A7CD7DB
{
public:
	// Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar Microsoft.MixedReality.Toolkit.BaseDataProvider`1::<Registrar>k__BackingField
	RuntimeObject* ___U3CRegistrarU3Ek__BackingField_8;
	// T Microsoft.MixedReality.Toolkit.BaseDataProvider`1::<Service>k__BackingField
	RuntimeObject* ___U3CServiceU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CRegistrarU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BaseDataProvider_1_t1D6E5ED6946EB7AF9582569B8384D8800F0BBE34, ___U3CRegistrarU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CRegistrarU3Ek__BackingField_8() const { return ___U3CRegistrarU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CRegistrarU3Ek__BackingField_8() { return &___U3CRegistrarU3Ek__BackingField_8; }
	inline void set_U3CRegistrarU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CRegistrarU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRegistrarU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CServiceU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(BaseDataProvider_1_t1D6E5ED6946EB7AF9582569B8384D8800F0BBE34, ___U3CServiceU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CServiceU3Ek__BackingField_9() const { return ___U3CServiceU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CServiceU3Ek__BackingField_9() { return &___U3CServiceU3Ek__BackingField_9; }
	inline void set_U3CServiceU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CServiceU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CServiceU3Ek__BackingField_9), (void*)value);
	}
};


// Microsoft.MixedReality.Toolkit.BaseDataProvider`1<System.Object>
struct BaseDataProvider_1_t664C3F8898C3B28F4040012FA554BEA276D3A25C  : public BaseService_t793754DF90682F7505A073585FC34F790A7CD7DB
{
public:
	// Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar Microsoft.MixedReality.Toolkit.BaseDataProvider`1::<Registrar>k__BackingField
	RuntimeObject* ___U3CRegistrarU3Ek__BackingField_8;
	// T Microsoft.MixedReality.Toolkit.BaseDataProvider`1::<Service>k__BackingField
	RuntimeObject * ___U3CServiceU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CRegistrarU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BaseDataProvider_1_t664C3F8898C3B28F4040012FA554BEA276D3A25C, ___U3CRegistrarU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CRegistrarU3Ek__BackingField_8() const { return ___U3CRegistrarU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CRegistrarU3Ek__BackingField_8() { return &___U3CRegistrarU3Ek__BackingField_8; }
	inline void set_U3CRegistrarU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CRegistrarU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRegistrarU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CServiceU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(BaseDataProvider_1_t664C3F8898C3B28F4040012FA554BEA276D3A25C, ___U3CServiceU3Ek__BackingField_9)); }
	inline RuntimeObject * get_U3CServiceU3Ek__BackingField_9() const { return ___U3CServiceU3Ek__BackingField_9; }
	inline RuntimeObject ** get_address_of_U3CServiceU3Ek__BackingField_9() { return &___U3CServiceU3Ek__BackingField_9; }
	inline void set_U3CServiceU3Ek__BackingField_9(RuntimeObject * value)
	{
		___U3CServiceU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CServiceU3Ek__BackingField_9), (void*)value);
	}
};


// Microsoft.MixedReality.Toolkit.Input.BaseController
struct BaseController_t838F2F1E5179222D8747EBA84A91579901943F76  : public RuntimeObject
{
public:
	// Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSourceDefinition Microsoft.MixedReality.Toolkit.Input.BaseController::<Definition>k__BackingField
	RuntimeObject* ___U3CDefinitionU3Ek__BackingField_0;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.BaseController::<Enabled>k__BackingField
	bool ___U3CEnabledU3Ek__BackingField_1;
	// Microsoft.MixedReality.Toolkit.TrackingState Microsoft.MixedReality.Toolkit.Input.BaseController::<TrackingState>k__BackingField
	int32_t ___U3CTrackingStateU3Ek__BackingField_2;
	// Microsoft.MixedReality.Toolkit.Utilities.Handedness Microsoft.MixedReality.Toolkit.Input.BaseController::<ControllerHandedness>k__BackingField
	uint8_t ___U3CControllerHandednessU3Ek__BackingField_3;
	// Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource Microsoft.MixedReality.Toolkit.Input.BaseController::<InputSource>k__BackingField
	RuntimeObject* ___U3CInputSourceU3Ek__BackingField_4;
	// Microsoft.MixedReality.Toolkit.Input.IMixedRealityControllerVisualizer Microsoft.MixedReality.Toolkit.Input.BaseController::<Visualizer>k__BackingField
	RuntimeObject* ___U3CVisualizerU3Ek__BackingField_5;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.BaseController::<IsPositionAvailable>k__BackingField
	bool ___U3CIsPositionAvailableU3Ek__BackingField_6;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.BaseController::<IsPositionApproximate>k__BackingField
	bool ___U3CIsPositionApproximateU3Ek__BackingField_7;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.BaseController::<IsRotationAvailable>k__BackingField
	bool ___U3CIsRotationAvailableU3Ek__BackingField_8;
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[] Microsoft.MixedReality.Toolkit.Input.BaseController::<Interactions>k__BackingField
	MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* ___U3CInteractionsU3Ek__BackingField_9;
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.BaseController::<AngularVelocity>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CAngularVelocityU3Ek__BackingField_10;
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.BaseController::<Velocity>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CVelocityU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CDefinitionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BaseController_t838F2F1E5179222D8747EBA84A91579901943F76, ___U3CDefinitionU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CDefinitionU3Ek__BackingField_0() const { return ___U3CDefinitionU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CDefinitionU3Ek__BackingField_0() { return &___U3CDefinitionU3Ek__BackingField_0; }
	inline void set_U3CDefinitionU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CDefinitionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDefinitionU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CEnabledU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BaseController_t838F2F1E5179222D8747EBA84A91579901943F76, ___U3CEnabledU3Ek__BackingField_1)); }
	inline bool get_U3CEnabledU3Ek__BackingField_1() const { return ___U3CEnabledU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CEnabledU3Ek__BackingField_1() { return &___U3CEnabledU3Ek__BackingField_1; }
	inline void set_U3CEnabledU3Ek__BackingField_1(bool value)
	{
		___U3CEnabledU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTrackingStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BaseController_t838F2F1E5179222D8747EBA84A91579901943F76, ___U3CTrackingStateU3Ek__BackingField_2)); }
	inline int32_t get_U3CTrackingStateU3Ek__BackingField_2() const { return ___U3CTrackingStateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CTrackingStateU3Ek__BackingField_2() { return &___U3CTrackingStateU3Ek__BackingField_2; }
	inline void set_U3CTrackingStateU3Ek__BackingField_2(int32_t value)
	{
		___U3CTrackingStateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CControllerHandednessU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BaseController_t838F2F1E5179222D8747EBA84A91579901943F76, ___U3CControllerHandednessU3Ek__BackingField_3)); }
	inline uint8_t get_U3CControllerHandednessU3Ek__BackingField_3() const { return ___U3CControllerHandednessU3Ek__BackingField_3; }
	inline uint8_t* get_address_of_U3CControllerHandednessU3Ek__BackingField_3() { return &___U3CControllerHandednessU3Ek__BackingField_3; }
	inline void set_U3CControllerHandednessU3Ek__BackingField_3(uint8_t value)
	{
		___U3CControllerHandednessU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CInputSourceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BaseController_t838F2F1E5179222D8747EBA84A91579901943F76, ___U3CInputSourceU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CInputSourceU3Ek__BackingField_4() const { return ___U3CInputSourceU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CInputSourceU3Ek__BackingField_4() { return &___U3CInputSourceU3Ek__BackingField_4; }
	inline void set_U3CInputSourceU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CInputSourceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInputSourceU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CVisualizerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BaseController_t838F2F1E5179222D8747EBA84A91579901943F76, ___U3CVisualizerU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CVisualizerU3Ek__BackingField_5() const { return ___U3CVisualizerU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CVisualizerU3Ek__BackingField_5() { return &___U3CVisualizerU3Ek__BackingField_5; }
	inline void set_U3CVisualizerU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CVisualizerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CVisualizerU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsPositionAvailableU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BaseController_t838F2F1E5179222D8747EBA84A91579901943F76, ___U3CIsPositionAvailableU3Ek__BackingField_6)); }
	inline bool get_U3CIsPositionAvailableU3Ek__BackingField_6() const { return ___U3CIsPositionAvailableU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsPositionAvailableU3Ek__BackingField_6() { return &___U3CIsPositionAvailableU3Ek__BackingField_6; }
	inline void set_U3CIsPositionAvailableU3Ek__BackingField_6(bool value)
	{
		___U3CIsPositionAvailableU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIsPositionApproximateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(BaseController_t838F2F1E5179222D8747EBA84A91579901943F76, ___U3CIsPositionApproximateU3Ek__BackingField_7)); }
	inline bool get_U3CIsPositionApproximateU3Ek__BackingField_7() const { return ___U3CIsPositionApproximateU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsPositionApproximateU3Ek__BackingField_7() { return &___U3CIsPositionApproximateU3Ek__BackingField_7; }
	inline void set_U3CIsPositionApproximateU3Ek__BackingField_7(bool value)
	{
		___U3CIsPositionApproximateU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsRotationAvailableU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BaseController_t838F2F1E5179222D8747EBA84A91579901943F76, ___U3CIsRotationAvailableU3Ek__BackingField_8)); }
	inline bool get_U3CIsRotationAvailableU3Ek__BackingField_8() const { return ___U3CIsRotationAvailableU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsRotationAvailableU3Ek__BackingField_8() { return &___U3CIsRotationAvailableU3Ek__BackingField_8; }
	inline void set_U3CIsRotationAvailableU3Ek__BackingField_8(bool value)
	{
		___U3CIsRotationAvailableU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CInteractionsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(BaseController_t838F2F1E5179222D8747EBA84A91579901943F76, ___U3CInteractionsU3Ek__BackingField_9)); }
	inline MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* get_U3CInteractionsU3Ek__BackingField_9() const { return ___U3CInteractionsU3Ek__BackingField_9; }
	inline MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375** get_address_of_U3CInteractionsU3Ek__BackingField_9() { return &___U3CInteractionsU3Ek__BackingField_9; }
	inline void set_U3CInteractionsU3Ek__BackingField_9(MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* value)
	{
		___U3CInteractionsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInteractionsU3Ek__BackingField_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAngularVelocityU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(BaseController_t838F2F1E5179222D8747EBA84A91579901943F76, ___U3CAngularVelocityU3Ek__BackingField_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CAngularVelocityU3Ek__BackingField_10() const { return ___U3CAngularVelocityU3Ek__BackingField_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CAngularVelocityU3Ek__BackingField_10() { return &___U3CAngularVelocityU3Ek__BackingField_10; }
	inline void set_U3CAngularVelocityU3Ek__BackingField_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CAngularVelocityU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CVelocityU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(BaseController_t838F2F1E5179222D8747EBA84A91579901943F76, ___U3CVelocityU3Ek__BackingField_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CVelocityU3Ek__BackingField_11() const { return ___U3CVelocityU3Ek__BackingField_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CVelocityU3Ek__BackingField_11() { return &___U3CVelocityU3Ek__BackingField_11; }
	inline void set_U3CVelocityU3Ek__BackingField_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CVelocityU3Ek__BackingField_11 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Input.BaseInputSourceDefinition
struct BaseInputSourceDefinition_t36140FA79FA8E7F98B8767451A5A6F779BA9DE60  : public RuntimeObject
{
public:
	// Microsoft.MixedReality.Toolkit.Utilities.Handedness Microsoft.MixedReality.Toolkit.Input.BaseInputSourceDefinition::<Handedness>k__BackingField
	uint8_t ___U3CHandednessU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CHandednessU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BaseInputSourceDefinition_t36140FA79FA8E7F98B8767451A5A6F779BA9DE60, ___U3CHandednessU3Ek__BackingField_0)); }
	inline uint8_t get_U3CHandednessU3Ek__BackingField_0() const { return ___U3CHandednessU3Ek__BackingField_0; }
	inline uint8_t* get_address_of_U3CHandednessU3Ek__BackingField_0() { return &___U3CHandednessU3Ek__BackingField_0; }
	inline void set_U3CHandednessU3Ek__BackingField_0(uint8_t value)
	{
		___U3CHandednessU3Ek__BackingField_0 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction
struct MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE 
{
public:
	// System.UInt32 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction::id
	uint32_t ___id_1;
	// System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction::description
	String_t* ___description_2;
	// Microsoft.MixedReality.Toolkit.Utilities.AxisType Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction::axisConstraint
	int32_t ___axisConstraint_3;

public:
	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE, ___id_1)); }
	inline uint32_t get_id_1() const { return ___id_1; }
	inline uint32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(uint32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE, ___description_2)); }
	inline String_t* get_description_2() const { return ___description_2; }
	inline String_t** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(String_t* value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___description_2), (void*)value);
	}

	inline static int32_t get_offset_of_axisConstraint_3() { return static_cast<int32_t>(offsetof(MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE, ___axisConstraint_3)); }
	inline int32_t get_axisConstraint_3() const { return ___axisConstraint_3; }
	inline int32_t* get_address_of_axisConstraint_3() { return &___axisConstraint_3; }
	inline void set_axisConstraint_3(int32_t value)
	{
		___axisConstraint_3 = value;
	}
};

struct MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE_StaticFields
{
public:
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction::<None>k__BackingField
	MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  ___U3CNoneU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNoneU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE_StaticFields, ___U3CNoneU3Ek__BackingField_0)); }
	inline MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  get_U3CNoneU3Ek__BackingField_0() const { return ___U3CNoneU3Ek__BackingField_0; }
	inline MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE * get_address_of_U3CNoneU3Ek__BackingField_0() { return &___U3CNoneU3Ek__BackingField_0; }
	inline void set_U3CNoneU3Ek__BackingField_0(MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  value)
	{
		___U3CNoneU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CNoneU3Ek__BackingField_0))->___description_2), (void*)NULL);
	}
};

// Native definition for P/Invoke marshalling of Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction
struct MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE_marshaled_pinvoke
{
	uint32_t ___id_1;
	char* ___description_2;
	int32_t ___axisConstraint_3;
};
// Native definition for COM marshalling of Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction
struct MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE_marshaled_com
{
	uint32_t ___id_1;
	Il2CppChar* ___description_2;
	int32_t ___axisConstraint_3;
};

// OVRBone
struct OVRBone_tF9C031559703607035E455CE642549C722BBF081  : public RuntimeObject
{
public:
	// OVRSkeleton/BoneId OVRBone::<Id>k__BackingField
	int32_t ___U3CIdU3Ek__BackingField_0;
	// System.Int16 OVRBone::<ParentBoneIndex>k__BackingField
	int16_t ___U3CParentBoneIndexU3Ek__BackingField_1;
	// UnityEngine.Transform OVRBone::<Transform>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CTransformU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OVRBone_tF9C031559703607035E455CE642549C722BBF081, ___U3CIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CParentBoneIndexU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OVRBone_tF9C031559703607035E455CE642549C722BBF081, ___U3CParentBoneIndexU3Ek__BackingField_1)); }
	inline int16_t get_U3CParentBoneIndexU3Ek__BackingField_1() const { return ___U3CParentBoneIndexU3Ek__BackingField_1; }
	inline int16_t* get_address_of_U3CParentBoneIndexU3Ek__BackingField_1() { return &___U3CParentBoneIndexU3Ek__BackingField_1; }
	inline void set_U3CParentBoneIndexU3Ek__BackingField_1(int16_t value)
	{
		___U3CParentBoneIndexU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTransformU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OVRBone_tF9C031559703607035E455CE642549C722BBF081, ___U3CTransformU3Ek__BackingField_2)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CTransformU3Ek__BackingField_2() const { return ___U3CTransformU3Ek__BackingField_2; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CTransformU3Ek__BackingField_2() { return &___U3CTransformU3Ek__BackingField_2; }
	inline void set_U3CTransformU3Ek__BackingField_2(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CTransformU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTransformU3Ek__BackingField_2), (void*)value);
	}
};


// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// OVRPlugin/HandState
struct HandState_t5377A12318B95003B2BB256777077FE3D05D0090 
{
public:
	// OVRPlugin/HandStatus OVRPlugin/HandState::Status
	int32_t ___Status_0;
	// OVRPlugin/Posef OVRPlugin/HandState::RootPose
	Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  ___RootPose_1;
	// OVRPlugin/Quatf[] OVRPlugin/HandState::BoneRotations
	QuatfU5BU5D_t652B5A0EAB7922962EBF168B1FC099D8873D11D5* ___BoneRotations_2;
	// OVRPlugin/HandFingerPinch OVRPlugin/HandState::Pinches
	int32_t ___Pinches_3;
	// System.Single[] OVRPlugin/HandState::PinchStrength
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___PinchStrength_4;
	// OVRPlugin/Posef OVRPlugin/HandState::PointerPose
	Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  ___PointerPose_5;
	// System.Single OVRPlugin/HandState::HandScale
	float ___HandScale_6;
	// OVRPlugin/TrackingConfidence OVRPlugin/HandState::HandConfidence
	int32_t ___HandConfidence_7;
	// OVRPlugin/TrackingConfidence[] OVRPlugin/HandState::FingerConfidences
	TrackingConfidenceU5BU5D_t568C8F700A41BD4F365DD5A0155BEB20A3289322* ___FingerConfidences_8;
	// System.Double OVRPlugin/HandState::RequestedTimeStamp
	double ___RequestedTimeStamp_9;
	// System.Double OVRPlugin/HandState::SampleTimeStamp
	double ___SampleTimeStamp_10;

public:
	inline static int32_t get_offset_of_Status_0() { return static_cast<int32_t>(offsetof(HandState_t5377A12318B95003B2BB256777077FE3D05D0090, ___Status_0)); }
	inline int32_t get_Status_0() const { return ___Status_0; }
	inline int32_t* get_address_of_Status_0() { return &___Status_0; }
	inline void set_Status_0(int32_t value)
	{
		___Status_0 = value;
	}

	inline static int32_t get_offset_of_RootPose_1() { return static_cast<int32_t>(offsetof(HandState_t5377A12318B95003B2BB256777077FE3D05D0090, ___RootPose_1)); }
	inline Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  get_RootPose_1() const { return ___RootPose_1; }
	inline Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952 * get_address_of_RootPose_1() { return &___RootPose_1; }
	inline void set_RootPose_1(Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  value)
	{
		___RootPose_1 = value;
	}

	inline static int32_t get_offset_of_BoneRotations_2() { return static_cast<int32_t>(offsetof(HandState_t5377A12318B95003B2BB256777077FE3D05D0090, ___BoneRotations_2)); }
	inline QuatfU5BU5D_t652B5A0EAB7922962EBF168B1FC099D8873D11D5* get_BoneRotations_2() const { return ___BoneRotations_2; }
	inline QuatfU5BU5D_t652B5A0EAB7922962EBF168B1FC099D8873D11D5** get_address_of_BoneRotations_2() { return &___BoneRotations_2; }
	inline void set_BoneRotations_2(QuatfU5BU5D_t652B5A0EAB7922962EBF168B1FC099D8873D11D5* value)
	{
		___BoneRotations_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BoneRotations_2), (void*)value);
	}

	inline static int32_t get_offset_of_Pinches_3() { return static_cast<int32_t>(offsetof(HandState_t5377A12318B95003B2BB256777077FE3D05D0090, ___Pinches_3)); }
	inline int32_t get_Pinches_3() const { return ___Pinches_3; }
	inline int32_t* get_address_of_Pinches_3() { return &___Pinches_3; }
	inline void set_Pinches_3(int32_t value)
	{
		___Pinches_3 = value;
	}

	inline static int32_t get_offset_of_PinchStrength_4() { return static_cast<int32_t>(offsetof(HandState_t5377A12318B95003B2BB256777077FE3D05D0090, ___PinchStrength_4)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_PinchStrength_4() const { return ___PinchStrength_4; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_PinchStrength_4() { return &___PinchStrength_4; }
	inline void set_PinchStrength_4(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___PinchStrength_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PinchStrength_4), (void*)value);
	}

	inline static int32_t get_offset_of_PointerPose_5() { return static_cast<int32_t>(offsetof(HandState_t5377A12318B95003B2BB256777077FE3D05D0090, ___PointerPose_5)); }
	inline Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  get_PointerPose_5() const { return ___PointerPose_5; }
	inline Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952 * get_address_of_PointerPose_5() { return &___PointerPose_5; }
	inline void set_PointerPose_5(Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  value)
	{
		___PointerPose_5 = value;
	}

	inline static int32_t get_offset_of_HandScale_6() { return static_cast<int32_t>(offsetof(HandState_t5377A12318B95003B2BB256777077FE3D05D0090, ___HandScale_6)); }
	inline float get_HandScale_6() const { return ___HandScale_6; }
	inline float* get_address_of_HandScale_6() { return &___HandScale_6; }
	inline void set_HandScale_6(float value)
	{
		___HandScale_6 = value;
	}

	inline static int32_t get_offset_of_HandConfidence_7() { return static_cast<int32_t>(offsetof(HandState_t5377A12318B95003B2BB256777077FE3D05D0090, ___HandConfidence_7)); }
	inline int32_t get_HandConfidence_7() const { return ___HandConfidence_7; }
	inline int32_t* get_address_of_HandConfidence_7() { return &___HandConfidence_7; }
	inline void set_HandConfidence_7(int32_t value)
	{
		___HandConfidence_7 = value;
	}

	inline static int32_t get_offset_of_FingerConfidences_8() { return static_cast<int32_t>(offsetof(HandState_t5377A12318B95003B2BB256777077FE3D05D0090, ___FingerConfidences_8)); }
	inline TrackingConfidenceU5BU5D_t568C8F700A41BD4F365DD5A0155BEB20A3289322* get_FingerConfidences_8() const { return ___FingerConfidences_8; }
	inline TrackingConfidenceU5BU5D_t568C8F700A41BD4F365DD5A0155BEB20A3289322** get_address_of_FingerConfidences_8() { return &___FingerConfidences_8; }
	inline void set_FingerConfidences_8(TrackingConfidenceU5BU5D_t568C8F700A41BD4F365DD5A0155BEB20A3289322* value)
	{
		___FingerConfidences_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FingerConfidences_8), (void*)value);
	}

	inline static int32_t get_offset_of_RequestedTimeStamp_9() { return static_cast<int32_t>(offsetof(HandState_t5377A12318B95003B2BB256777077FE3D05D0090, ___RequestedTimeStamp_9)); }
	inline double get_RequestedTimeStamp_9() const { return ___RequestedTimeStamp_9; }
	inline double* get_address_of_RequestedTimeStamp_9() { return &___RequestedTimeStamp_9; }
	inline void set_RequestedTimeStamp_9(double value)
	{
		___RequestedTimeStamp_9 = value;
	}

	inline static int32_t get_offset_of_SampleTimeStamp_10() { return static_cast<int32_t>(offsetof(HandState_t5377A12318B95003B2BB256777077FE3D05D0090, ___SampleTimeStamp_10)); }
	inline double get_SampleTimeStamp_10() const { return ___SampleTimeStamp_10; }
	inline double* get_address_of_SampleTimeStamp_10() { return &___SampleTimeStamp_10; }
	inline void set_SampleTimeStamp_10(double value)
	{
		___SampleTimeStamp_10 = value;
	}
};

// Native definition for P/Invoke marshalling of OVRPlugin/HandState
struct HandState_t5377A12318B95003B2BB256777077FE3D05D0090_marshaled_pinvoke
{
	int32_t ___Status_0;
	Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  ___RootPose_1;
	Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D * ___BoneRotations_2;
	int32_t ___Pinches_3;
	Il2CppSafeArray/*NONE*/* ___PinchStrength_4;
	Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  ___PointerPose_5;
	float ___HandScale_6;
	int32_t ___HandConfidence_7;
	int32_t* ___FingerConfidences_8;
	double ___RequestedTimeStamp_9;
	double ___SampleTimeStamp_10;
};
// Native definition for COM marshalling of OVRPlugin/HandState
struct HandState_t5377A12318B95003B2BB256777077FE3D05D0090_marshaled_com
{
	int32_t ___Status_0;
	Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  ___RootPose_1;
	Quatf_t62062FC7CE72F55D499CA6BFF7EF122D2C6AF42D * ___BoneRotations_2;
	int32_t ___Pinches_3;
	Il2CppSafeArray/*NONE*/* ___PinchStrength_4;
	Posef_tAB560ABDD92D837B4DF63F6FAEE32A1E1144B952  ___PointerPose_5;
	float ___HandScale_6;
	int32_t ___HandConfidence_7;
	int32_t* ___FingerConfidences_8;
	double ___RequestedTimeStamp_9;
	double ___SampleTimeStamp_10;
};

// OVRPlugin/Skeleton2
struct Skeleton2_tBCE168B418608021C1551D640F39DF88F1282E13 
{
public:
	// OVRPlugin/SkeletonType OVRPlugin/Skeleton2::Type
	int32_t ___Type_0;
	// System.UInt32 OVRPlugin/Skeleton2::NumBones
	uint32_t ___NumBones_1;
	// System.UInt32 OVRPlugin/Skeleton2::NumBoneCapsules
	uint32_t ___NumBoneCapsules_2;
	// OVRPlugin/Bone[] OVRPlugin/Skeleton2::Bones
	BoneU5BU5D_t1288E05740DDAC86F76F4CD2019C35A17E72C981* ___Bones_3;
	// OVRPlugin/BoneCapsule[] OVRPlugin/Skeleton2::BoneCapsules
	BoneCapsuleU5BU5D_tB4D9AC5CF59206C13FBD0A3725C45C1F6EBCE295* ___BoneCapsules_4;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(Skeleton2_tBCE168B418608021C1551D640F39DF88F1282E13, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_NumBones_1() { return static_cast<int32_t>(offsetof(Skeleton2_tBCE168B418608021C1551D640F39DF88F1282E13, ___NumBones_1)); }
	inline uint32_t get_NumBones_1() const { return ___NumBones_1; }
	inline uint32_t* get_address_of_NumBones_1() { return &___NumBones_1; }
	inline void set_NumBones_1(uint32_t value)
	{
		___NumBones_1 = value;
	}

	inline static int32_t get_offset_of_NumBoneCapsules_2() { return static_cast<int32_t>(offsetof(Skeleton2_tBCE168B418608021C1551D640F39DF88F1282E13, ___NumBoneCapsules_2)); }
	inline uint32_t get_NumBoneCapsules_2() const { return ___NumBoneCapsules_2; }
	inline uint32_t* get_address_of_NumBoneCapsules_2() { return &___NumBoneCapsules_2; }
	inline void set_NumBoneCapsules_2(uint32_t value)
	{
		___NumBoneCapsules_2 = value;
	}

	inline static int32_t get_offset_of_Bones_3() { return static_cast<int32_t>(offsetof(Skeleton2_tBCE168B418608021C1551D640F39DF88F1282E13, ___Bones_3)); }
	inline BoneU5BU5D_t1288E05740DDAC86F76F4CD2019C35A17E72C981* get_Bones_3() const { return ___Bones_3; }
	inline BoneU5BU5D_t1288E05740DDAC86F76F4CD2019C35A17E72C981** get_address_of_Bones_3() { return &___Bones_3; }
	inline void set_Bones_3(BoneU5BU5D_t1288E05740DDAC86F76F4CD2019C35A17E72C981* value)
	{
		___Bones_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Bones_3), (void*)value);
	}

	inline static int32_t get_offset_of_BoneCapsules_4() { return static_cast<int32_t>(offsetof(Skeleton2_tBCE168B418608021C1551D640F39DF88F1282E13, ___BoneCapsules_4)); }
	inline BoneCapsuleU5BU5D_tB4D9AC5CF59206C13FBD0A3725C45C1F6EBCE295* get_BoneCapsules_4() const { return ___BoneCapsules_4; }
	inline BoneCapsuleU5BU5D_tB4D9AC5CF59206C13FBD0A3725C45C1F6EBCE295** get_address_of_BoneCapsules_4() { return &___BoneCapsules_4; }
	inline void set_BoneCapsules_4(BoneCapsuleU5BU5D_tB4D9AC5CF59206C13FBD0A3725C45C1F6EBCE295* value)
	{
		___BoneCapsules_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BoneCapsules_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of OVRPlugin/Skeleton2
struct Skeleton2_tBCE168B418608021C1551D640F39DF88F1282E13_marshaled_pinvoke
{
	int32_t ___Type_0;
	uint32_t ___NumBones_1;
	uint32_t ___NumBoneCapsules_2;
	Bone_tFB367C47EBF89DA78C7AA2A09C8D24427723B500 * ___Bones_3;
	BoneCapsule_t482B1E40C3A6FD1C90BAF5B3A24719B196071B07 * ___BoneCapsules_4;
};
// Native definition for COM marshalling of OVRPlugin/Skeleton2
struct Skeleton2_tBCE168B418608021C1551D640F39DF88F1282E13_marshaled_com
{
	int32_t ___Type_0;
	uint32_t ___NumBones_1;
	uint32_t ___NumBoneCapsules_2;
	Bone_tFB367C47EBF89DA78C7AA2A09C8D24427723B500 * ___Bones_3;
	BoneCapsule_t482B1E40C3A6FD1C90BAF5B3A24719B196071B07 * ___BoneCapsules_4;
};

// Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition
struct ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5  : public BaseInputSourceDefinition_t36140FA79FA8E7F98B8767451A5A6F779BA9DE60
{
public:
	// Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::<InputSource>k__BackingField
	RuntimeObject* ___U3CInputSourceU3Ek__BackingField_1;
	// System.Single Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::cursorBeamBackwardTolerance
	float ___cursorBeamBackwardTolerance_2;
	// System.Single Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::cursorBeamUpTolerance
	float ___cursorBeamUpTolerance_3;
	// System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose> Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::unityJointPoses
	Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * ___unityJointPoses_4;
	// Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::currentIndexPose
	MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  ___currentIndexPose_5;
	// System.Single Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::minimumPinchDistance
	float ___minimumPinchDistance_6;
	// System.Single Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::maximumPinchDistance
	float ___maximumPinchDistance_7;
	// System.Single Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::enterPinchDistance
	float ___enterPinchDistance_8;
	// System.Single Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::exitPinchDistance
	float ___exitPinchDistance_9;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::isPinching
	bool ___isPinching_10;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::isIndexGrabbing
	bool ___isIndexGrabbing_11;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::isMiddleGrabbing
	bool ___isMiddleGrabbing_12;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::isThumbGrabbing
	bool ___isThumbGrabbing_13;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::previousReadyToTeleport
	bool ___previousReadyToTeleport_16;
	// Microsoft.MixedReality.Toolkit.Input.IMixedRealityTeleportPointer Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::teleportPointer
	RuntimeObject* ___teleportPointer_17;

public:
	inline static int32_t get_offset_of_U3CInputSourceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___U3CInputSourceU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CInputSourceU3Ek__BackingField_1() const { return ___U3CInputSourceU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CInputSourceU3Ek__BackingField_1() { return &___U3CInputSourceU3Ek__BackingField_1; }
	inline void set_U3CInputSourceU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CInputSourceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInputSourceU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_cursorBeamBackwardTolerance_2() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___cursorBeamBackwardTolerance_2)); }
	inline float get_cursorBeamBackwardTolerance_2() const { return ___cursorBeamBackwardTolerance_2; }
	inline float* get_address_of_cursorBeamBackwardTolerance_2() { return &___cursorBeamBackwardTolerance_2; }
	inline void set_cursorBeamBackwardTolerance_2(float value)
	{
		___cursorBeamBackwardTolerance_2 = value;
	}

	inline static int32_t get_offset_of_cursorBeamUpTolerance_3() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___cursorBeamUpTolerance_3)); }
	inline float get_cursorBeamUpTolerance_3() const { return ___cursorBeamUpTolerance_3; }
	inline float* get_address_of_cursorBeamUpTolerance_3() { return &___cursorBeamUpTolerance_3; }
	inline void set_cursorBeamUpTolerance_3(float value)
	{
		___cursorBeamUpTolerance_3 = value;
	}

	inline static int32_t get_offset_of_unityJointPoses_4() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___unityJointPoses_4)); }
	inline Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * get_unityJointPoses_4() const { return ___unityJointPoses_4; }
	inline Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 ** get_address_of_unityJointPoses_4() { return &___unityJointPoses_4; }
	inline void set_unityJointPoses_4(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * value)
	{
		___unityJointPoses_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unityJointPoses_4), (void*)value);
	}

	inline static int32_t get_offset_of_currentIndexPose_5() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___currentIndexPose_5)); }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  get_currentIndexPose_5() const { return ___currentIndexPose_5; }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * get_address_of_currentIndexPose_5() { return &___currentIndexPose_5; }
	inline void set_currentIndexPose_5(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  value)
	{
		___currentIndexPose_5 = value;
	}

	inline static int32_t get_offset_of_minimumPinchDistance_6() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___minimumPinchDistance_6)); }
	inline float get_minimumPinchDistance_6() const { return ___minimumPinchDistance_6; }
	inline float* get_address_of_minimumPinchDistance_6() { return &___minimumPinchDistance_6; }
	inline void set_minimumPinchDistance_6(float value)
	{
		___minimumPinchDistance_6 = value;
	}

	inline static int32_t get_offset_of_maximumPinchDistance_7() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___maximumPinchDistance_7)); }
	inline float get_maximumPinchDistance_7() const { return ___maximumPinchDistance_7; }
	inline float* get_address_of_maximumPinchDistance_7() { return &___maximumPinchDistance_7; }
	inline void set_maximumPinchDistance_7(float value)
	{
		___maximumPinchDistance_7 = value;
	}

	inline static int32_t get_offset_of_enterPinchDistance_8() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___enterPinchDistance_8)); }
	inline float get_enterPinchDistance_8() const { return ___enterPinchDistance_8; }
	inline float* get_address_of_enterPinchDistance_8() { return &___enterPinchDistance_8; }
	inline void set_enterPinchDistance_8(float value)
	{
		___enterPinchDistance_8 = value;
	}

	inline static int32_t get_offset_of_exitPinchDistance_9() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___exitPinchDistance_9)); }
	inline float get_exitPinchDistance_9() const { return ___exitPinchDistance_9; }
	inline float* get_address_of_exitPinchDistance_9() { return &___exitPinchDistance_9; }
	inline void set_exitPinchDistance_9(float value)
	{
		___exitPinchDistance_9 = value;
	}

	inline static int32_t get_offset_of_isPinching_10() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___isPinching_10)); }
	inline bool get_isPinching_10() const { return ___isPinching_10; }
	inline bool* get_address_of_isPinching_10() { return &___isPinching_10; }
	inline void set_isPinching_10(bool value)
	{
		___isPinching_10 = value;
	}

	inline static int32_t get_offset_of_isIndexGrabbing_11() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___isIndexGrabbing_11)); }
	inline bool get_isIndexGrabbing_11() const { return ___isIndexGrabbing_11; }
	inline bool* get_address_of_isIndexGrabbing_11() { return &___isIndexGrabbing_11; }
	inline void set_isIndexGrabbing_11(bool value)
	{
		___isIndexGrabbing_11 = value;
	}

	inline static int32_t get_offset_of_isMiddleGrabbing_12() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___isMiddleGrabbing_12)); }
	inline bool get_isMiddleGrabbing_12() const { return ___isMiddleGrabbing_12; }
	inline bool* get_address_of_isMiddleGrabbing_12() { return &___isMiddleGrabbing_12; }
	inline void set_isMiddleGrabbing_12(bool value)
	{
		___isMiddleGrabbing_12 = value;
	}

	inline static int32_t get_offset_of_isThumbGrabbing_13() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___isThumbGrabbing_13)); }
	inline bool get_isThumbGrabbing_13() const { return ___isThumbGrabbing_13; }
	inline bool* get_address_of_isThumbGrabbing_13() { return &___isThumbGrabbing_13; }
	inline void set_isThumbGrabbing_13(bool value)
	{
		___isThumbGrabbing_13 = value;
	}

	inline static int32_t get_offset_of_previousReadyToTeleport_16() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___previousReadyToTeleport_16)); }
	inline bool get_previousReadyToTeleport_16() const { return ___previousReadyToTeleport_16; }
	inline bool* get_address_of_previousReadyToTeleport_16() { return &___previousReadyToTeleport_16; }
	inline void set_previousReadyToTeleport_16(bool value)
	{
		___previousReadyToTeleport_16 = value;
	}

	inline static int32_t get_offset_of_teleportPointer_17() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5, ___teleportPointer_17)); }
	inline RuntimeObject* get_teleportPointer_17() const { return ___teleportPointer_17; }
	inline RuntimeObject** get_address_of_teleportPointer_17() { return &___teleportPointer_17; }
	inline void set_teleportPointer_17(RuntimeObject* value)
	{
		___teleportPointer_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___teleportPointer_17), (void*)value);
	}
};

struct ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5_StaticFields
{
public:
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::UpdateHandJointsPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UpdateHandJointsPerfMarker_14;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::UpdateCurrentIndexPosePerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UpdateCurrentIndexPosePerfMarker_15;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::UpdateCurrentTeleportPosePerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UpdateCurrentTeleportPosePerfMarker_18;

public:
	inline static int32_t get_offset_of_UpdateHandJointsPerfMarker_14() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5_StaticFields, ___UpdateHandJointsPerfMarker_14)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UpdateHandJointsPerfMarker_14() const { return ___UpdateHandJointsPerfMarker_14; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UpdateHandJointsPerfMarker_14() { return &___UpdateHandJointsPerfMarker_14; }
	inline void set_UpdateHandJointsPerfMarker_14(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UpdateHandJointsPerfMarker_14 = value;
	}

	inline static int32_t get_offset_of_UpdateCurrentIndexPosePerfMarker_15() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5_StaticFields, ___UpdateCurrentIndexPosePerfMarker_15)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UpdateCurrentIndexPosePerfMarker_15() const { return ___UpdateCurrentIndexPosePerfMarker_15; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UpdateCurrentIndexPosePerfMarker_15() { return &___UpdateCurrentIndexPosePerfMarker_15; }
	inline void set_UpdateCurrentIndexPosePerfMarker_15(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UpdateCurrentIndexPosePerfMarker_15 = value;
	}

	inline static int32_t get_offset_of_UpdateCurrentTeleportPosePerfMarker_18() { return static_cast<int32_t>(offsetof(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5_StaticFields, ___UpdateCurrentTeleportPosePerfMarker_18)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UpdateCurrentTeleportPosePerfMarker_18() const { return ___UpdateCurrentTeleportPosePerfMarker_18; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UpdateCurrentTeleportPosePerfMarker_18() { return &___UpdateCurrentTeleportPosePerfMarker_18; }
	inline void set_UpdateCurrentTeleportPosePerfMarker_18(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UpdateCurrentTeleportPosePerfMarker_18 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Input.BaseHand
struct BaseHand_t44436345C300F7B303FBC825541C6EF243DCCE1D  : public BaseController_t838F2F1E5179222D8747EBA84A91579901943F76
{
public:
	// Microsoft.MixedReality.Toolkit.Input.IHandRay Microsoft.MixedReality.Toolkit.Input.BaseHand::<HandRay>k__BackingField
	RuntimeObject* ___U3CHandRayU3Ek__BackingField_12;
	// System.Single Microsoft.MixedReality.Toolkit.Input.BaseHand::deltaTimeStart
	float ___deltaTimeStart_13;
	// System.Int32 Microsoft.MixedReality.Toolkit.Input.BaseHand::frameOn
	int32_t ___frameOn_15;
	// UnityEngine.Vector3[] Microsoft.MixedReality.Toolkit.Input.BaseHand::velocityPositionsCache
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___velocityPositionsCache_16;
	// UnityEngine.Vector3[] Microsoft.MixedReality.Toolkit.Input.BaseHand::velocityNormalsCache
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___velocityNormalsCache_17;
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.BaseHand::velocityPositionsSum
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___velocityPositionsSum_18;
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.BaseHand::velocityNormalsSum
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___velocityNormalsSum_19;

public:
	inline static int32_t get_offset_of_U3CHandRayU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(BaseHand_t44436345C300F7B303FBC825541C6EF243DCCE1D, ___U3CHandRayU3Ek__BackingField_12)); }
	inline RuntimeObject* get_U3CHandRayU3Ek__BackingField_12() const { return ___U3CHandRayU3Ek__BackingField_12; }
	inline RuntimeObject** get_address_of_U3CHandRayU3Ek__BackingField_12() { return &___U3CHandRayU3Ek__BackingField_12; }
	inline void set_U3CHandRayU3Ek__BackingField_12(RuntimeObject* value)
	{
		___U3CHandRayU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CHandRayU3Ek__BackingField_12), (void*)value);
	}

	inline static int32_t get_offset_of_deltaTimeStart_13() { return static_cast<int32_t>(offsetof(BaseHand_t44436345C300F7B303FBC825541C6EF243DCCE1D, ___deltaTimeStart_13)); }
	inline float get_deltaTimeStart_13() const { return ___deltaTimeStart_13; }
	inline float* get_address_of_deltaTimeStart_13() { return &___deltaTimeStart_13; }
	inline void set_deltaTimeStart_13(float value)
	{
		___deltaTimeStart_13 = value;
	}

	inline static int32_t get_offset_of_frameOn_15() { return static_cast<int32_t>(offsetof(BaseHand_t44436345C300F7B303FBC825541C6EF243DCCE1D, ___frameOn_15)); }
	inline int32_t get_frameOn_15() const { return ___frameOn_15; }
	inline int32_t* get_address_of_frameOn_15() { return &___frameOn_15; }
	inline void set_frameOn_15(int32_t value)
	{
		___frameOn_15 = value;
	}

	inline static int32_t get_offset_of_velocityPositionsCache_16() { return static_cast<int32_t>(offsetof(BaseHand_t44436345C300F7B303FBC825541C6EF243DCCE1D, ___velocityPositionsCache_16)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_velocityPositionsCache_16() const { return ___velocityPositionsCache_16; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_velocityPositionsCache_16() { return &___velocityPositionsCache_16; }
	inline void set_velocityPositionsCache_16(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___velocityPositionsCache_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___velocityPositionsCache_16), (void*)value);
	}

	inline static int32_t get_offset_of_velocityNormalsCache_17() { return static_cast<int32_t>(offsetof(BaseHand_t44436345C300F7B303FBC825541C6EF243DCCE1D, ___velocityNormalsCache_17)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_velocityNormalsCache_17() const { return ___velocityNormalsCache_17; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_velocityNormalsCache_17() { return &___velocityNormalsCache_17; }
	inline void set_velocityNormalsCache_17(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___velocityNormalsCache_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___velocityNormalsCache_17), (void*)value);
	}

	inline static int32_t get_offset_of_velocityPositionsSum_18() { return static_cast<int32_t>(offsetof(BaseHand_t44436345C300F7B303FBC825541C6EF243DCCE1D, ___velocityPositionsSum_18)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_velocityPositionsSum_18() const { return ___velocityPositionsSum_18; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_velocityPositionsSum_18() { return &___velocityPositionsSum_18; }
	inline void set_velocityPositionsSum_18(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___velocityPositionsSum_18 = value;
	}

	inline static int32_t get_offset_of_velocityNormalsSum_19() { return static_cast<int32_t>(offsetof(BaseHand_t44436345C300F7B303FBC825541C6EF243DCCE1D, ___velocityNormalsSum_19)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_velocityNormalsSum_19() const { return ___velocityNormalsSum_19; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_velocityNormalsSum_19() { return &___velocityNormalsSum_19; }
	inline void set_velocityNormalsSum_19(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___velocityNormalsSum_19 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Input.BaseInputDeviceManager
struct BaseInputDeviceManager_t8793A6A8DB7F36F2123A5AE2B0572DEFC07501CD  : public BaseDataProvider_1_t1D6E5ED6946EB7AF9582569B8384D8800F0BBE34
{
public:
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.BaseInputDeviceManager::enablePointerCache
	bool ___enablePointerCache_10;
	// Microsoft.MixedReality.Toolkit.Input.BaseInputDeviceManager/PointerConfig[] Microsoft.MixedReality.Toolkit.Input.BaseInputDeviceManager::pointerConfigurations
	PointerConfigU5BU5D_tD4B506A11EDD45B4BB995B8B1ABC7932AEF3DC82* ___pointerConfigurations_11;
	// System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,System.UInt32> Microsoft.MixedReality.Toolkit.Input.BaseInputDeviceManager::activePointersToConfig
	Dictionary_2_t665131D7AD1ECCB861C41FC1A9F7B01D5386FDC3 * ___activePointersToConfig_13;

public:
	inline static int32_t get_offset_of_enablePointerCache_10() { return static_cast<int32_t>(offsetof(BaseInputDeviceManager_t8793A6A8DB7F36F2123A5AE2B0572DEFC07501CD, ___enablePointerCache_10)); }
	inline bool get_enablePointerCache_10() const { return ___enablePointerCache_10; }
	inline bool* get_address_of_enablePointerCache_10() { return &___enablePointerCache_10; }
	inline void set_enablePointerCache_10(bool value)
	{
		___enablePointerCache_10 = value;
	}

	inline static int32_t get_offset_of_pointerConfigurations_11() { return static_cast<int32_t>(offsetof(BaseInputDeviceManager_t8793A6A8DB7F36F2123A5AE2B0572DEFC07501CD, ___pointerConfigurations_11)); }
	inline PointerConfigU5BU5D_tD4B506A11EDD45B4BB995B8B1ABC7932AEF3DC82* get_pointerConfigurations_11() const { return ___pointerConfigurations_11; }
	inline PointerConfigU5BU5D_tD4B506A11EDD45B4BB995B8B1ABC7932AEF3DC82** get_address_of_pointerConfigurations_11() { return &___pointerConfigurations_11; }
	inline void set_pointerConfigurations_11(PointerConfigU5BU5D_tD4B506A11EDD45B4BB995B8B1ABC7932AEF3DC82* value)
	{
		___pointerConfigurations_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointerConfigurations_11), (void*)value);
	}

	inline static int32_t get_offset_of_activePointersToConfig_13() { return static_cast<int32_t>(offsetof(BaseInputDeviceManager_t8793A6A8DB7F36F2123A5AE2B0572DEFC07501CD, ___activePointersToConfig_13)); }
	inline Dictionary_2_t665131D7AD1ECCB861C41FC1A9F7B01D5386FDC3 * get_activePointersToConfig_13() const { return ___activePointersToConfig_13; }
	inline Dictionary_2_t665131D7AD1ECCB861C41FC1A9F7B01D5386FDC3 ** get_address_of_activePointersToConfig_13() { return &___activePointersToConfig_13; }
	inline void set_activePointersToConfig_13(Dictionary_2_t665131D7AD1ECCB861C41FC1A9F7B01D5386FDC3 * value)
	{
		___activePointersToConfig_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___activePointersToConfig_13), (void*)value);
	}
};

struct BaseInputDeviceManager_t8793A6A8DB7F36F2123A5AE2B0572DEFC07501CD_StaticFields
{
public:
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Input.BaseInputDeviceManager::RequestPointersPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___RequestPointersPerfMarker_12;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Input.BaseInputDeviceManager::RecyclePointersPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___RecyclePointersPerfMarker_14;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Input.BaseInputDeviceManager::CreatePointerPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___CreatePointerPerfMarker_15;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.Input.BaseInputDeviceManager::CleanActivePointersPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___CleanActivePointersPerfMarker_16;

public:
	inline static int32_t get_offset_of_RequestPointersPerfMarker_12() { return static_cast<int32_t>(offsetof(BaseInputDeviceManager_t8793A6A8DB7F36F2123A5AE2B0572DEFC07501CD_StaticFields, ___RequestPointersPerfMarker_12)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_RequestPointersPerfMarker_12() const { return ___RequestPointersPerfMarker_12; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_RequestPointersPerfMarker_12() { return &___RequestPointersPerfMarker_12; }
	inline void set_RequestPointersPerfMarker_12(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___RequestPointersPerfMarker_12 = value;
	}

	inline static int32_t get_offset_of_RecyclePointersPerfMarker_14() { return static_cast<int32_t>(offsetof(BaseInputDeviceManager_t8793A6A8DB7F36F2123A5AE2B0572DEFC07501CD_StaticFields, ___RecyclePointersPerfMarker_14)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_RecyclePointersPerfMarker_14() const { return ___RecyclePointersPerfMarker_14; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_RecyclePointersPerfMarker_14() { return &___RecyclePointersPerfMarker_14; }
	inline void set_RecyclePointersPerfMarker_14(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___RecyclePointersPerfMarker_14 = value;
	}

	inline static int32_t get_offset_of_CreatePointerPerfMarker_15() { return static_cast<int32_t>(offsetof(BaseInputDeviceManager_t8793A6A8DB7F36F2123A5AE2B0572DEFC07501CD_StaticFields, ___CreatePointerPerfMarker_15)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_CreatePointerPerfMarker_15() const { return ___CreatePointerPerfMarker_15; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_CreatePointerPerfMarker_15() { return &___CreatePointerPerfMarker_15; }
	inline void set_CreatePointerPerfMarker_15(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___CreatePointerPerfMarker_15 = value;
	}

	inline static int32_t get_offset_of_CleanActivePointersPerfMarker_16() { return static_cast<int32_t>(offsetof(BaseInputDeviceManager_t8793A6A8DB7F36F2123A5AE2B0572DEFC07501CD_StaticFields, ___CleanActivePointersPerfMarker_16)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_CleanActivePointersPerfMarker_16() const { return ___CleanActivePointersPerfMarker_16; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_CleanActivePointersPerfMarker_16() { return &___CleanActivePointersPerfMarker_16; }
	inline void set_CleanActivePointersPerfMarker_16(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___CleanActivePointersPerfMarker_16 = value;
	}
};


// Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile
struct BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Boolean Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile::isCustomProfile
	bool ___isCustomProfile_4;

public:
	inline static int32_t get_offset_of_isCustomProfile_4() { return static_cast<int32_t>(offsetof(BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3, ___isCustomProfile_4)); }
	inline bool get_isCustomProfile_4() const { return ___isCustomProfile_4; }
	inline bool* get_address_of_isCustomProfile_4() { return &___isCustomProfile_4; }
	inline void set_isCustomProfile_4(bool value)
	{
		___isCustomProfile_4 = value;
	}
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController
struct GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4  : public BaseController_t838F2F1E5179222D8747EBA84A91579901943F76
{
public:
	// Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController::CurrentControllerPose
	MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  ___CurrentControllerPose_12;
	// Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController::LastControllerPose
	MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  ___LastControllerPose_13;
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController::CurrentControllerPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___CurrentControllerPosition_14;
	// UnityEngine.Quaternion Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController::CurrentControllerRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___CurrentControllerRotation_15;

public:
	inline static int32_t get_offset_of_CurrentControllerPose_12() { return static_cast<int32_t>(offsetof(GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4, ___CurrentControllerPose_12)); }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  get_CurrentControllerPose_12() const { return ___CurrentControllerPose_12; }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * get_address_of_CurrentControllerPose_12() { return &___CurrentControllerPose_12; }
	inline void set_CurrentControllerPose_12(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  value)
	{
		___CurrentControllerPose_12 = value;
	}

	inline static int32_t get_offset_of_LastControllerPose_13() { return static_cast<int32_t>(offsetof(GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4, ___LastControllerPose_13)); }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  get_LastControllerPose_13() const { return ___LastControllerPose_13; }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * get_address_of_LastControllerPose_13() { return &___LastControllerPose_13; }
	inline void set_LastControllerPose_13(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  value)
	{
		___LastControllerPose_13 = value;
	}

	inline static int32_t get_offset_of_CurrentControllerPosition_14() { return static_cast<int32_t>(offsetof(GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4, ___CurrentControllerPosition_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_CurrentControllerPosition_14() const { return ___CurrentControllerPosition_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_CurrentControllerPosition_14() { return &___CurrentControllerPosition_14; }
	inline void set_CurrentControllerPosition_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___CurrentControllerPosition_14 = value;
	}

	inline static int32_t get_offset_of_CurrentControllerRotation_15() { return static_cast<int32_t>(offsetof(GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4, ___CurrentControllerRotation_15)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_CurrentControllerRotation_15() const { return ___CurrentControllerRotation_15; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_CurrentControllerRotation_15() { return &___CurrentControllerRotation_15; }
	inline void set_CurrentControllerRotation_15(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___CurrentControllerRotation_15 = value;
	}
};

struct GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4_StaticFields
{
public:
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController::UpdateControllerPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UpdateControllerPerfMarker_16;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController::UpdateButtonDataPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UpdateButtonDataPerfMarker_17;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController::UpdateSingleAxisDataPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UpdateSingleAxisDataPerfMarker_18;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController::UpdateDualAxisDataPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UpdateDualAxisDataPerfMarker_19;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController::UpdatePoseDataPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UpdatePoseDataPerfMarker_20;

public:
	inline static int32_t get_offset_of_UpdateControllerPerfMarker_16() { return static_cast<int32_t>(offsetof(GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4_StaticFields, ___UpdateControllerPerfMarker_16)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UpdateControllerPerfMarker_16() const { return ___UpdateControllerPerfMarker_16; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UpdateControllerPerfMarker_16() { return &___UpdateControllerPerfMarker_16; }
	inline void set_UpdateControllerPerfMarker_16(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UpdateControllerPerfMarker_16 = value;
	}

	inline static int32_t get_offset_of_UpdateButtonDataPerfMarker_17() { return static_cast<int32_t>(offsetof(GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4_StaticFields, ___UpdateButtonDataPerfMarker_17)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UpdateButtonDataPerfMarker_17() const { return ___UpdateButtonDataPerfMarker_17; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UpdateButtonDataPerfMarker_17() { return &___UpdateButtonDataPerfMarker_17; }
	inline void set_UpdateButtonDataPerfMarker_17(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UpdateButtonDataPerfMarker_17 = value;
	}

	inline static int32_t get_offset_of_UpdateSingleAxisDataPerfMarker_18() { return static_cast<int32_t>(offsetof(GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4_StaticFields, ___UpdateSingleAxisDataPerfMarker_18)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UpdateSingleAxisDataPerfMarker_18() const { return ___UpdateSingleAxisDataPerfMarker_18; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UpdateSingleAxisDataPerfMarker_18() { return &___UpdateSingleAxisDataPerfMarker_18; }
	inline void set_UpdateSingleAxisDataPerfMarker_18(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UpdateSingleAxisDataPerfMarker_18 = value;
	}

	inline static int32_t get_offset_of_UpdateDualAxisDataPerfMarker_19() { return static_cast<int32_t>(offsetof(GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4_StaticFields, ___UpdateDualAxisDataPerfMarker_19)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UpdateDualAxisDataPerfMarker_19() const { return ___UpdateDualAxisDataPerfMarker_19; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UpdateDualAxisDataPerfMarker_19() { return &___UpdateDualAxisDataPerfMarker_19; }
	inline void set_UpdateDualAxisDataPerfMarker_19(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UpdateDualAxisDataPerfMarker_19 = value;
	}

	inline static int32_t get_offset_of_UpdatePoseDataPerfMarker_20() { return static_cast<int32_t>(offsetof(GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4_StaticFields, ___UpdatePoseDataPerfMarker_20)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UpdatePoseDataPerfMarker_20() const { return ___UpdatePoseDataPerfMarker_20; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UpdatePoseDataPerfMarker_20() { return &___UpdatePoseDataPerfMarker_20; }
	inline void set_UpdatePoseDataPerfMarker_20(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UpdatePoseDataPerfMarker_20 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping
struct MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505  : public RuntimeObject
{
public:
	// System.UInt32 Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::id
	uint32_t ___id_0;
	// System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::description
	String_t* ___description_1;
	// Microsoft.MixedReality.Toolkit.Utilities.AxisType Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::axisType
	int32_t ___axisType_2;
	// Microsoft.MixedReality.Toolkit.Input.DeviceInputType Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::inputType
	int32_t ___inputType_3;
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::inputAction
	MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  ___inputAction_4;
	// UnityEngine.KeyCode Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::keyCode
	int32_t ___keyCode_5;
	// System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::axisCodeX
	String_t* ___axisCodeX_6;
	// System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::axisCodeY
	String_t* ___axisCodeY_7;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::invertXAxis
	bool ___invertXAxis_8;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::invertYAxis
	bool ___invertYAxis_9;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::changed
	bool ___changed_10;
	// System.Object Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::rawData
	RuntimeObject * ___rawData_11;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::boolData
	bool ___boolData_12;
	// System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::floatData
	float ___floatData_13;
	// UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::vector2Data
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___vector2Data_14;
	// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::positionData
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positionData_15;
	// UnityEngine.Quaternion Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::rotationData
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotationData_16;
	// Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::poseData
	MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  ___poseData_17;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___id_0)); }
	inline uint32_t get_id_0() const { return ___id_0; }
	inline uint32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(uint32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_description_1() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___description_1)); }
	inline String_t* get_description_1() const { return ___description_1; }
	inline String_t** get_address_of_description_1() { return &___description_1; }
	inline void set_description_1(String_t* value)
	{
		___description_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___description_1), (void*)value);
	}

	inline static int32_t get_offset_of_axisType_2() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___axisType_2)); }
	inline int32_t get_axisType_2() const { return ___axisType_2; }
	inline int32_t* get_address_of_axisType_2() { return &___axisType_2; }
	inline void set_axisType_2(int32_t value)
	{
		___axisType_2 = value;
	}

	inline static int32_t get_offset_of_inputType_3() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___inputType_3)); }
	inline int32_t get_inputType_3() const { return ___inputType_3; }
	inline int32_t* get_address_of_inputType_3() { return &___inputType_3; }
	inline void set_inputType_3(int32_t value)
	{
		___inputType_3 = value;
	}

	inline static int32_t get_offset_of_inputAction_4() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___inputAction_4)); }
	inline MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  get_inputAction_4() const { return ___inputAction_4; }
	inline MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE * get_address_of_inputAction_4() { return &___inputAction_4; }
	inline void set_inputAction_4(MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  value)
	{
		___inputAction_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___inputAction_4))->___description_2), (void*)NULL);
	}

	inline static int32_t get_offset_of_keyCode_5() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___keyCode_5)); }
	inline int32_t get_keyCode_5() const { return ___keyCode_5; }
	inline int32_t* get_address_of_keyCode_5() { return &___keyCode_5; }
	inline void set_keyCode_5(int32_t value)
	{
		___keyCode_5 = value;
	}

	inline static int32_t get_offset_of_axisCodeX_6() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___axisCodeX_6)); }
	inline String_t* get_axisCodeX_6() const { return ___axisCodeX_6; }
	inline String_t** get_address_of_axisCodeX_6() { return &___axisCodeX_6; }
	inline void set_axisCodeX_6(String_t* value)
	{
		___axisCodeX_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___axisCodeX_6), (void*)value);
	}

	inline static int32_t get_offset_of_axisCodeY_7() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___axisCodeY_7)); }
	inline String_t* get_axisCodeY_7() const { return ___axisCodeY_7; }
	inline String_t** get_address_of_axisCodeY_7() { return &___axisCodeY_7; }
	inline void set_axisCodeY_7(String_t* value)
	{
		___axisCodeY_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___axisCodeY_7), (void*)value);
	}

	inline static int32_t get_offset_of_invertXAxis_8() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___invertXAxis_8)); }
	inline bool get_invertXAxis_8() const { return ___invertXAxis_8; }
	inline bool* get_address_of_invertXAxis_8() { return &___invertXAxis_8; }
	inline void set_invertXAxis_8(bool value)
	{
		___invertXAxis_8 = value;
	}

	inline static int32_t get_offset_of_invertYAxis_9() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___invertYAxis_9)); }
	inline bool get_invertYAxis_9() const { return ___invertYAxis_9; }
	inline bool* get_address_of_invertYAxis_9() { return &___invertYAxis_9; }
	inline void set_invertYAxis_9(bool value)
	{
		___invertYAxis_9 = value;
	}

	inline static int32_t get_offset_of_changed_10() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___changed_10)); }
	inline bool get_changed_10() const { return ___changed_10; }
	inline bool* get_address_of_changed_10() { return &___changed_10; }
	inline void set_changed_10(bool value)
	{
		___changed_10 = value;
	}

	inline static int32_t get_offset_of_rawData_11() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___rawData_11)); }
	inline RuntimeObject * get_rawData_11() const { return ___rawData_11; }
	inline RuntimeObject ** get_address_of_rawData_11() { return &___rawData_11; }
	inline void set_rawData_11(RuntimeObject * value)
	{
		___rawData_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rawData_11), (void*)value);
	}

	inline static int32_t get_offset_of_boolData_12() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___boolData_12)); }
	inline bool get_boolData_12() const { return ___boolData_12; }
	inline bool* get_address_of_boolData_12() { return &___boolData_12; }
	inline void set_boolData_12(bool value)
	{
		___boolData_12 = value;
	}

	inline static int32_t get_offset_of_floatData_13() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___floatData_13)); }
	inline float get_floatData_13() const { return ___floatData_13; }
	inline float* get_address_of_floatData_13() { return &___floatData_13; }
	inline void set_floatData_13(float value)
	{
		___floatData_13 = value;
	}

	inline static int32_t get_offset_of_vector2Data_14() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___vector2Data_14)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_vector2Data_14() const { return ___vector2Data_14; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_vector2Data_14() { return &___vector2Data_14; }
	inline void set_vector2Data_14(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___vector2Data_14 = value;
	}

	inline static int32_t get_offset_of_positionData_15() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___positionData_15)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positionData_15() const { return ___positionData_15; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positionData_15() { return &___positionData_15; }
	inline void set_positionData_15(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positionData_15 = value;
	}

	inline static int32_t get_offset_of_rotationData_16() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___rotationData_16)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_rotationData_16() const { return ___rotationData_16; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_rotationData_16() { return &___rotationData_16; }
	inline void set_rotationData_16(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___rotationData_16 = value;
	}

	inline static int32_t get_offset_of_poseData_17() { return static_cast<int32_t>(offsetof(MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505, ___poseData_17)); }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  get_poseData_17() const { return ___poseData_17; }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * get_address_of_poseData_17() { return &___poseData_17; }
	inline void set_poseData_17(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  value)
	{
		___poseData_17 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Input.OculusTouchControllerDefinition
struct OculusTouchControllerDefinition_t9C81F5CDBD5908C5D1CE58FB90A4375457C7F831  : public BaseInputSourceDefinition_t36140FA79FA8E7F98B8767451A5A6F779BA9DE60
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// Microsoft.MixedReality.Toolkit.Input.MixedRealityHandTrackingProfile
struct MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8  : public BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3
{
public:
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.MixedRealityHandTrackingProfile::jointPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___jointPrefab_5;
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.MixedRealityHandTrackingProfile::palmPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___palmPrefab_6;
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.MixedRealityHandTrackingProfile::fingertipPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___fingertipPrefab_7;
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.MixedRealityHandTrackingProfile::handMeshPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___handMeshPrefab_8;
	// Microsoft.MixedReality.Toolkit.Utilities.SupportedApplicationModes Microsoft.MixedReality.Toolkit.Input.MixedRealityHandTrackingProfile::handMeshVisualizationModes
	int32_t ___handMeshVisualizationModes_9;
	// Microsoft.MixedReality.Toolkit.Utilities.SupportedApplicationModes Microsoft.MixedReality.Toolkit.Input.MixedRealityHandTrackingProfile::handJointVisualizationModes
	int32_t ___handJointVisualizationModes_10;

public:
	inline static int32_t get_offset_of_jointPrefab_5() { return static_cast<int32_t>(offsetof(MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8, ___jointPrefab_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_jointPrefab_5() const { return ___jointPrefab_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_jointPrefab_5() { return &___jointPrefab_5; }
	inline void set_jointPrefab_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___jointPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jointPrefab_5), (void*)value);
	}

	inline static int32_t get_offset_of_palmPrefab_6() { return static_cast<int32_t>(offsetof(MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8, ___palmPrefab_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_palmPrefab_6() const { return ___palmPrefab_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_palmPrefab_6() { return &___palmPrefab_6; }
	inline void set_palmPrefab_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___palmPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___palmPrefab_6), (void*)value);
	}

	inline static int32_t get_offset_of_fingertipPrefab_7() { return static_cast<int32_t>(offsetof(MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8, ___fingertipPrefab_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_fingertipPrefab_7() const { return ___fingertipPrefab_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_fingertipPrefab_7() { return &___fingertipPrefab_7; }
	inline void set_fingertipPrefab_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___fingertipPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fingertipPrefab_7), (void*)value);
	}

	inline static int32_t get_offset_of_handMeshPrefab_8() { return static_cast<int32_t>(offsetof(MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8, ___handMeshPrefab_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_handMeshPrefab_8() const { return ___handMeshPrefab_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_handMeshPrefab_8() { return &___handMeshPrefab_8; }
	inline void set_handMeshPrefab_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___handMeshPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handMeshPrefab_8), (void*)value);
	}

	inline static int32_t get_offset_of_handMeshVisualizationModes_9() { return static_cast<int32_t>(offsetof(MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8, ___handMeshVisualizationModes_9)); }
	inline int32_t get_handMeshVisualizationModes_9() const { return ___handMeshVisualizationModes_9; }
	inline int32_t* get_address_of_handMeshVisualizationModes_9() { return &___handMeshVisualizationModes_9; }
	inline void set_handMeshVisualizationModes_9(int32_t value)
	{
		___handMeshVisualizationModes_9 = value;
	}

	inline static int32_t get_offset_of_handJointVisualizationModes_10() { return static_cast<int32_t>(offsetof(MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8, ___handJointVisualizationModes_10)); }
	inline int32_t get_handJointVisualizationModes_10() const { return ___handJointVisualizationModes_10; }
	inline int32_t* get_address_of_handJointVisualizationModes_10() { return &___handJointVisualizationModes_10; }
	inline void set_handJointVisualizationModes_10(int32_t value)
	{
		___handJointVisualizationModes_10 = value;
	}
};


// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile
struct MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C  : public BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3
{
public:
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputDataProviderConfiguration[] Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::dataProviderConfigurations
	MixedRealityInputDataProviderConfigurationU5BU5D_tACE18ABAB93C0C35F5703C34BFCEE34C58FE2107* ___dataProviderConfigurations_5;
	// Microsoft.MixedReality.Toolkit.Utilities.SystemType Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::focusProviderType
	SystemType_t5A8091D125CA72F271FAB6EE68A869F3628DBED5 * ___focusProviderType_6;
	// Microsoft.MixedReality.Toolkit.Utilities.SystemType Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::raycastProviderType
	SystemType_t5A8091D125CA72F271FAB6EE68A869F3628DBED5 * ___raycastProviderType_7;
	// System.Int32 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::focusQueryBufferSize
	int32_t ___focusQueryBufferSize_8;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::focusIndividualCompoundCollider
	bool ___focusIndividualCompoundCollider_9;
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputActionsProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::inputActionsProfile
	MixedRealityInputActionsProfile_tDA21846ABEF425362A21B447C04C8C199392AC82 * ___inputActionsProfile_10;
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputActionRulesProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::inputActionRulesProfile
	MixedRealityInputActionRulesProfile_tF2708C51AAF3894A860028C79DB05A44D6EDD64F * ___inputActionRulesProfile_11;
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::pointerProfile
	MixedRealityPointerProfile_t51E556B24A06CDA2552A8B38F3874757C70128FE * ___pointerProfile_12;
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityGesturesProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::gesturesProfile
	MixedRealityGesturesProfile_t344E8703F2DA838589B447BE14DC1C446D171395 * ___gesturesProfile_13;
	// System.Collections.Generic.List`1<System.Globalization.CultureInfo> Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::supportedVoiceCultures
	List_1_tB93032867275F6C802C3659FC027FF6FB1372359 * ___supportedVoiceCultures_14;
	// Microsoft.MixedReality.Toolkit.Input.MixedRealitySpeechCommandsProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::speechCommandsProfile
	MixedRealitySpeechCommandsProfile_tD8FD268D341E7DD3B3858B34A2CB2D902AA35DED * ___speechCommandsProfile_15;
	// System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::enableControllerMapping
	bool ___enableControllerMapping_16;
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityControllerMappingProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::controllerMappingProfile
	MixedRealityControllerMappingProfile_tA162449AD6794B68CC303CB68AC0EDE1B8BC72D8 * ___controllerMappingProfile_17;
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityControllerVisualizationProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::controllerVisualizationProfile
	MixedRealityControllerVisualizationProfile_t422A4125E237779F487A8BD82D7670928B885105 * ___controllerVisualizationProfile_18;
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityHandTrackingProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::handTrackingProfile
	MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 * ___handTrackingProfile_19;

public:
	inline static int32_t get_offset_of_dataProviderConfigurations_5() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___dataProviderConfigurations_5)); }
	inline MixedRealityInputDataProviderConfigurationU5BU5D_tACE18ABAB93C0C35F5703C34BFCEE34C58FE2107* get_dataProviderConfigurations_5() const { return ___dataProviderConfigurations_5; }
	inline MixedRealityInputDataProviderConfigurationU5BU5D_tACE18ABAB93C0C35F5703C34BFCEE34C58FE2107** get_address_of_dataProviderConfigurations_5() { return &___dataProviderConfigurations_5; }
	inline void set_dataProviderConfigurations_5(MixedRealityInputDataProviderConfigurationU5BU5D_tACE18ABAB93C0C35F5703C34BFCEE34C58FE2107* value)
	{
		___dataProviderConfigurations_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataProviderConfigurations_5), (void*)value);
	}

	inline static int32_t get_offset_of_focusProviderType_6() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___focusProviderType_6)); }
	inline SystemType_t5A8091D125CA72F271FAB6EE68A869F3628DBED5 * get_focusProviderType_6() const { return ___focusProviderType_6; }
	inline SystemType_t5A8091D125CA72F271FAB6EE68A869F3628DBED5 ** get_address_of_focusProviderType_6() { return &___focusProviderType_6; }
	inline void set_focusProviderType_6(SystemType_t5A8091D125CA72F271FAB6EE68A869F3628DBED5 * value)
	{
		___focusProviderType_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___focusProviderType_6), (void*)value);
	}

	inline static int32_t get_offset_of_raycastProviderType_7() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___raycastProviderType_7)); }
	inline SystemType_t5A8091D125CA72F271FAB6EE68A869F3628DBED5 * get_raycastProviderType_7() const { return ___raycastProviderType_7; }
	inline SystemType_t5A8091D125CA72F271FAB6EE68A869F3628DBED5 ** get_address_of_raycastProviderType_7() { return &___raycastProviderType_7; }
	inline void set_raycastProviderType_7(SystemType_t5A8091D125CA72F271FAB6EE68A869F3628DBED5 * value)
	{
		___raycastProviderType_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___raycastProviderType_7), (void*)value);
	}

	inline static int32_t get_offset_of_focusQueryBufferSize_8() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___focusQueryBufferSize_8)); }
	inline int32_t get_focusQueryBufferSize_8() const { return ___focusQueryBufferSize_8; }
	inline int32_t* get_address_of_focusQueryBufferSize_8() { return &___focusQueryBufferSize_8; }
	inline void set_focusQueryBufferSize_8(int32_t value)
	{
		___focusQueryBufferSize_8 = value;
	}

	inline static int32_t get_offset_of_focusIndividualCompoundCollider_9() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___focusIndividualCompoundCollider_9)); }
	inline bool get_focusIndividualCompoundCollider_9() const { return ___focusIndividualCompoundCollider_9; }
	inline bool* get_address_of_focusIndividualCompoundCollider_9() { return &___focusIndividualCompoundCollider_9; }
	inline void set_focusIndividualCompoundCollider_9(bool value)
	{
		___focusIndividualCompoundCollider_9 = value;
	}

	inline static int32_t get_offset_of_inputActionsProfile_10() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___inputActionsProfile_10)); }
	inline MixedRealityInputActionsProfile_tDA21846ABEF425362A21B447C04C8C199392AC82 * get_inputActionsProfile_10() const { return ___inputActionsProfile_10; }
	inline MixedRealityInputActionsProfile_tDA21846ABEF425362A21B447C04C8C199392AC82 ** get_address_of_inputActionsProfile_10() { return &___inputActionsProfile_10; }
	inline void set_inputActionsProfile_10(MixedRealityInputActionsProfile_tDA21846ABEF425362A21B447C04C8C199392AC82 * value)
	{
		___inputActionsProfile_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputActionsProfile_10), (void*)value);
	}

	inline static int32_t get_offset_of_inputActionRulesProfile_11() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___inputActionRulesProfile_11)); }
	inline MixedRealityInputActionRulesProfile_tF2708C51AAF3894A860028C79DB05A44D6EDD64F * get_inputActionRulesProfile_11() const { return ___inputActionRulesProfile_11; }
	inline MixedRealityInputActionRulesProfile_tF2708C51AAF3894A860028C79DB05A44D6EDD64F ** get_address_of_inputActionRulesProfile_11() { return &___inputActionRulesProfile_11; }
	inline void set_inputActionRulesProfile_11(MixedRealityInputActionRulesProfile_tF2708C51AAF3894A860028C79DB05A44D6EDD64F * value)
	{
		___inputActionRulesProfile_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputActionRulesProfile_11), (void*)value);
	}

	inline static int32_t get_offset_of_pointerProfile_12() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___pointerProfile_12)); }
	inline MixedRealityPointerProfile_t51E556B24A06CDA2552A8B38F3874757C70128FE * get_pointerProfile_12() const { return ___pointerProfile_12; }
	inline MixedRealityPointerProfile_t51E556B24A06CDA2552A8B38F3874757C70128FE ** get_address_of_pointerProfile_12() { return &___pointerProfile_12; }
	inline void set_pointerProfile_12(MixedRealityPointerProfile_t51E556B24A06CDA2552A8B38F3874757C70128FE * value)
	{
		___pointerProfile_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointerProfile_12), (void*)value);
	}

	inline static int32_t get_offset_of_gesturesProfile_13() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___gesturesProfile_13)); }
	inline MixedRealityGesturesProfile_t344E8703F2DA838589B447BE14DC1C446D171395 * get_gesturesProfile_13() const { return ___gesturesProfile_13; }
	inline MixedRealityGesturesProfile_t344E8703F2DA838589B447BE14DC1C446D171395 ** get_address_of_gesturesProfile_13() { return &___gesturesProfile_13; }
	inline void set_gesturesProfile_13(MixedRealityGesturesProfile_t344E8703F2DA838589B447BE14DC1C446D171395 * value)
	{
		___gesturesProfile_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gesturesProfile_13), (void*)value);
	}

	inline static int32_t get_offset_of_supportedVoiceCultures_14() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___supportedVoiceCultures_14)); }
	inline List_1_tB93032867275F6C802C3659FC027FF6FB1372359 * get_supportedVoiceCultures_14() const { return ___supportedVoiceCultures_14; }
	inline List_1_tB93032867275F6C802C3659FC027FF6FB1372359 ** get_address_of_supportedVoiceCultures_14() { return &___supportedVoiceCultures_14; }
	inline void set_supportedVoiceCultures_14(List_1_tB93032867275F6C802C3659FC027FF6FB1372359 * value)
	{
		___supportedVoiceCultures_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___supportedVoiceCultures_14), (void*)value);
	}

	inline static int32_t get_offset_of_speechCommandsProfile_15() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___speechCommandsProfile_15)); }
	inline MixedRealitySpeechCommandsProfile_tD8FD268D341E7DD3B3858B34A2CB2D902AA35DED * get_speechCommandsProfile_15() const { return ___speechCommandsProfile_15; }
	inline MixedRealitySpeechCommandsProfile_tD8FD268D341E7DD3B3858B34A2CB2D902AA35DED ** get_address_of_speechCommandsProfile_15() { return &___speechCommandsProfile_15; }
	inline void set_speechCommandsProfile_15(MixedRealitySpeechCommandsProfile_tD8FD268D341E7DD3B3858B34A2CB2D902AA35DED * value)
	{
		___speechCommandsProfile_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___speechCommandsProfile_15), (void*)value);
	}

	inline static int32_t get_offset_of_enableControllerMapping_16() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___enableControllerMapping_16)); }
	inline bool get_enableControllerMapping_16() const { return ___enableControllerMapping_16; }
	inline bool* get_address_of_enableControllerMapping_16() { return &___enableControllerMapping_16; }
	inline void set_enableControllerMapping_16(bool value)
	{
		___enableControllerMapping_16 = value;
	}

	inline static int32_t get_offset_of_controllerMappingProfile_17() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___controllerMappingProfile_17)); }
	inline MixedRealityControllerMappingProfile_tA162449AD6794B68CC303CB68AC0EDE1B8BC72D8 * get_controllerMappingProfile_17() const { return ___controllerMappingProfile_17; }
	inline MixedRealityControllerMappingProfile_tA162449AD6794B68CC303CB68AC0EDE1B8BC72D8 ** get_address_of_controllerMappingProfile_17() { return &___controllerMappingProfile_17; }
	inline void set_controllerMappingProfile_17(MixedRealityControllerMappingProfile_tA162449AD6794B68CC303CB68AC0EDE1B8BC72D8 * value)
	{
		___controllerMappingProfile_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controllerMappingProfile_17), (void*)value);
	}

	inline static int32_t get_offset_of_controllerVisualizationProfile_18() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___controllerVisualizationProfile_18)); }
	inline MixedRealityControllerVisualizationProfile_t422A4125E237779F487A8BD82D7670928B885105 * get_controllerVisualizationProfile_18() const { return ___controllerVisualizationProfile_18; }
	inline MixedRealityControllerVisualizationProfile_t422A4125E237779F487A8BD82D7670928B885105 ** get_address_of_controllerVisualizationProfile_18() { return &___controllerVisualizationProfile_18; }
	inline void set_controllerVisualizationProfile_18(MixedRealityControllerVisualizationProfile_t422A4125E237779F487A8BD82D7670928B885105 * value)
	{
		___controllerVisualizationProfile_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controllerVisualizationProfile_18), (void*)value);
	}

	inline static int32_t get_offset_of_handTrackingProfile_19() { return static_cast<int32_t>(offsetof(MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C, ___handTrackingProfile_19)); }
	inline MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 * get_handTrackingProfile_19() const { return ___handTrackingProfile_19; }
	inline MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 ** get_address_of_handTrackingProfile_19() { return &___handTrackingProfile_19; }
	inline void set_handTrackingProfile_19(MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 * value)
	{
		___handTrackingProfile_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handTrackingProfile_19), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand
struct OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4  : public BaseHand_t44436345C300F7B303FBC825541C6EF243DCCE1D
{
public:
	// Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::currentPointerPose
	MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  ___currentPointerPose_20;
	// Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::currentGripPose
	MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  ___currentGripPose_21;
	// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::isIndexGrabbing
	bool ___isIndexGrabbing_22;
	// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::isMiddleGrabbing
	bool ___isMiddleGrabbing_23;
	// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::settingsProfile
	OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * ___settingsProfile_24;
	// Microsoft.MixedReality.Toolkit.Input.MixedRealityHandTrackingProfile Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::handTrackingProfile
	MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 * ___handTrackingProfile_25;
	// System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose> Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::jointPoses
	Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * ___jointPoses_26;
	// Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::handDefinition
	ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * ___handDefinition_27;
	// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::<IsPinching>k__BackingField
	bool ___U3CIsPinchingU3Ek__BackingField_28;
	// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::<IsGrabbing>k__BackingField
	bool ___U3CIsGrabbingU3Ek__BackingField_29;
	// System.Collections.Generic.Dictionary`2<OVRSkeleton/BoneId,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint> Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::boneJointMapping
	Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * ___boneJointMapping_30;
	// System.Single Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::_lastHighConfidenceTime
	float ____lastHighConfidenceTime_31;

public:
	inline static int32_t get_offset_of_currentPointerPose_20() { return static_cast<int32_t>(offsetof(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4, ___currentPointerPose_20)); }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  get_currentPointerPose_20() const { return ___currentPointerPose_20; }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * get_address_of_currentPointerPose_20() { return &___currentPointerPose_20; }
	inline void set_currentPointerPose_20(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  value)
	{
		___currentPointerPose_20 = value;
	}

	inline static int32_t get_offset_of_currentGripPose_21() { return static_cast<int32_t>(offsetof(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4, ___currentGripPose_21)); }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  get_currentGripPose_21() const { return ___currentGripPose_21; }
	inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * get_address_of_currentGripPose_21() { return &___currentGripPose_21; }
	inline void set_currentGripPose_21(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  value)
	{
		___currentGripPose_21 = value;
	}

	inline static int32_t get_offset_of_isIndexGrabbing_22() { return static_cast<int32_t>(offsetof(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4, ___isIndexGrabbing_22)); }
	inline bool get_isIndexGrabbing_22() const { return ___isIndexGrabbing_22; }
	inline bool* get_address_of_isIndexGrabbing_22() { return &___isIndexGrabbing_22; }
	inline void set_isIndexGrabbing_22(bool value)
	{
		___isIndexGrabbing_22 = value;
	}

	inline static int32_t get_offset_of_isMiddleGrabbing_23() { return static_cast<int32_t>(offsetof(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4, ___isMiddleGrabbing_23)); }
	inline bool get_isMiddleGrabbing_23() const { return ___isMiddleGrabbing_23; }
	inline bool* get_address_of_isMiddleGrabbing_23() { return &___isMiddleGrabbing_23; }
	inline void set_isMiddleGrabbing_23(bool value)
	{
		___isMiddleGrabbing_23 = value;
	}

	inline static int32_t get_offset_of_settingsProfile_24() { return static_cast<int32_t>(offsetof(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4, ___settingsProfile_24)); }
	inline OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * get_settingsProfile_24() const { return ___settingsProfile_24; }
	inline OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 ** get_address_of_settingsProfile_24() { return &___settingsProfile_24; }
	inline void set_settingsProfile_24(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * value)
	{
		___settingsProfile_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___settingsProfile_24), (void*)value);
	}

	inline static int32_t get_offset_of_handTrackingProfile_25() { return static_cast<int32_t>(offsetof(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4, ___handTrackingProfile_25)); }
	inline MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 * get_handTrackingProfile_25() const { return ___handTrackingProfile_25; }
	inline MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 ** get_address_of_handTrackingProfile_25() { return &___handTrackingProfile_25; }
	inline void set_handTrackingProfile_25(MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 * value)
	{
		___handTrackingProfile_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handTrackingProfile_25), (void*)value);
	}

	inline static int32_t get_offset_of_jointPoses_26() { return static_cast<int32_t>(offsetof(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4, ___jointPoses_26)); }
	inline Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * get_jointPoses_26() const { return ___jointPoses_26; }
	inline Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 ** get_address_of_jointPoses_26() { return &___jointPoses_26; }
	inline void set_jointPoses_26(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * value)
	{
		___jointPoses_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jointPoses_26), (void*)value);
	}

	inline static int32_t get_offset_of_handDefinition_27() { return static_cast<int32_t>(offsetof(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4, ___handDefinition_27)); }
	inline ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * get_handDefinition_27() const { return ___handDefinition_27; }
	inline ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 ** get_address_of_handDefinition_27() { return &___handDefinition_27; }
	inline void set_handDefinition_27(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * value)
	{
		___handDefinition_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___handDefinition_27), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsPinchingU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4, ___U3CIsPinchingU3Ek__BackingField_28)); }
	inline bool get_U3CIsPinchingU3Ek__BackingField_28() const { return ___U3CIsPinchingU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CIsPinchingU3Ek__BackingField_28() { return &___U3CIsPinchingU3Ek__BackingField_28; }
	inline void set_U3CIsPinchingU3Ek__BackingField_28(bool value)
	{
		___U3CIsPinchingU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CIsGrabbingU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4, ___U3CIsGrabbingU3Ek__BackingField_29)); }
	inline bool get_U3CIsGrabbingU3Ek__BackingField_29() const { return ___U3CIsGrabbingU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CIsGrabbingU3Ek__BackingField_29() { return &___U3CIsGrabbingU3Ek__BackingField_29; }
	inline void set_U3CIsGrabbingU3Ek__BackingField_29(bool value)
	{
		___U3CIsGrabbingU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_boneJointMapping_30() { return static_cast<int32_t>(offsetof(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4, ___boneJointMapping_30)); }
	inline Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * get_boneJointMapping_30() const { return ___boneJointMapping_30; }
	inline Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B ** get_address_of_boneJointMapping_30() { return &___boneJointMapping_30; }
	inline void set_boneJointMapping_30(Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * value)
	{
		___boneJointMapping_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___boneJointMapping_30), (void*)value);
	}

	inline static int32_t get_offset_of__lastHighConfidenceTime_31() { return static_cast<int32_t>(offsetof(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4, ____lastHighConfidenceTime_31)); }
	inline float get__lastHighConfidenceTime_31() const { return ____lastHighConfidenceTime_31; }
	inline float* get_address_of__lastHighConfidenceTime_31() { return &____lastHighConfidenceTime_31; }
	inline void set__lastHighConfidenceTime_31(float value)
	{
		____lastHighConfidenceTime_31 = value;
	}
};


// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile
struct OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7  : public BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3
{
public:
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::ovrCameraRigPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ovrCameraRigPrefab_5;
	// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::renderAvatarHandsInsteadOfControllers
	bool ___renderAvatarHandsInsteadOfControllers_6;
	// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::localAvatarPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___localAvatarPrefab_7;
	// OVRHand/TrackingConfidence Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::minimumHandConfidence
	int32_t ___minimumHandConfidence_8;
	// OVRHand/TrackingConfidence Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::<CurrentLeftHandTrackingConfidence>k__BackingField
	int32_t ___U3CCurrentLeftHandTrackingConfidenceU3Ek__BackingField_9;
	// OVRHand/TrackingConfidence Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::<CurrentRightHandTrackingConfidence>k__BackingField
	int32_t ___U3CCurrentRightHandTrackingConfidenceU3Ek__BackingField_10;
	// System.Single Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::lowConfidenceTimeThreshold
	float ___lowConfidenceTimeThreshold_11;
	// System.Int32 Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::defaultCpuLevel
	int32_t ___defaultCpuLevel_12;
	// System.Int32 Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::defaultGpuLevel
	int32_t ___defaultGpuLevel_13;
	// System.Single Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::resolutionScale
	float ___resolutionScale_14;
	// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::useDynamicFixedFoveatedRendering
	bool ___useDynamicFixedFoveatedRendering_15;
	// OVRManager/FixedFoveatedRenderingLevel Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::fixedFoveatedRenderingLevel
	int32_t ___fixedFoveatedRenderingLevel_16;

public:
	inline static int32_t get_offset_of_ovrCameraRigPrefab_5() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7, ___ovrCameraRigPrefab_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ovrCameraRigPrefab_5() const { return ___ovrCameraRigPrefab_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ovrCameraRigPrefab_5() { return &___ovrCameraRigPrefab_5; }
	inline void set_ovrCameraRigPrefab_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ovrCameraRigPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ovrCameraRigPrefab_5), (void*)value);
	}

	inline static int32_t get_offset_of_renderAvatarHandsInsteadOfControllers_6() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7, ___renderAvatarHandsInsteadOfControllers_6)); }
	inline bool get_renderAvatarHandsInsteadOfControllers_6() const { return ___renderAvatarHandsInsteadOfControllers_6; }
	inline bool* get_address_of_renderAvatarHandsInsteadOfControllers_6() { return &___renderAvatarHandsInsteadOfControllers_6; }
	inline void set_renderAvatarHandsInsteadOfControllers_6(bool value)
	{
		___renderAvatarHandsInsteadOfControllers_6 = value;
	}

	inline static int32_t get_offset_of_localAvatarPrefab_7() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7, ___localAvatarPrefab_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_localAvatarPrefab_7() const { return ___localAvatarPrefab_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_localAvatarPrefab_7() { return &___localAvatarPrefab_7; }
	inline void set_localAvatarPrefab_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___localAvatarPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___localAvatarPrefab_7), (void*)value);
	}

	inline static int32_t get_offset_of_minimumHandConfidence_8() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7, ___minimumHandConfidence_8)); }
	inline int32_t get_minimumHandConfidence_8() const { return ___minimumHandConfidence_8; }
	inline int32_t* get_address_of_minimumHandConfidence_8() { return &___minimumHandConfidence_8; }
	inline void set_minimumHandConfidence_8(int32_t value)
	{
		___minimumHandConfidence_8 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentLeftHandTrackingConfidenceU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7, ___U3CCurrentLeftHandTrackingConfidenceU3Ek__BackingField_9)); }
	inline int32_t get_U3CCurrentLeftHandTrackingConfidenceU3Ek__BackingField_9() const { return ___U3CCurrentLeftHandTrackingConfidenceU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CCurrentLeftHandTrackingConfidenceU3Ek__BackingField_9() { return &___U3CCurrentLeftHandTrackingConfidenceU3Ek__BackingField_9; }
	inline void set_U3CCurrentLeftHandTrackingConfidenceU3Ek__BackingField_9(int32_t value)
	{
		___U3CCurrentLeftHandTrackingConfidenceU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentRightHandTrackingConfidenceU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7, ___U3CCurrentRightHandTrackingConfidenceU3Ek__BackingField_10)); }
	inline int32_t get_U3CCurrentRightHandTrackingConfidenceU3Ek__BackingField_10() const { return ___U3CCurrentRightHandTrackingConfidenceU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CCurrentRightHandTrackingConfidenceU3Ek__BackingField_10() { return &___U3CCurrentRightHandTrackingConfidenceU3Ek__BackingField_10; }
	inline void set_U3CCurrentRightHandTrackingConfidenceU3Ek__BackingField_10(int32_t value)
	{
		___U3CCurrentRightHandTrackingConfidenceU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_lowConfidenceTimeThreshold_11() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7, ___lowConfidenceTimeThreshold_11)); }
	inline float get_lowConfidenceTimeThreshold_11() const { return ___lowConfidenceTimeThreshold_11; }
	inline float* get_address_of_lowConfidenceTimeThreshold_11() { return &___lowConfidenceTimeThreshold_11; }
	inline void set_lowConfidenceTimeThreshold_11(float value)
	{
		___lowConfidenceTimeThreshold_11 = value;
	}

	inline static int32_t get_offset_of_defaultCpuLevel_12() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7, ___defaultCpuLevel_12)); }
	inline int32_t get_defaultCpuLevel_12() const { return ___defaultCpuLevel_12; }
	inline int32_t* get_address_of_defaultCpuLevel_12() { return &___defaultCpuLevel_12; }
	inline void set_defaultCpuLevel_12(int32_t value)
	{
		___defaultCpuLevel_12 = value;
	}

	inline static int32_t get_offset_of_defaultGpuLevel_13() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7, ___defaultGpuLevel_13)); }
	inline int32_t get_defaultGpuLevel_13() const { return ___defaultGpuLevel_13; }
	inline int32_t* get_address_of_defaultGpuLevel_13() { return &___defaultGpuLevel_13; }
	inline void set_defaultGpuLevel_13(int32_t value)
	{
		___defaultGpuLevel_13 = value;
	}

	inline static int32_t get_offset_of_resolutionScale_14() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7, ___resolutionScale_14)); }
	inline float get_resolutionScale_14() const { return ___resolutionScale_14; }
	inline float* get_address_of_resolutionScale_14() { return &___resolutionScale_14; }
	inline void set_resolutionScale_14(float value)
	{
		___resolutionScale_14 = value;
	}

	inline static int32_t get_offset_of_useDynamicFixedFoveatedRendering_15() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7, ___useDynamicFixedFoveatedRendering_15)); }
	inline bool get_useDynamicFixedFoveatedRendering_15() const { return ___useDynamicFixedFoveatedRendering_15; }
	inline bool* get_address_of_useDynamicFixedFoveatedRendering_15() { return &___useDynamicFixedFoveatedRendering_15; }
	inline void set_useDynamicFixedFoveatedRendering_15(bool value)
	{
		___useDynamicFixedFoveatedRendering_15 = value;
	}

	inline static int32_t get_offset_of_fixedFoveatedRenderingLevel_16() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7, ___fixedFoveatedRenderingLevel_16)); }
	inline int32_t get_fixedFoveatedRenderingLevel_16() const { return ___fixedFoveatedRenderingLevel_16; }
	inline int32_t* get_address_of_fixedFoveatedRenderingLevel_16() { return &___fixedFoveatedRenderingLevel_16; }
	inline void set_fixedFoveatedRenderingLevel_16(int32_t value)
	{
		___fixedFoveatedRenderingLevel_16 = value;
	}
};


// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKTouchController
struct OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED  : public GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4
{
public:

public:
};

struct OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_StaticFields
{
public:
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKTouchController::UpdateButtonDataPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UpdateButtonDataPerfMarker_21;

public:
	inline static int32_t get_offset_of_UpdateButtonDataPerfMarker_21() { return static_cast<int32_t>(offsetof(OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_StaticFields, ___UpdateButtonDataPerfMarker_21)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UpdateButtonDataPerfMarker_21() const { return ___UpdateButtonDataPerfMarker_21; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UpdateButtonDataPerfMarker_21() { return &___UpdateButtonDataPerfMarker_21; }
	inline void set_UpdateButtonDataPerfMarker_21(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UpdateButtonDataPerfMarker_21 = value;
	}
};


// Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager
struct XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7  : public BaseInputDeviceManager_t8793A6A8DB7F36F2123A5AE2B0572DEFC07501CD
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice> Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::inputDevices
	List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * ___inputDevices_18;
	// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice> Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::inputDevicesSubset
	List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * ___inputDevicesSubset_19;
	// System.Collections.Generic.List`1<UnityEngine.XR.InputDevice> Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::lastInputDevices
	List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * ___lastInputDevices_20;
	// System.Collections.Generic.List`1<UnityEngine.XR.InputDeviceCharacteristics> Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::<DesiredInputCharacteristics>k__BackingField
	List_1_tD812850D83CCFFD34E9A310E2AE62B198E513F2C * ___U3CDesiredInputCharacteristicsU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_inputDevices_18() { return static_cast<int32_t>(offsetof(XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7, ___inputDevices_18)); }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * get_inputDevices_18() const { return ___inputDevices_18; }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F ** get_address_of_inputDevices_18() { return &___inputDevices_18; }
	inline void set_inputDevices_18(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * value)
	{
		___inputDevices_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputDevices_18), (void*)value);
	}

	inline static int32_t get_offset_of_inputDevicesSubset_19() { return static_cast<int32_t>(offsetof(XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7, ___inputDevicesSubset_19)); }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * get_inputDevicesSubset_19() const { return ___inputDevicesSubset_19; }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F ** get_address_of_inputDevicesSubset_19() { return &___inputDevicesSubset_19; }
	inline void set_inputDevicesSubset_19(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * value)
	{
		___inputDevicesSubset_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputDevicesSubset_19), (void*)value);
	}

	inline static int32_t get_offset_of_lastInputDevices_20() { return static_cast<int32_t>(offsetof(XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7, ___lastInputDevices_20)); }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * get_lastInputDevices_20() const { return ___lastInputDevices_20; }
	inline List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F ** get_address_of_lastInputDevices_20() { return &___lastInputDevices_20; }
	inline void set_lastInputDevices_20(List_1_t476C8CC2E74FC5F7DE5B5CFE6830822665402F1F * value)
	{
		___lastInputDevices_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastInputDevices_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDesiredInputCharacteristicsU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7, ___U3CDesiredInputCharacteristicsU3Ek__BackingField_21)); }
	inline List_1_tD812850D83CCFFD34E9A310E2AE62B198E513F2C * get_U3CDesiredInputCharacteristicsU3Ek__BackingField_21() const { return ___U3CDesiredInputCharacteristicsU3Ek__BackingField_21; }
	inline List_1_tD812850D83CCFFD34E9A310E2AE62B198E513F2C ** get_address_of_U3CDesiredInputCharacteristicsU3Ek__BackingField_21() { return &___U3CDesiredInputCharacteristicsU3Ek__BackingField_21; }
	inline void set_U3CDesiredInputCharacteristicsU3Ek__BackingField_21(List_1_tD812850D83CCFFD34E9A310E2AE62B198E513F2C * value)
	{
		___U3CDesiredInputCharacteristicsU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDesiredInputCharacteristicsU3Ek__BackingField_21), (void*)value);
	}
};

struct XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.XR.InputDevice,Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController> Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::ActiveControllers
	Dictionary_2_t761F6D6973076571DFB4BC441C37BE195205B815 * ___ActiveControllers_17;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::UpdatePerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___UpdatePerfMarker_22;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::GetOrAddControllerPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___GetOrAddControllerPerfMarker_23;
	// Unity.Profiling.ProfilerMarker Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::RemoveControllerPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___RemoveControllerPerfMarker_24;

public:
	inline static int32_t get_offset_of_ActiveControllers_17() { return static_cast<int32_t>(offsetof(XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7_StaticFields, ___ActiveControllers_17)); }
	inline Dictionary_2_t761F6D6973076571DFB4BC441C37BE195205B815 * get_ActiveControllers_17() const { return ___ActiveControllers_17; }
	inline Dictionary_2_t761F6D6973076571DFB4BC441C37BE195205B815 ** get_address_of_ActiveControllers_17() { return &___ActiveControllers_17; }
	inline void set_ActiveControllers_17(Dictionary_2_t761F6D6973076571DFB4BC441C37BE195205B815 * value)
	{
		___ActiveControllers_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ActiveControllers_17), (void*)value);
	}

	inline static int32_t get_offset_of_UpdatePerfMarker_22() { return static_cast<int32_t>(offsetof(XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7_StaticFields, ___UpdatePerfMarker_22)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_UpdatePerfMarker_22() const { return ___UpdatePerfMarker_22; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_UpdatePerfMarker_22() { return &___UpdatePerfMarker_22; }
	inline void set_UpdatePerfMarker_22(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___UpdatePerfMarker_22 = value;
	}

	inline static int32_t get_offset_of_GetOrAddControllerPerfMarker_23() { return static_cast<int32_t>(offsetof(XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7_StaticFields, ___GetOrAddControllerPerfMarker_23)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_GetOrAddControllerPerfMarker_23() const { return ___GetOrAddControllerPerfMarker_23; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_GetOrAddControllerPerfMarker_23() { return &___GetOrAddControllerPerfMarker_23; }
	inline void set_GetOrAddControllerPerfMarker_23(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___GetOrAddControllerPerfMarker_23 = value;
	}

	inline static int32_t get_offset_of_RemoveControllerPerfMarker_24() { return static_cast<int32_t>(offsetof(XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7_StaticFields, ___RemoveControllerPerfMarker_24)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_RemoveControllerPerfMarker_24() const { return ___RemoveControllerPerfMarker_24; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_RemoveControllerPerfMarker_24() { return &___RemoveControllerPerfMarker_24; }
	inline void set_RemoveControllerPerfMarker_24(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___RemoveControllerPerfMarker_24 = value;
	}
};


// OVRCameraRig
struct OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform OVRCameraRig::<trackingSpace>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CtrackingSpaceU3Ek__BackingField_4;
	// UnityEngine.Transform OVRCameraRig::<leftEyeAnchor>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CleftEyeAnchorU3Ek__BackingField_5;
	// UnityEngine.Transform OVRCameraRig::<centerEyeAnchor>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CcenterEyeAnchorU3Ek__BackingField_6;
	// UnityEngine.Transform OVRCameraRig::<rightEyeAnchor>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CrightEyeAnchorU3Ek__BackingField_7;
	// UnityEngine.Transform OVRCameraRig::<leftHandAnchor>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CleftHandAnchorU3Ek__BackingField_8;
	// UnityEngine.Transform OVRCameraRig::<rightHandAnchor>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CrightHandAnchorU3Ek__BackingField_9;
	// UnityEngine.Transform OVRCameraRig::<leftControllerAnchor>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CleftControllerAnchorU3Ek__BackingField_10;
	// UnityEngine.Transform OVRCameraRig::<rightControllerAnchor>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CrightControllerAnchorU3Ek__BackingField_11;
	// UnityEngine.Transform OVRCameraRig::<trackerAnchor>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CtrackerAnchorU3Ek__BackingField_12;
	// System.Action`1<OVRCameraRig> OVRCameraRig::UpdatedAnchors
	Action_1_t75B6371B869E36A42B0DFFC8D8B2630F1D659A6C * ___UpdatedAnchors_13;
	// System.Boolean OVRCameraRig::usePerEyeCameras
	bool ___usePerEyeCameras_14;
	// System.Boolean OVRCameraRig::useFixedUpdateForTracking
	bool ___useFixedUpdateForTracking_15;
	// System.Boolean OVRCameraRig::disableEyeAnchorCameras
	bool ___disableEyeAnchorCameras_16;
	// System.Boolean OVRCameraRig::_skipUpdate
	bool ____skipUpdate_17;
	// System.String OVRCameraRig::trackingSpaceName
	String_t* ___trackingSpaceName_18;
	// System.String OVRCameraRig::trackerAnchorName
	String_t* ___trackerAnchorName_19;
	// System.String OVRCameraRig::leftEyeAnchorName
	String_t* ___leftEyeAnchorName_20;
	// System.String OVRCameraRig::centerEyeAnchorName
	String_t* ___centerEyeAnchorName_21;
	// System.String OVRCameraRig::rightEyeAnchorName
	String_t* ___rightEyeAnchorName_22;
	// System.String OVRCameraRig::leftHandAnchorName
	String_t* ___leftHandAnchorName_23;
	// System.String OVRCameraRig::rightHandAnchorName
	String_t* ___rightHandAnchorName_24;
	// System.String OVRCameraRig::leftControllerAnchorName
	String_t* ___leftControllerAnchorName_25;
	// System.String OVRCameraRig::rightControllerAnchorName
	String_t* ___rightControllerAnchorName_26;
	// UnityEngine.Camera OVRCameraRig::_centerEyeCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ____centerEyeCamera_27;
	// UnityEngine.Camera OVRCameraRig::_leftEyeCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ____leftEyeCamera_28;
	// UnityEngine.Camera OVRCameraRig::_rightEyeCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ____rightEyeCamera_29;

public:
	inline static int32_t get_offset_of_U3CtrackingSpaceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___U3CtrackingSpaceU3Ek__BackingField_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CtrackingSpaceU3Ek__BackingField_4() const { return ___U3CtrackingSpaceU3Ek__BackingField_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CtrackingSpaceU3Ek__BackingField_4() { return &___U3CtrackingSpaceU3Ek__BackingField_4; }
	inline void set_U3CtrackingSpaceU3Ek__BackingField_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CtrackingSpaceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtrackingSpaceU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CleftEyeAnchorU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___U3CleftEyeAnchorU3Ek__BackingField_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CleftEyeAnchorU3Ek__BackingField_5() const { return ___U3CleftEyeAnchorU3Ek__BackingField_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CleftEyeAnchorU3Ek__BackingField_5() { return &___U3CleftEyeAnchorU3Ek__BackingField_5; }
	inline void set_U3CleftEyeAnchorU3Ek__BackingField_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CleftEyeAnchorU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CleftEyeAnchorU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcenterEyeAnchorU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___U3CcenterEyeAnchorU3Ek__BackingField_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CcenterEyeAnchorU3Ek__BackingField_6() const { return ___U3CcenterEyeAnchorU3Ek__BackingField_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CcenterEyeAnchorU3Ek__BackingField_6() { return &___U3CcenterEyeAnchorU3Ek__BackingField_6; }
	inline void set_U3CcenterEyeAnchorU3Ek__BackingField_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CcenterEyeAnchorU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcenterEyeAnchorU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrightEyeAnchorU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___U3CrightEyeAnchorU3Ek__BackingField_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CrightEyeAnchorU3Ek__BackingField_7() const { return ___U3CrightEyeAnchorU3Ek__BackingField_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CrightEyeAnchorU3Ek__BackingField_7() { return &___U3CrightEyeAnchorU3Ek__BackingField_7; }
	inline void set_U3CrightEyeAnchorU3Ek__BackingField_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CrightEyeAnchorU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrightEyeAnchorU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CleftHandAnchorU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___U3CleftHandAnchorU3Ek__BackingField_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CleftHandAnchorU3Ek__BackingField_8() const { return ___U3CleftHandAnchorU3Ek__BackingField_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CleftHandAnchorU3Ek__BackingField_8() { return &___U3CleftHandAnchorU3Ek__BackingField_8; }
	inline void set_U3CleftHandAnchorU3Ek__BackingField_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CleftHandAnchorU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CleftHandAnchorU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrightHandAnchorU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___U3CrightHandAnchorU3Ek__BackingField_9)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CrightHandAnchorU3Ek__BackingField_9() const { return ___U3CrightHandAnchorU3Ek__BackingField_9; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CrightHandAnchorU3Ek__BackingField_9() { return &___U3CrightHandAnchorU3Ek__BackingField_9; }
	inline void set_U3CrightHandAnchorU3Ek__BackingField_9(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CrightHandAnchorU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrightHandAnchorU3Ek__BackingField_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CleftControllerAnchorU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___U3CleftControllerAnchorU3Ek__BackingField_10)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CleftControllerAnchorU3Ek__BackingField_10() const { return ___U3CleftControllerAnchorU3Ek__BackingField_10; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CleftControllerAnchorU3Ek__BackingField_10() { return &___U3CleftControllerAnchorU3Ek__BackingField_10; }
	inline void set_U3CleftControllerAnchorU3Ek__BackingField_10(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CleftControllerAnchorU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CleftControllerAnchorU3Ek__BackingField_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrightControllerAnchorU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___U3CrightControllerAnchorU3Ek__BackingField_11)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CrightControllerAnchorU3Ek__BackingField_11() const { return ___U3CrightControllerAnchorU3Ek__BackingField_11; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CrightControllerAnchorU3Ek__BackingField_11() { return &___U3CrightControllerAnchorU3Ek__BackingField_11; }
	inline void set_U3CrightControllerAnchorU3Ek__BackingField_11(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CrightControllerAnchorU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrightControllerAnchorU3Ek__BackingField_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtrackerAnchorU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___U3CtrackerAnchorU3Ek__BackingField_12)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CtrackerAnchorU3Ek__BackingField_12() const { return ___U3CtrackerAnchorU3Ek__BackingField_12; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CtrackerAnchorU3Ek__BackingField_12() { return &___U3CtrackerAnchorU3Ek__BackingField_12; }
	inline void set_U3CtrackerAnchorU3Ek__BackingField_12(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CtrackerAnchorU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtrackerAnchorU3Ek__BackingField_12), (void*)value);
	}

	inline static int32_t get_offset_of_UpdatedAnchors_13() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___UpdatedAnchors_13)); }
	inline Action_1_t75B6371B869E36A42B0DFFC8D8B2630F1D659A6C * get_UpdatedAnchors_13() const { return ___UpdatedAnchors_13; }
	inline Action_1_t75B6371B869E36A42B0DFFC8D8B2630F1D659A6C ** get_address_of_UpdatedAnchors_13() { return &___UpdatedAnchors_13; }
	inline void set_UpdatedAnchors_13(Action_1_t75B6371B869E36A42B0DFFC8D8B2630F1D659A6C * value)
	{
		___UpdatedAnchors_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UpdatedAnchors_13), (void*)value);
	}

	inline static int32_t get_offset_of_usePerEyeCameras_14() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___usePerEyeCameras_14)); }
	inline bool get_usePerEyeCameras_14() const { return ___usePerEyeCameras_14; }
	inline bool* get_address_of_usePerEyeCameras_14() { return &___usePerEyeCameras_14; }
	inline void set_usePerEyeCameras_14(bool value)
	{
		___usePerEyeCameras_14 = value;
	}

	inline static int32_t get_offset_of_useFixedUpdateForTracking_15() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___useFixedUpdateForTracking_15)); }
	inline bool get_useFixedUpdateForTracking_15() const { return ___useFixedUpdateForTracking_15; }
	inline bool* get_address_of_useFixedUpdateForTracking_15() { return &___useFixedUpdateForTracking_15; }
	inline void set_useFixedUpdateForTracking_15(bool value)
	{
		___useFixedUpdateForTracking_15 = value;
	}

	inline static int32_t get_offset_of_disableEyeAnchorCameras_16() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___disableEyeAnchorCameras_16)); }
	inline bool get_disableEyeAnchorCameras_16() const { return ___disableEyeAnchorCameras_16; }
	inline bool* get_address_of_disableEyeAnchorCameras_16() { return &___disableEyeAnchorCameras_16; }
	inline void set_disableEyeAnchorCameras_16(bool value)
	{
		___disableEyeAnchorCameras_16 = value;
	}

	inline static int32_t get_offset_of__skipUpdate_17() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ____skipUpdate_17)); }
	inline bool get__skipUpdate_17() const { return ____skipUpdate_17; }
	inline bool* get_address_of__skipUpdate_17() { return &____skipUpdate_17; }
	inline void set__skipUpdate_17(bool value)
	{
		____skipUpdate_17 = value;
	}

	inline static int32_t get_offset_of_trackingSpaceName_18() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___trackingSpaceName_18)); }
	inline String_t* get_trackingSpaceName_18() const { return ___trackingSpaceName_18; }
	inline String_t** get_address_of_trackingSpaceName_18() { return &___trackingSpaceName_18; }
	inline void set_trackingSpaceName_18(String_t* value)
	{
		___trackingSpaceName_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackingSpaceName_18), (void*)value);
	}

	inline static int32_t get_offset_of_trackerAnchorName_19() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___trackerAnchorName_19)); }
	inline String_t* get_trackerAnchorName_19() const { return ___trackerAnchorName_19; }
	inline String_t** get_address_of_trackerAnchorName_19() { return &___trackerAnchorName_19; }
	inline void set_trackerAnchorName_19(String_t* value)
	{
		___trackerAnchorName_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackerAnchorName_19), (void*)value);
	}

	inline static int32_t get_offset_of_leftEyeAnchorName_20() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___leftEyeAnchorName_20)); }
	inline String_t* get_leftEyeAnchorName_20() const { return ___leftEyeAnchorName_20; }
	inline String_t** get_address_of_leftEyeAnchorName_20() { return &___leftEyeAnchorName_20; }
	inline void set_leftEyeAnchorName_20(String_t* value)
	{
		___leftEyeAnchorName_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___leftEyeAnchorName_20), (void*)value);
	}

	inline static int32_t get_offset_of_centerEyeAnchorName_21() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___centerEyeAnchorName_21)); }
	inline String_t* get_centerEyeAnchorName_21() const { return ___centerEyeAnchorName_21; }
	inline String_t** get_address_of_centerEyeAnchorName_21() { return &___centerEyeAnchorName_21; }
	inline void set_centerEyeAnchorName_21(String_t* value)
	{
		___centerEyeAnchorName_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___centerEyeAnchorName_21), (void*)value);
	}

	inline static int32_t get_offset_of_rightEyeAnchorName_22() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___rightEyeAnchorName_22)); }
	inline String_t* get_rightEyeAnchorName_22() const { return ___rightEyeAnchorName_22; }
	inline String_t** get_address_of_rightEyeAnchorName_22() { return &___rightEyeAnchorName_22; }
	inline void set_rightEyeAnchorName_22(String_t* value)
	{
		___rightEyeAnchorName_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rightEyeAnchorName_22), (void*)value);
	}

	inline static int32_t get_offset_of_leftHandAnchorName_23() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___leftHandAnchorName_23)); }
	inline String_t* get_leftHandAnchorName_23() const { return ___leftHandAnchorName_23; }
	inline String_t** get_address_of_leftHandAnchorName_23() { return &___leftHandAnchorName_23; }
	inline void set_leftHandAnchorName_23(String_t* value)
	{
		___leftHandAnchorName_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___leftHandAnchorName_23), (void*)value);
	}

	inline static int32_t get_offset_of_rightHandAnchorName_24() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___rightHandAnchorName_24)); }
	inline String_t* get_rightHandAnchorName_24() const { return ___rightHandAnchorName_24; }
	inline String_t** get_address_of_rightHandAnchorName_24() { return &___rightHandAnchorName_24; }
	inline void set_rightHandAnchorName_24(String_t* value)
	{
		___rightHandAnchorName_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rightHandAnchorName_24), (void*)value);
	}

	inline static int32_t get_offset_of_leftControllerAnchorName_25() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___leftControllerAnchorName_25)); }
	inline String_t* get_leftControllerAnchorName_25() const { return ___leftControllerAnchorName_25; }
	inline String_t** get_address_of_leftControllerAnchorName_25() { return &___leftControllerAnchorName_25; }
	inline void set_leftControllerAnchorName_25(String_t* value)
	{
		___leftControllerAnchorName_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___leftControllerAnchorName_25), (void*)value);
	}

	inline static int32_t get_offset_of_rightControllerAnchorName_26() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ___rightControllerAnchorName_26)); }
	inline String_t* get_rightControllerAnchorName_26() const { return ___rightControllerAnchorName_26; }
	inline String_t** get_address_of_rightControllerAnchorName_26() { return &___rightControllerAnchorName_26; }
	inline void set_rightControllerAnchorName_26(String_t* value)
	{
		___rightControllerAnchorName_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rightControllerAnchorName_26), (void*)value);
	}

	inline static int32_t get_offset_of__centerEyeCamera_27() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ____centerEyeCamera_27)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get__centerEyeCamera_27() const { return ____centerEyeCamera_27; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of__centerEyeCamera_27() { return &____centerEyeCamera_27; }
	inline void set__centerEyeCamera_27(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		____centerEyeCamera_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____centerEyeCamera_27), (void*)value);
	}

	inline static int32_t get_offset_of__leftEyeCamera_28() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ____leftEyeCamera_28)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get__leftEyeCamera_28() const { return ____leftEyeCamera_28; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of__leftEyeCamera_28() { return &____leftEyeCamera_28; }
	inline void set__leftEyeCamera_28(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		____leftEyeCamera_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____leftEyeCamera_28), (void*)value);
	}

	inline static int32_t get_offset_of__rightEyeCamera_29() { return static_cast<int32_t>(offsetof(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517, ____rightEyeCamera_29)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get__rightEyeCamera_29() const { return ____rightEyeCamera_29; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of__rightEyeCamera_29() { return &____rightEyeCamera_29; }
	inline void set__rightEyeCamera_29(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		____rightEyeCamera_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rightEyeCamera_29), (void*)value);
	}
};


// OVRControllerHelper
struct OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject OVRControllerHelper::m_modelOculusTouchQuestAndRiftSLeftController
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_modelOculusTouchQuestAndRiftSLeftController_4;
	// UnityEngine.GameObject OVRControllerHelper::m_modelOculusTouchQuestAndRiftSRightController
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_modelOculusTouchQuestAndRiftSRightController_5;
	// UnityEngine.GameObject OVRControllerHelper::m_modelOculusTouchRiftLeftController
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_modelOculusTouchRiftLeftController_6;
	// UnityEngine.GameObject OVRControllerHelper::m_modelOculusTouchRiftRightController
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_modelOculusTouchRiftRightController_7;
	// UnityEngine.GameObject OVRControllerHelper::m_modelOculusTouchQuest2LeftController
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_modelOculusTouchQuest2LeftController_8;
	// UnityEngine.GameObject OVRControllerHelper::m_modelOculusTouchQuest2RightController
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_modelOculusTouchQuest2RightController_9;
	// OVRInput/Controller OVRControllerHelper::m_controller
	int32_t ___m_controller_10;
	// UnityEngine.Animator OVRControllerHelper::m_animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___m_animator_11;
	// OVRControllerHelper/ControllerType OVRControllerHelper::activeControllerType
	int32_t ___activeControllerType_12;
	// System.Boolean OVRControllerHelper::m_prevControllerConnected
	bool ___m_prevControllerConnected_13;
	// System.Boolean OVRControllerHelper::m_prevControllerConnectedCached
	bool ___m_prevControllerConnectedCached_14;

public:
	inline static int32_t get_offset_of_m_modelOculusTouchQuestAndRiftSLeftController_4() { return static_cast<int32_t>(offsetof(OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6, ___m_modelOculusTouchQuestAndRiftSLeftController_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_modelOculusTouchQuestAndRiftSLeftController_4() const { return ___m_modelOculusTouchQuestAndRiftSLeftController_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_modelOculusTouchQuestAndRiftSLeftController_4() { return &___m_modelOculusTouchQuestAndRiftSLeftController_4; }
	inline void set_m_modelOculusTouchQuestAndRiftSLeftController_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_modelOculusTouchQuestAndRiftSLeftController_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_modelOculusTouchQuestAndRiftSLeftController_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_modelOculusTouchQuestAndRiftSRightController_5() { return static_cast<int32_t>(offsetof(OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6, ___m_modelOculusTouchQuestAndRiftSRightController_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_modelOculusTouchQuestAndRiftSRightController_5() const { return ___m_modelOculusTouchQuestAndRiftSRightController_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_modelOculusTouchQuestAndRiftSRightController_5() { return &___m_modelOculusTouchQuestAndRiftSRightController_5; }
	inline void set_m_modelOculusTouchQuestAndRiftSRightController_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_modelOculusTouchQuestAndRiftSRightController_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_modelOculusTouchQuestAndRiftSRightController_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_modelOculusTouchRiftLeftController_6() { return static_cast<int32_t>(offsetof(OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6, ___m_modelOculusTouchRiftLeftController_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_modelOculusTouchRiftLeftController_6() const { return ___m_modelOculusTouchRiftLeftController_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_modelOculusTouchRiftLeftController_6() { return &___m_modelOculusTouchRiftLeftController_6; }
	inline void set_m_modelOculusTouchRiftLeftController_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_modelOculusTouchRiftLeftController_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_modelOculusTouchRiftLeftController_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_modelOculusTouchRiftRightController_7() { return static_cast<int32_t>(offsetof(OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6, ___m_modelOculusTouchRiftRightController_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_modelOculusTouchRiftRightController_7() const { return ___m_modelOculusTouchRiftRightController_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_modelOculusTouchRiftRightController_7() { return &___m_modelOculusTouchRiftRightController_7; }
	inline void set_m_modelOculusTouchRiftRightController_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_modelOculusTouchRiftRightController_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_modelOculusTouchRiftRightController_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_modelOculusTouchQuest2LeftController_8() { return static_cast<int32_t>(offsetof(OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6, ___m_modelOculusTouchQuest2LeftController_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_modelOculusTouchQuest2LeftController_8() const { return ___m_modelOculusTouchQuest2LeftController_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_modelOculusTouchQuest2LeftController_8() { return &___m_modelOculusTouchQuest2LeftController_8; }
	inline void set_m_modelOculusTouchQuest2LeftController_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_modelOculusTouchQuest2LeftController_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_modelOculusTouchQuest2LeftController_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_modelOculusTouchQuest2RightController_9() { return static_cast<int32_t>(offsetof(OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6, ___m_modelOculusTouchQuest2RightController_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_modelOculusTouchQuest2RightController_9() const { return ___m_modelOculusTouchQuest2RightController_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_modelOculusTouchQuest2RightController_9() { return &___m_modelOculusTouchQuest2RightController_9; }
	inline void set_m_modelOculusTouchQuest2RightController_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_modelOculusTouchQuest2RightController_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_modelOculusTouchQuest2RightController_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_controller_10() { return static_cast<int32_t>(offsetof(OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6, ___m_controller_10)); }
	inline int32_t get_m_controller_10() const { return ___m_controller_10; }
	inline int32_t* get_address_of_m_controller_10() { return &___m_controller_10; }
	inline void set_m_controller_10(int32_t value)
	{
		___m_controller_10 = value;
	}

	inline static int32_t get_offset_of_m_animator_11() { return static_cast<int32_t>(offsetof(OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6, ___m_animator_11)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_m_animator_11() const { return ___m_animator_11; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_m_animator_11() { return &___m_animator_11; }
	inline void set_m_animator_11(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___m_animator_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_animator_11), (void*)value);
	}

	inline static int32_t get_offset_of_activeControllerType_12() { return static_cast<int32_t>(offsetof(OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6, ___activeControllerType_12)); }
	inline int32_t get_activeControllerType_12() const { return ___activeControllerType_12; }
	inline int32_t* get_address_of_activeControllerType_12() { return &___activeControllerType_12; }
	inline void set_activeControllerType_12(int32_t value)
	{
		___activeControllerType_12 = value;
	}

	inline static int32_t get_offset_of_m_prevControllerConnected_13() { return static_cast<int32_t>(offsetof(OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6, ___m_prevControllerConnected_13)); }
	inline bool get_m_prevControllerConnected_13() const { return ___m_prevControllerConnected_13; }
	inline bool* get_address_of_m_prevControllerConnected_13() { return &___m_prevControllerConnected_13; }
	inline void set_m_prevControllerConnected_13(bool value)
	{
		___m_prevControllerConnected_13 = value;
	}

	inline static int32_t get_offset_of_m_prevControllerConnectedCached_14() { return static_cast<int32_t>(offsetof(OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6, ___m_prevControllerConnectedCached_14)); }
	inline bool get_m_prevControllerConnectedCached_14() const { return ___m_prevControllerConnectedCached_14; }
	inline bool* get_address_of_m_prevControllerConnectedCached_14() { return &___m_prevControllerConnectedCached_14; }
	inline void set_m_prevControllerConnectedCached_14(bool value)
	{
		___m_prevControllerConnectedCached_14 = value;
	}
};


// OVRHand
struct OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// OVRHand/Hand OVRHand::HandType
	int32_t ___HandType_4;
	// UnityEngine.Transform OVRHand::_pointerPoseRoot
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____pointerPoseRoot_5;
	// UnityEngine.GameObject OVRHand::_pointerPoseGO
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____pointerPoseGO_6;
	// OVRPlugin/HandState OVRHand::_handState
	HandState_t5377A12318B95003B2BB256777077FE3D05D0090  ____handState_7;
	// System.Boolean OVRHand::<IsDataValid>k__BackingField
	bool ___U3CIsDataValidU3Ek__BackingField_8;
	// System.Boolean OVRHand::<IsDataHighConfidence>k__BackingField
	bool ___U3CIsDataHighConfidenceU3Ek__BackingField_9;
	// System.Boolean OVRHand::<IsTracked>k__BackingField
	bool ___U3CIsTrackedU3Ek__BackingField_10;
	// System.Boolean OVRHand::<IsSystemGestureInProgress>k__BackingField
	bool ___U3CIsSystemGestureInProgressU3Ek__BackingField_11;
	// System.Boolean OVRHand::<IsPointerPoseValid>k__BackingField
	bool ___U3CIsPointerPoseValidU3Ek__BackingField_12;
	// UnityEngine.Transform OVRHand::<PointerPose>k__BackingField
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___U3CPointerPoseU3Ek__BackingField_13;
	// System.Single OVRHand::<HandScale>k__BackingField
	float ___U3CHandScaleU3Ek__BackingField_14;
	// OVRHand/TrackingConfidence OVRHand::<HandConfidence>k__BackingField
	int32_t ___U3CHandConfidenceU3Ek__BackingField_15;
	// System.Boolean OVRHand::<IsDominantHand>k__BackingField
	bool ___U3CIsDominantHandU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_HandType_4() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ___HandType_4)); }
	inline int32_t get_HandType_4() const { return ___HandType_4; }
	inline int32_t* get_address_of_HandType_4() { return &___HandType_4; }
	inline void set_HandType_4(int32_t value)
	{
		___HandType_4 = value;
	}

	inline static int32_t get_offset_of__pointerPoseRoot_5() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ____pointerPoseRoot_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__pointerPoseRoot_5() const { return ____pointerPoseRoot_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__pointerPoseRoot_5() { return &____pointerPoseRoot_5; }
	inline void set__pointerPoseRoot_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____pointerPoseRoot_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pointerPoseRoot_5), (void*)value);
	}

	inline static int32_t get_offset_of__pointerPoseGO_6() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ____pointerPoseGO_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__pointerPoseGO_6() const { return ____pointerPoseGO_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__pointerPoseGO_6() { return &____pointerPoseGO_6; }
	inline void set__pointerPoseGO_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____pointerPoseGO_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pointerPoseGO_6), (void*)value);
	}

	inline static int32_t get_offset_of__handState_7() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ____handState_7)); }
	inline HandState_t5377A12318B95003B2BB256777077FE3D05D0090  get__handState_7() const { return ____handState_7; }
	inline HandState_t5377A12318B95003B2BB256777077FE3D05D0090 * get_address_of__handState_7() { return &____handState_7; }
	inline void set__handState_7(HandState_t5377A12318B95003B2BB256777077FE3D05D0090  value)
	{
		____handState_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&____handState_7))->___BoneRotations_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&____handState_7))->___PinchStrength_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&____handState_7))->___FingerConfidences_8), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CIsDataValidU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ___U3CIsDataValidU3Ek__BackingField_8)); }
	inline bool get_U3CIsDataValidU3Ek__BackingField_8() const { return ___U3CIsDataValidU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsDataValidU3Ek__BackingField_8() { return &___U3CIsDataValidU3Ek__BackingField_8; }
	inline void set_U3CIsDataValidU3Ek__BackingField_8(bool value)
	{
		___U3CIsDataValidU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CIsDataHighConfidenceU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ___U3CIsDataHighConfidenceU3Ek__BackingField_9)); }
	inline bool get_U3CIsDataHighConfidenceU3Ek__BackingField_9() const { return ___U3CIsDataHighConfidenceU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIsDataHighConfidenceU3Ek__BackingField_9() { return &___U3CIsDataHighConfidenceU3Ek__BackingField_9; }
	inline void set_U3CIsDataHighConfidenceU3Ek__BackingField_9(bool value)
	{
		___U3CIsDataHighConfidenceU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CIsTrackedU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ___U3CIsTrackedU3Ek__BackingField_10)); }
	inline bool get_U3CIsTrackedU3Ek__BackingField_10() const { return ___U3CIsTrackedU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsTrackedU3Ek__BackingField_10() { return &___U3CIsTrackedU3Ek__BackingField_10; }
	inline void set_U3CIsTrackedU3Ek__BackingField_10(bool value)
	{
		___U3CIsTrackedU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CIsSystemGestureInProgressU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ___U3CIsSystemGestureInProgressU3Ek__BackingField_11)); }
	inline bool get_U3CIsSystemGestureInProgressU3Ek__BackingField_11() const { return ___U3CIsSystemGestureInProgressU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsSystemGestureInProgressU3Ek__BackingField_11() { return &___U3CIsSystemGestureInProgressU3Ek__BackingField_11; }
	inline void set_U3CIsSystemGestureInProgressU3Ek__BackingField_11(bool value)
	{
		___U3CIsSystemGestureInProgressU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CIsPointerPoseValidU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ___U3CIsPointerPoseValidU3Ek__BackingField_12)); }
	inline bool get_U3CIsPointerPoseValidU3Ek__BackingField_12() const { return ___U3CIsPointerPoseValidU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CIsPointerPoseValidU3Ek__BackingField_12() { return &___U3CIsPointerPoseValidU3Ek__BackingField_12; }
	inline void set_U3CIsPointerPoseValidU3Ek__BackingField_12(bool value)
	{
		___U3CIsPointerPoseValidU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CPointerPoseU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ___U3CPointerPoseU3Ek__BackingField_13)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_U3CPointerPoseU3Ek__BackingField_13() const { return ___U3CPointerPoseU3Ek__BackingField_13; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_U3CPointerPoseU3Ek__BackingField_13() { return &___U3CPointerPoseU3Ek__BackingField_13; }
	inline void set_U3CPointerPoseU3Ek__BackingField_13(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___U3CPointerPoseU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPointerPoseU3Ek__BackingField_13), (void*)value);
	}

	inline static int32_t get_offset_of_U3CHandScaleU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ___U3CHandScaleU3Ek__BackingField_14)); }
	inline float get_U3CHandScaleU3Ek__BackingField_14() const { return ___U3CHandScaleU3Ek__BackingField_14; }
	inline float* get_address_of_U3CHandScaleU3Ek__BackingField_14() { return &___U3CHandScaleU3Ek__BackingField_14; }
	inline void set_U3CHandScaleU3Ek__BackingField_14(float value)
	{
		___U3CHandScaleU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CHandConfidenceU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ___U3CHandConfidenceU3Ek__BackingField_15)); }
	inline int32_t get_U3CHandConfidenceU3Ek__BackingField_15() const { return ___U3CHandConfidenceU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CHandConfidenceU3Ek__BackingField_15() { return &___U3CHandConfidenceU3Ek__BackingField_15; }
	inline void set_U3CHandConfidenceU3Ek__BackingField_15(int32_t value)
	{
		___U3CHandConfidenceU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CIsDominantHandU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A, ___U3CIsDominantHandU3Ek__BackingField_16)); }
	inline bool get_U3CIsDominantHandU3Ek__BackingField_16() const { return ___U3CIsDominantHandU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CIsDominantHandU3Ek__BackingField_16() { return &___U3CIsDominantHandU3Ek__BackingField_16; }
	inline void set_U3CIsDominantHandU3Ek__BackingField_16(bool value)
	{
		___U3CIsDominantHandU3Ek__BackingField_16 = value;
	}
};


// OVRSkeleton
struct OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// OVRSkeleton/SkeletonType OVRSkeleton::_skeletonType
	int32_t ____skeletonType_4;
	// OVRSkeleton/IOVRSkeletonDataProvider OVRSkeleton::_dataProvider
	RuntimeObject* ____dataProvider_5;
	// System.Boolean OVRSkeleton::_updateRootPose
	bool ____updateRootPose_6;
	// System.Boolean OVRSkeleton::_updateRootScale
	bool ____updateRootScale_7;
	// System.Boolean OVRSkeleton::_enablePhysicsCapsules
	bool ____enablePhysicsCapsules_8;
	// UnityEngine.GameObject OVRSkeleton::_bonesGO
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____bonesGO_9;
	// UnityEngine.GameObject OVRSkeleton::_bindPosesGO
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____bindPosesGO_10;
	// UnityEngine.GameObject OVRSkeleton::_capsulesGO
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____capsulesGO_11;
	// System.Collections.Generic.List`1<OVRBone> OVRSkeleton::_bones
	List_1_t8958585D92650E004D74521A8D0B3CE051FA276F * ____bones_12;
	// System.Collections.Generic.List`1<OVRBone> OVRSkeleton::_bindPoses
	List_1_t8958585D92650E004D74521A8D0B3CE051FA276F * ____bindPoses_13;
	// System.Collections.Generic.List`1<OVRBoneCapsule> OVRSkeleton::_capsules
	List_1_t2757DE18D8F3AB63917B2846D8F68D853749967C * ____capsules_14;
	// OVRPlugin/Skeleton2 OVRSkeleton::_skeleton
	Skeleton2_tBCE168B418608021C1551D640F39DF88F1282E13  ____skeleton_15;
	// UnityEngine.Quaternion OVRSkeleton::wristFixupRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___wristFixupRotation_16;
	// System.Boolean OVRSkeleton::<IsInitialized>k__BackingField
	bool ___U3CIsInitializedU3Ek__BackingField_17;
	// System.Boolean OVRSkeleton::<IsDataValid>k__BackingField
	bool ___U3CIsDataValidU3Ek__BackingField_18;
	// System.Boolean OVRSkeleton::<IsDataHighConfidence>k__BackingField
	bool ___U3CIsDataHighConfidenceU3Ek__BackingField_19;
	// System.Collections.Generic.IList`1<OVRBone> OVRSkeleton::<Bones>k__BackingField
	RuntimeObject* ___U3CBonesU3Ek__BackingField_20;
	// System.Collections.Generic.IList`1<OVRBone> OVRSkeleton::<BindPoses>k__BackingField
	RuntimeObject* ___U3CBindPosesU3Ek__BackingField_21;
	// System.Collections.Generic.IList`1<OVRBoneCapsule> OVRSkeleton::<Capsules>k__BackingField
	RuntimeObject* ___U3CCapsulesU3Ek__BackingField_22;
	// System.Int32 OVRSkeleton::<SkeletonChangedCount>k__BackingField
	int32_t ___U3CSkeletonChangedCountU3Ek__BackingField_23;

public:
	inline static int32_t get_offset_of__skeletonType_4() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ____skeletonType_4)); }
	inline int32_t get__skeletonType_4() const { return ____skeletonType_4; }
	inline int32_t* get_address_of__skeletonType_4() { return &____skeletonType_4; }
	inline void set__skeletonType_4(int32_t value)
	{
		____skeletonType_4 = value;
	}

	inline static int32_t get_offset_of__dataProvider_5() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ____dataProvider_5)); }
	inline RuntimeObject* get__dataProvider_5() const { return ____dataProvider_5; }
	inline RuntimeObject** get_address_of__dataProvider_5() { return &____dataProvider_5; }
	inline void set__dataProvider_5(RuntimeObject* value)
	{
		____dataProvider_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dataProvider_5), (void*)value);
	}

	inline static int32_t get_offset_of__updateRootPose_6() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ____updateRootPose_6)); }
	inline bool get__updateRootPose_6() const { return ____updateRootPose_6; }
	inline bool* get_address_of__updateRootPose_6() { return &____updateRootPose_6; }
	inline void set__updateRootPose_6(bool value)
	{
		____updateRootPose_6 = value;
	}

	inline static int32_t get_offset_of__updateRootScale_7() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ____updateRootScale_7)); }
	inline bool get__updateRootScale_7() const { return ____updateRootScale_7; }
	inline bool* get_address_of__updateRootScale_7() { return &____updateRootScale_7; }
	inline void set__updateRootScale_7(bool value)
	{
		____updateRootScale_7 = value;
	}

	inline static int32_t get_offset_of__enablePhysicsCapsules_8() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ____enablePhysicsCapsules_8)); }
	inline bool get__enablePhysicsCapsules_8() const { return ____enablePhysicsCapsules_8; }
	inline bool* get_address_of__enablePhysicsCapsules_8() { return &____enablePhysicsCapsules_8; }
	inline void set__enablePhysicsCapsules_8(bool value)
	{
		____enablePhysicsCapsules_8 = value;
	}

	inline static int32_t get_offset_of__bonesGO_9() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ____bonesGO_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__bonesGO_9() const { return ____bonesGO_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__bonesGO_9() { return &____bonesGO_9; }
	inline void set__bonesGO_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____bonesGO_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bonesGO_9), (void*)value);
	}

	inline static int32_t get_offset_of__bindPosesGO_10() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ____bindPosesGO_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__bindPosesGO_10() const { return ____bindPosesGO_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__bindPosesGO_10() { return &____bindPosesGO_10; }
	inline void set__bindPosesGO_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____bindPosesGO_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bindPosesGO_10), (void*)value);
	}

	inline static int32_t get_offset_of__capsulesGO_11() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ____capsulesGO_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__capsulesGO_11() const { return ____capsulesGO_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__capsulesGO_11() { return &____capsulesGO_11; }
	inline void set__capsulesGO_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____capsulesGO_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____capsulesGO_11), (void*)value);
	}

	inline static int32_t get_offset_of__bones_12() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ____bones_12)); }
	inline List_1_t8958585D92650E004D74521A8D0B3CE051FA276F * get__bones_12() const { return ____bones_12; }
	inline List_1_t8958585D92650E004D74521A8D0B3CE051FA276F ** get_address_of__bones_12() { return &____bones_12; }
	inline void set__bones_12(List_1_t8958585D92650E004D74521A8D0B3CE051FA276F * value)
	{
		____bones_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bones_12), (void*)value);
	}

	inline static int32_t get_offset_of__bindPoses_13() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ____bindPoses_13)); }
	inline List_1_t8958585D92650E004D74521A8D0B3CE051FA276F * get__bindPoses_13() const { return ____bindPoses_13; }
	inline List_1_t8958585D92650E004D74521A8D0B3CE051FA276F ** get_address_of__bindPoses_13() { return &____bindPoses_13; }
	inline void set__bindPoses_13(List_1_t8958585D92650E004D74521A8D0B3CE051FA276F * value)
	{
		____bindPoses_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bindPoses_13), (void*)value);
	}

	inline static int32_t get_offset_of__capsules_14() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ____capsules_14)); }
	inline List_1_t2757DE18D8F3AB63917B2846D8F68D853749967C * get__capsules_14() const { return ____capsules_14; }
	inline List_1_t2757DE18D8F3AB63917B2846D8F68D853749967C ** get_address_of__capsules_14() { return &____capsules_14; }
	inline void set__capsules_14(List_1_t2757DE18D8F3AB63917B2846D8F68D853749967C * value)
	{
		____capsules_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____capsules_14), (void*)value);
	}

	inline static int32_t get_offset_of__skeleton_15() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ____skeleton_15)); }
	inline Skeleton2_tBCE168B418608021C1551D640F39DF88F1282E13  get__skeleton_15() const { return ____skeleton_15; }
	inline Skeleton2_tBCE168B418608021C1551D640F39DF88F1282E13 * get_address_of__skeleton_15() { return &____skeleton_15; }
	inline void set__skeleton_15(Skeleton2_tBCE168B418608021C1551D640F39DF88F1282E13  value)
	{
		____skeleton_15 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&____skeleton_15))->___Bones_3), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&____skeleton_15))->___BoneCapsules_4), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_wristFixupRotation_16() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ___wristFixupRotation_16)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_wristFixupRotation_16() const { return ___wristFixupRotation_16; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_wristFixupRotation_16() { return &___wristFixupRotation_16; }
	inline void set_wristFixupRotation_16(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___wristFixupRotation_16 = value;
	}

	inline static int32_t get_offset_of_U3CIsInitializedU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ___U3CIsInitializedU3Ek__BackingField_17)); }
	inline bool get_U3CIsInitializedU3Ek__BackingField_17() const { return ___U3CIsInitializedU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CIsInitializedU3Ek__BackingField_17() { return &___U3CIsInitializedU3Ek__BackingField_17; }
	inline void set_U3CIsInitializedU3Ek__BackingField_17(bool value)
	{
		___U3CIsInitializedU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CIsDataValidU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ___U3CIsDataValidU3Ek__BackingField_18)); }
	inline bool get_U3CIsDataValidU3Ek__BackingField_18() const { return ___U3CIsDataValidU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CIsDataValidU3Ek__BackingField_18() { return &___U3CIsDataValidU3Ek__BackingField_18; }
	inline void set_U3CIsDataValidU3Ek__BackingField_18(bool value)
	{
		___U3CIsDataValidU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CIsDataHighConfidenceU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ___U3CIsDataHighConfidenceU3Ek__BackingField_19)); }
	inline bool get_U3CIsDataHighConfidenceU3Ek__BackingField_19() const { return ___U3CIsDataHighConfidenceU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CIsDataHighConfidenceU3Ek__BackingField_19() { return &___U3CIsDataHighConfidenceU3Ek__BackingField_19; }
	inline void set_U3CIsDataHighConfidenceU3Ek__BackingField_19(bool value)
	{
		___U3CIsDataHighConfidenceU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CBonesU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ___U3CBonesU3Ek__BackingField_20)); }
	inline RuntimeObject* get_U3CBonesU3Ek__BackingField_20() const { return ___U3CBonesU3Ek__BackingField_20; }
	inline RuntimeObject** get_address_of_U3CBonesU3Ek__BackingField_20() { return &___U3CBonesU3Ek__BackingField_20; }
	inline void set_U3CBonesU3Ek__BackingField_20(RuntimeObject* value)
	{
		___U3CBonesU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBonesU3Ek__BackingField_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3CBindPosesU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ___U3CBindPosesU3Ek__BackingField_21)); }
	inline RuntimeObject* get_U3CBindPosesU3Ek__BackingField_21() const { return ___U3CBindPosesU3Ek__BackingField_21; }
	inline RuntimeObject** get_address_of_U3CBindPosesU3Ek__BackingField_21() { return &___U3CBindPosesU3Ek__BackingField_21; }
	inline void set_U3CBindPosesU3Ek__BackingField_21(RuntimeObject* value)
	{
		___U3CBindPosesU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBindPosesU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCapsulesU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ___U3CCapsulesU3Ek__BackingField_22)); }
	inline RuntimeObject* get_U3CCapsulesU3Ek__BackingField_22() const { return ___U3CCapsulesU3Ek__BackingField_22; }
	inline RuntimeObject** get_address_of_U3CCapsulesU3Ek__BackingField_22() { return &___U3CCapsulesU3Ek__BackingField_22; }
	inline void set_U3CCapsulesU3Ek__BackingField_22(RuntimeObject* value)
	{
		___U3CCapsulesU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCapsulesU3Ek__BackingField_22), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSkeletonChangedCountU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC, ___U3CSkeletonChangedCountU3Ek__BackingField_23)); }
	inline int32_t get_U3CSkeletonChangedCountU3Ek__BackingField_23() const { return ___U3CSkeletonChangedCountU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3CSkeletonChangedCountU3Ek__BackingField_23() { return &___U3CSkeletonChangedCountU3Ek__BackingField_23; }
	inline void set_U3CSkeletonChangedCountU3Ek__BackingField_23(int32_t value)
	{
		___U3CSkeletonChangedCountU3Ek__BackingField_23 = value;
	}
};


// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager
struct OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C  : public XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7
{
public:
	// System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand> Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::trackedHands
	Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * ___trackedHands_25;
	// OVRCameraRig Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::cameraRig
	OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * ___cameraRig_26;
	// OVRHand Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::rightHand
	OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * ___rightHand_27;
	// OVRSkeleton Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::rightSkeleton
	OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * ___rightSkeleton_28;
	// OVRHand Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::leftHand
	OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * ___leftHand_29;
	// OVRSkeleton Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::leftSkeleton
	OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * ___leftSkeleton_30;
	// System.Nullable`1<System.Boolean> Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::isActiveLoader
	Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  ___isActiveLoader_31;

public:
	inline static int32_t get_offset_of_trackedHands_25() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C, ___trackedHands_25)); }
	inline Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * get_trackedHands_25() const { return ___trackedHands_25; }
	inline Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 ** get_address_of_trackedHands_25() { return &___trackedHands_25; }
	inline void set_trackedHands_25(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * value)
	{
		___trackedHands_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trackedHands_25), (void*)value);
	}

	inline static int32_t get_offset_of_cameraRig_26() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C, ___cameraRig_26)); }
	inline OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * get_cameraRig_26() const { return ___cameraRig_26; }
	inline OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 ** get_address_of_cameraRig_26() { return &___cameraRig_26; }
	inline void set_cameraRig_26(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * value)
	{
		___cameraRig_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameraRig_26), (void*)value);
	}

	inline static int32_t get_offset_of_rightHand_27() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C, ___rightHand_27)); }
	inline OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * get_rightHand_27() const { return ___rightHand_27; }
	inline OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A ** get_address_of_rightHand_27() { return &___rightHand_27; }
	inline void set_rightHand_27(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * value)
	{
		___rightHand_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rightHand_27), (void*)value);
	}

	inline static int32_t get_offset_of_rightSkeleton_28() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C, ___rightSkeleton_28)); }
	inline OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * get_rightSkeleton_28() const { return ___rightSkeleton_28; }
	inline OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC ** get_address_of_rightSkeleton_28() { return &___rightSkeleton_28; }
	inline void set_rightSkeleton_28(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * value)
	{
		___rightSkeleton_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rightSkeleton_28), (void*)value);
	}

	inline static int32_t get_offset_of_leftHand_29() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C, ___leftHand_29)); }
	inline OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * get_leftHand_29() const { return ___leftHand_29; }
	inline OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A ** get_address_of_leftHand_29() { return &___leftHand_29; }
	inline void set_leftHand_29(OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * value)
	{
		___leftHand_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___leftHand_29), (void*)value);
	}

	inline static int32_t get_offset_of_leftSkeleton_30() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C, ___leftSkeleton_30)); }
	inline OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * get_leftSkeleton_30() const { return ___leftSkeleton_30; }
	inline OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC ** get_address_of_leftSkeleton_30() { return &___leftSkeleton_30; }
	inline void set_leftSkeleton_30(OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * value)
	{
		___leftSkeleton_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___leftSkeleton_30), (void*)value);
	}

	inline static int32_t get_offset_of_isActiveLoader_31() { return static_cast<int32_t>(offsetof(OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C, ___isActiveLoader_31)); }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  get_isActiveLoader_31() const { return ___isActiveLoader_31; }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * get_address_of_isActiveLoader_31() { return &___isActiveLoader_31; }
	inline void set_isActiveLoader_31(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  value)
	{
		___isActiveLoader_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[]
struct MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * m_Items[1];

public:
	inline MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// OVRControllerHelper[]
struct OVRControllerHelperU5BU5D_tC1041712E5487B448EA158DCEAE5D8D1F8996DD9  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6 * m_Items[1];

public:
	inline OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// OVRHand[]
struct OVRHandU5BU5D_tDE7CB5D0BAC901AB98623D26B8A07D4DAA25C6DF  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * m_Items[1];

public:
	inline OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer[]
struct IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m7EEBAAB59D9E1EC1D55B04A0BA4C1EBC772B0961_gshared (Dictionary_2_t9C45AE0B9705F160EE5D2856D9908DD6FE2A8069 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Int32Enum>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m52B7F9C69C872D65BB7C8A5AE4D039AD9324D8E0_gshared (Dictionary_2_tFABA756D73CAA65D26DBC9CE4B49A893F6812BB1 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Int32Enum>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m812B9182035013C4742555C8B08A716B702B8E53_gshared (Dictionary_2_tFABA756D73CAA65D26DBC9CE4B49A893F6812BB1 * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32Enum,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m289082E9DC96F6DEAF5ED052276EDBBF233955B0_gshared (Dictionary_2_t9C45AE0B9705F160EE5D2856D9908DD6FE2A8069 * __this, int32_t ___key0, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * ___value1, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Int32Enum,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>::get_Item(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  Dictionary_2_get_Item_m76B8E450990485DC6DCC5E6747DEDEEA482AF608_gshared (Dictionary_2_t9C45AE0B9705F160EE5D2856D9908DD6FE2A8069 * __this, int32_t ___key0, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Int32>::.ctor(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184_gshared (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, int32_t ___value0, const RuntimeMethod* method);
// !0 System.Nullable`1<System.Int32>::GetValueOrDefault()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_gshared_inline (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m7455E879CFAAE682AE3786D4D2B1F65C8AA23921_gshared_inline (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Int32Enum>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_mDC142D358C757B1CEC171DAAE0C36BEBFCED138E_gshared (Dictionary_2_tFABA756D73CAA65D26DBC9CE4B49A893F6812BB1 * __this, int32_t ___key0, int32_t* ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32Enum,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>::ContainsKey(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m535A937118A4E4A94E86B49AAB1B01A18619AD8B_gshared (Dictionary_2_t9C45AE0B9705F160EE5D2856D9908DD6FE2A8069 * __this, int32_t ___key0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m944621C1FD5CFA4DBCC1D9DEAF5AD285AC07C426_gshared (Dictionary_2_t9C45AE0B9705F160EE5D2856D9908DD6FE2A8069 * __this, int32_t ___key0, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>::set_Item(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m354BF9B9ABF0B1DBFCA2A2C1DF6932460825124D_gshared (Dictionary_2_t9C45AE0B9705F160EE5D2856D9908DD6FE2A8069 * __this, int32_t ___key0, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.ByteEnum,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_mB1BD995056039C2AD2CE07E04A53E652A6C039B9_gshared (Dictionary_2_tF6533D4775708EB95178ECDC1D347094A5009988 * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m52F33C6963C9A76A14F54B4B2C30CEF580DB1EB7_gshared_inline (Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Boolean>::.ctor(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1__ctor_m402A94AC9070B345C95919DCFFFF627252B3E67A_gshared (Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * __this, bool ___value0, const RuntimeMethod* method);
// !0 System.Nullable`1<System.Boolean>::GetValueOrDefault()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_GetValueOrDefault_mBECC58FEFD1088EC070D9F9A892ECD1D8BBF2A0F_gshared_inline (Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m25AA6DB6AABFD5D66AFA1A8C0E91A7AF61429C37_gshared (const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m4039C8E65629D33E1EC84D2505BF1D5DDC936622_gshared (RuntimeObject * ___original0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* GameObject_GetComponentsInChildren_TisRuntimeObject_m6662AE3C936281A25097CCBD9098A9F85C69279A_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mD211EB15E9E128684605B4CC7277F10840F8E8CF_gshared (RuntimeObject * ___original0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent1, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Component_GetComponentsInChildren_TisRuntimeObject_mCA5B356D4B0824C6DE60A8E90E6A6D4188C56C2F_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.ByteEnum,System.Object>::ContainsKey(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m18463BD56578964BC10206CA83C202E5D51FD502_gshared (Dictionary_2_tF6533D4775708EB95178ECDC1D347094A5009988 * __this, uint8_t ___key0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.ByteEnum,System.Object>::get_Item(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Dictionary_2_get_Item_mEA5665B69C064636B283B6E324FAEA99DDC82A18_gshared (Dictionary_2_tF6533D4775708EB95178ECDC1D347094A5009988 * __this, uint8_t ___key0, const RuntimeMethod* method);
// !0 Microsoft.MixedReality.Toolkit.BaseDataProvider`1<System.Object>::get_Service()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * BaseDataProvider_1_get_Service_m041BFCBFB9FCF6AD8183DE3BE1736CEAA9048863_gshared_inline (BaseDataProvider_1_t664C3F8898C3B28F4040012FA554BEA276D3A25C * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.ByteEnum,System.Object>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m0ED5FEF3A5E5E1AFCE9C2F5CAB746A29C7730059_gshared (Dictionary_2_tF6533D4775708EB95178ECDC1D347094A5009988 * __this, uint8_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.ByteEnum,System.Object>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_mEFC0DB89DF161BD8BEF226C8150B5216A5665F6A_gshared (Dictionary_2_tF6533D4775708EB95178ECDC1D347094A5009988 * __this, uint8_t ___key0, RuntimeObject ** ___value1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Dictionary`2<System.ByteEnum,System.Object>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Count_m20EE44B3BEDA1B90B843AE95B79715F1DEE3B537_gshared (Dictionary_2_tF6533D4775708EB95178ECDC1D347094A5009988 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.ByteEnum,System.Object>::get_Values()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ValueCollection_t9CDC52A109ED7F9848B919DD1D1117BBC2F03DCB * Dictionary_2_get_Values_mFEBA424428F76B6060D7B261B41DDA8927275FB0_gshared (Dictionary_2_tF6533D4775708EB95178ECDC1D347094A5009988 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m1D864B65CCD0498EC4BFFBDA8F8D04AE5333195A_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.ByteEnum,System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Clear_m0C5D3DDD1E23EBEFEBF57B1A58EBAED4359418C1_gshared (Dictionary_2_tF6533D4775708EB95178ECDC1D347094A5009988 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.ByteEnum,System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_Remove_mF0624214F2AE94824B5955D3CD09527AA94D536F_gshared (Dictionary_2_tF6533D4775708EB95178ECDC1D347094A5009988 * __this, uint8_t ___key0, const RuntimeMethod* method);

// Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose::get_ZeroIdentity()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  MixedRealityPose_get_ZeroIdentity_m0668ACB9F881F2046B81685B02D13BFD78338998_inline (const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>::.ctor()
inline void Dictionary_2__ctor_m11C00E81E12AE2761EDA2BB312BBE57AA4E8C696 (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 *, const RuntimeMethod*))Dictionary_2__ctor_m7EEBAAB59D9E1EC1D55B04A0BA4C1EBC772B0961_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<OVRSkeleton/BoneId,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint>::.ctor()
inline void Dictionary_2__ctor_m8A8D33B5A8E9011511505D1AB8698F63E2622E51 (Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B *, const RuntimeMethod*))Dictionary_2__ctor_m52B7F9C69C872D65BB7C8A5AE4D039AD9324D8E0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<OVRSkeleton/BoneId,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint>::Add(!0,!1)
inline void Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8 (Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B *, int32_t, int32_t, const RuntimeMethod*))Dictionary_2_Add_m812B9182035013C4742555C8B08A716B702B8E53_gshared)(__this, ___key0, ___value1, method);
}
// System.Void Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArticulatedHandDefinition__ctor_m502F18A61CCBE95F3FDE120D47C7A12619A83087 (ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * __this, RuntimeObject* ___source0, uint8_t ___handedness1, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Input.BaseHand::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[],Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSourceDefinition)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseHand__ctor_mC039ECD3265CBEBD569E8AB59C4A96EB0AD97DEB (BaseHand_t44436345C300F7B303FBC825541C6EF243DCCE1D * __this, int32_t ___trackingState0, uint8_t ___controllerHandedness1, RuntimeObject* ___inputSource2, MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* ___interactions3, RuntimeObject* ___definition4, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Input.BaseController::AssignControllerMappings(Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseController_AssignControllerMappings_mE2C0913573EEB9BE3396880A642A2B8419138B43 (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* ___mappings0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m91A28CD3479611BC6DE4868B185BC381A2114D78 (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * __this, int32_t ___key0, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 *, int32_t, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *, const RuntimeMethod*))Dictionary_2_TryGetValue_m289082E9DC96F6DEAF5ED052276EDBBF233955B0_gshared)(__this, ___key0, ___value1, method);
}
// Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem Microsoft.MixedReality.Toolkit.CoreServices::get_InputSystem()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CoreServices_get_InputSystem_m03F66B157660C21D67B5A6A4D132BF889CBFE440 (const RuntimeMethod* method);
// Microsoft.MixedReality.Toolkit.Input.MixedRealityHandTrackingProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile::get_HandTrackingProfile()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 * MixedRealityInputSystemProfile_get_HandTrackingProfile_m68AB045F05D5B7E5723E8D3331E837CAC058CE7A_inline (MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C * __this, const RuntimeMethod* method);
// Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::get_HandDefinition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * OculusHand_get_HandDefinition_mF576EE597AFC69C901F66DA59822582FFAAC1958 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method);
// System.Boolean Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::get_IsInPointingPose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ArticulatedHandDefinition_get_IsInPointingPose_m1591492D993E0A019C98DA078CA267648C7E4BBE (ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * __this, const RuntimeMethod* method);
// System.Boolean Microsoft.MixedReality.Toolkit.Input.BaseController::get_Enabled()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool BaseController_get_Enabled_m23BE305FD0E3643AB6B26D80F0F0FBB33CF285D4_inline (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::UpdateHandData(OVRHand,OVRSkeleton)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OculusHand_UpdateHandData_m1A965734213BE9AB52AE8B439AF9FD9C58222105 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * ___ovrHand0, OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * ___ovrSkeleton1, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Input.BaseController::set_IsRotationAvailable(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void BaseController_set_IsRotationAvailable_m36835958007BE5F83FC443D7837E00F38D31E7D8_inline (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Input.BaseController::set_IsPositionAvailable(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void BaseController_set_IsPositionAvailable_m8762BC7C636F247BED1DB49EE4509475DA072F44_inline (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Transform OVRHand::get_PointerPose()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * OVRHand_get_PointerPose_m5FD5EE4BC344C1F3D2F85B1F8C6E3D0E518DF7A2_inline (OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_TransformPoint_m68AF95765A9279192E601208A9C5170027A5F0D2 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose::set_Position(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void MixedRealityPose_set_Position_m119B5345F1415E482F83D6BD4E7B1F4951B7375A_inline (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_TransformDirection_m6B5E3F0A7C6323159DEC6D9BC035FB53ADD96E91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_LookRotation_m8A7DB5BDBC361586191AB67ACF857F46160EE3F1 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forward0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upwards1, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose::set_Rotation(UnityEngine.Quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void MixedRealityPose_set_Rotation_mF14187710EDE143DFC85AB2A0892FD72B793DF77_inline (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>::get_Item(!0)
inline MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  Dictionary_2_get_Item_m5A745DC8015EA0953A57CDB2866344D2AF45CC9E (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * __this, int32_t ___key0, const RuntimeMethod* method)
{
	return ((  MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  (*) (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_m76B8E450990485DC6DCC5E6747DEDEEA482AF608_gshared)(__this, ___key0, method);
}
// Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource Microsoft.MixedReality.Toolkit.Input.BaseController::get_InputSource()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Input.BaseHand::UpdateVelocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseHand_UpdateVelocity_mD2D4D7FC66091E4061F281E9351A696B6FC8D356 (BaseHand_t44436345C300F7B303FBC825541C6EF243DCCE1D * __this, const RuntimeMethod* method);
// Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[] Microsoft.MixedReality.Toolkit.Input.BaseController::get_Interactions()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, const RuntimeMethod* method);
// Microsoft.MixedReality.Toolkit.Input.DeviceInputType Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::get_InputType()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t MixedRealityInteractionMapping_get_InputType_mF467F74CF7AAA7B1B3C3EB6BE950BEABA6F1A435_inline (MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::set_PoseData(Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MixedRealityInteractionMapping_set_PoseData_m79D33F56E91FB9CE5BE2A86A78F9F2492AA558F1 (MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * __this, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  ___value0, const RuntimeMethod* method);
// System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::get_Changed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MixedRealityInteractionMapping_get_Changed_mA0C90E795316CFE4F8E911F7371D6E47A433FEDF (MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * __this, const RuntimeMethod* method);
// Microsoft.MixedReality.Toolkit.Utilities.Handedness Microsoft.MixedReality.Toolkit.Input.BaseController::get_ControllerHandedness()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint8_t BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, const RuntimeMethod* method);
// Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::get_MixedRealityInputAction()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  MixedRealityInteractionMapping_get_MixedRealityInputAction_m65770DAB10749381CC2E06AE4F4D4756AA65424A_inline (MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * __this, const RuntimeMethod* method);
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::get_IsPinching()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool OculusHand_get_IsPinching_m1DEFD901E5E5FE10261629E06E5941C2DEF0F707_inline (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method);
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::get_IsGrabbing()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool OculusHand_get_IsGrabbing_m461FECC082EDF20D3F788A5754B7231DC93EEA0A_inline (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::set_BoolData(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MixedRealityInteractionMapping_set_BoolData_mB9E1F1E8CC1CCC3ADFDFB08A564FA528A6399068 (MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping::get_BoolData()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool MixedRealityInteractionMapping_get_BoolData_m002148D894B01A0998785DE1E8149B433174A79A_inline (MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::UpdateCurrentIndexPose(Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArticulatedHandDefinition_UpdateCurrentIndexPose_mD2E582B958E504018BE34CC26AE420555D8DE754 (ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * __this, MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * ___interactionMapping0, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::UpdateCurrentTeleportPose(Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArticulatedHandDefinition_UpdateCurrentTeleportPose_m2DC840B71B67204E4D7A1221AD489FF55643462A (ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * __this, MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * ___interactionMapping0, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Int32>::.ctor(!0)
inline void Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184 (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 *, int32_t, const RuntimeMethod*))Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184_gshared)(__this, ___value0, method);
}
// !0 System.Nullable`1<System.Int32>::GetValueOrDefault()
inline int32_t Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_inline (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 *, const RuntimeMethod*))Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_gshared_inline)(__this, method);
}
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
inline bool Nullable_1_get_HasValue_m7455E879CFAAE682AE3786D4D2B1F65C8AA23921_inline (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 *, const RuntimeMethod*))Nullable_1_get_HasValue_m7455E879CFAAE682AE3786D4D2B1F65C8AA23921_gshared_inline)(__this, method);
}
// System.Boolean OVRHand::get_IsTracked()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool OVRHand_get_IsTracked_m1B1B95C54623252AD41089F461ACC3669CE2AE7C_inline (OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * __this, const RuntimeMethod* method);
// OVRHand/TrackingConfidence OVRHand::get_HandConfidence()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t OVRHand_get_HandConfidence_mE06ACA8BA1C4D00BA5F196AC3CA3DCD46C9801FE_inline (OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_unscaledTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258 (const RuntimeMethod* method);
// OVRHand/TrackingConfidence Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_MinimumHandConfidence()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManagerProfile_get_MinimumHandConfidence_m219D821F8CEA93A8D563B5F88F46C5E3A2BFD20D_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844 (const RuntimeMethod* method);
// System.Single Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_LowConfidenceTimeThreshold()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float OculusXRSDKDeviceManagerProfile_get_LowConfidenceTimeThreshold_mA6197D5CFC9C78F61EB6B040A43E17277A26A743_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::set_CurrentLeftHandTrackingConfidence(OVRHand/TrackingConfidence)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_set_CurrentLeftHandTrackingConfidence_m36B081D88A939B6F1D83987B46ACFD4C5AE1F10D_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::set_CurrentRightHandTrackingConfidence(OVRHand/TrackingConfidence)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_set_CurrentRightHandTrackingConfidence_mDEE11B9CDA4CE2DD9C1262D2DCBC72CF04CAD57B_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Collections.Generic.IList`1<OVRBone> OVRSkeleton::get_Bones()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* OVRSkeleton_get_Bones_mC8D26C6EF0253FBC0EF8B350EEBBE84C97045C0F_inline (OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::UpdateBone(OVRBone)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_UpdateBone_mC448C0E93B90FB31A0C34C5B9E3BEB0A758E8E62 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, OVRBone_tF9C031559703607035E455CE642549C722BBF081 * ___bone0, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::UpdatePalm()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_UpdatePalm_m8584D7681FB7E16CCEFB42F5EC5E972320397301 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition::UpdateHandJoints(System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArticulatedHandDefinition_UpdateHandJoints_m23EE90FF8F0A03C3FF55929B3E1418AF1468670B (ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * __this, Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * ___jointPoses0, const RuntimeMethod* method);
// System.Single Microsoft.MixedReality.Toolkit.Utilities.HandPoseUtils::CalculateIndexPinch(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float HandPoseUtils_CalculateIndexPinch_mD8E7EDEBE7490F26B53A4C7A9DA64D43579BCD45 (uint8_t ___handedness0, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::set_IsPinching(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OculusHand_set_IsPinching_mAAEFC3C08B379B80AF3411AC98531581C90B1B4A_inline (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, bool ___value0, const RuntimeMethod* method);
// OVRHand/TrackingConfidence OVRHand::GetFingerConfidence(OVRHand/HandFinger)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OVRHand_GetFingerConfidence_m50B0F85F1FF69DF3DFF61CD1EC846D95213E4C96 (OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * __this, int32_t ___finger0, const RuntimeMethod* method);
// System.Boolean Microsoft.MixedReality.Toolkit.Utilities.HandPoseUtils::IsIndexGrabbing(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HandPoseUtils_IsIndexGrabbing_m56746CC4DA7560A5E4DDDF47D412AC070FDB783E (uint8_t ___hand0, const RuntimeMethod* method);
// System.Boolean Microsoft.MixedReality.Toolkit.Utilities.HandPoseUtils::IsMiddleGrabbing(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HandPoseUtils_IsMiddleGrabbing_mB2B428983A4A54AEE69434B739327C31F4092479 (uint8_t ___hand0, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::set_IsGrabbing(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OculusHand_set_IsGrabbing_mD4B8A213D51B07938ED3B39C75D37BC0BF5C6872_inline (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose::get_Position()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  MixedRealityPose_get_Position_m34E0E9859576E00CEB1CC3D1D31F18C45B1712A3_inline (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// OVRSkeleton/BoneId OVRBone::get_Id()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t OVRBone_get_Id_m60B032C7EB5D597C9F5D3D7911A241F31EE8E12E_inline (OVRBone_tF9C031559703607035E455CE642549C722BBF081 * __this, const RuntimeMethod* method);
// UnityEngine.Transform OVRBone::get_Transform()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * OVRBone_get_Transform_m0E8C60FF2CF31336D654B3319EFFB04BDC964FCB_inline (OVRBone_tF9C031559703607035E455CE642549C722BBF081 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<OVRSkeleton/BoneId,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m6CC999BE14BA489631964C386680A89AEEB456DA (Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * __this, int32_t ___key0, int32_t* ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B *, int32_t, int32_t*, const RuntimeMethod*))Dictionary_2_TryGetValue_mDC142D358C757B1CEC171DAAE0C36BEBFCED138E_gshared)(__this, ___key0, ___value1, method);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___lhs0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rhs1, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::UpdateJointPose(Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_UpdateJointPose_mF3900F3A833A0A62400A8EEF683E6C764F47FBD8 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, int32_t ___joint0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// UnityEngine.Quaternion Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose::get_Rotation()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  MixedRealityPose_get_Rotation_m884C69734F7CD693912ECF2ACC36904CB18BE907_inline (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MixedRealityPose__ctor_m8B1A703ACB7F2BA742D3960612891435AD5D4D28 (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m4DC00BAE7DFA4CF7ECDD2E576E31E5FA31F36DE7 (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * __this, int32_t ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m535A937118A4E4A94E86B49AAB1B01A18619AD8B_gshared)(__this, ___key0, method);
}
// System.Void System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>::Add(!0,!1)
inline void Dictionary_2_Add_mD183155545F86332FE7DF071F13B274F6D514344 (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * __this, int32_t ___key0, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 *, int32_t, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 , const RuntimeMethod*))Dictionary_2_Add_m944621C1FD5CFA4DBCC1D9DEAF5AD285AC07C426_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_m899DD9BD808EDEA1D10982AD411594DEC9DE218A (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * __this, int32_t ___key0, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 *, int32_t, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 , const RuntimeMethod*))Dictionary_2_set_Item_m354BF9B9ABF0B1DBFCA2A2C1DF6932460825124D_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::.ctor()
inline void Dictionary_2__ctor_mB3D1F54DCBB3BAAA711763CA6512DEE2A954228C (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 *, const RuntimeMethod*))Dictionary_2__ctor_mB1BD995056039C2AD2CE07E04A53E652A6C039B9_gshared)(__this, method);
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XRSDKDeviceManager__ctor_mABAC44F39E97E651B7EDE61E11058320828012A7 (XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7 * __this, RuntimeObject* ___inputSystem0, String_t* ___name1, uint32_t ___priority2, BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3 * ___profile3, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Type Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::GetControllerType(Microsoft.MixedReality.Toolkit.Input.SupportedControllerType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * XRSDKDeviceManager_GetControllerType_mA71B0AFA13ED0D198F6B878E3773BF8734F21671 (XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7 * __this, int32_t ___supportedControllerType0, const RuntimeMethod* method);
// Microsoft.MixedReality.Toolkit.Input.InputSourceType Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::GetInputSourceType(Microsoft.MixedReality.Toolkit.Input.SupportedControllerType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t XRSDKDeviceManager_GetInputSourceType_m2BD4DEA82E00D7CFEF0954425C67B6232E04D6E1 (XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7 * __this, int32_t ___supportedControllerType0, const RuntimeMethod* method);
// UnityEngine.XR.InputDeviceCharacteristics UnityEngine.XR.InputDevice::get_characteristics()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t InputDevice_get_characteristics_mF7A1F32CD82CA6EE97AD113089F063109322C63A (InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * __this, const RuntimeMethod* method);
// System.Boolean System.Enum::HasFlag(System.Enum)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enum_HasFlag_mF1BCDA3744B0CC82C7A3D7313C0858E82010151E (RuntimeObject * __this, Enum_t23B90B40F60E677A8025267341651C94AE079CDA * ___flag0, const RuntimeMethod* method);
// Microsoft.MixedReality.Toolkit.Input.SupportedControllerType Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::GetCurrentControllerType(UnityEngine.XR.InputDevice)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t XRSDKDeviceManager_GetCurrentControllerType_m28138A9FA1C6A4ADBBA48FFFEF22BAD47AA0FB19 (XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7 * __this, InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  ___inputDevice0, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
inline bool Nullable_1_get_HasValue_m52F33C6963C9A76A14F54B4B2C30CEF580DB1EB7_inline (Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 *, const RuntimeMethod*))Nullable_1_get_HasValue_m52F33C6963C9A76A14F54B4B2C30CEF580DB1EB7_gshared_inline)(__this, method);
}
// System.Void System.Nullable`1<System.Boolean>::.ctor(!0)
inline void Nullable_1__ctor_m402A94AC9070B345C95919DCFFFF627252B3E67A (Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * __this, bool ___value0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 *, bool, const RuntimeMethod*))Nullable_1__ctor_m402A94AC9070B345C95919DCFFFF627252B3E67A_gshared)(__this, ___value0, method);
}
// !0 System.Nullable`1<System.Boolean>::GetValueOrDefault()
inline bool Nullable_1_GetValueOrDefault_mBECC58FEFD1088EC070D9F9A892ECD1D8BBF2A0F_inline (Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 *, const RuntimeMethod*))Nullable_1_GetValueOrDefault_mBECC58FEFD1088EC070D9F9A892ECD1D8BBF2A0F_gshared_inline)(__this, method);
}
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::get_IsActiveLoader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OculusXRSDKDeviceManager_get_IsActiveLoader_m3AA564057D5F1D64A3E5F98D8F4191BF726E37E8 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.BaseService::Enable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseService_Enable_m60768260471BD4A6E1EF8D6A9CD661CB90E445B3 (BaseService_t793754DF90682F7505A073585FC34F790A7CD7DB * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::SetupInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_SetupInput_m1E47F82D6614002FF0D875E7A6BEEB69C8AAB03D (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::ConfigurePerformancePreferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_ConfigurePerformancePreferences_m977EEF04FE8EBB33DD433802997E9796CAD201B0 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Input.XRSDKDeviceManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XRSDKDeviceManager_Update_m7DD58B2925107ADF7A0903F107C63D0092DE9C02 (XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7 * __this, const RuntimeMethod* method);
// System.Boolean OVRPlugin::GetHandTrackingEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OVRPlugin_GetHandTrackingEnabled_m36DCF981657D16F798B7D880C6480FD0D1429FFF (const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::UpdateHands()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_UpdateHands_m957A3B4619FDE131C5537B4F19537D802CB27989 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::RemoveAllHandDevices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_RemoveAllHandDevices_m6BF093E29DECA10A3CF59199E2CAB0031992AE55 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<OVRCameraRig>()
inline OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * Object_FindObjectOfType_TisOVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517_mCD0D2DA6EF2EE7BAF6D2611A748177C0E8894A31 (const RuntimeMethod* method)
{
	return ((  OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m25AA6DB6AABFD5D66AFA1A8C0E91A7AF61429C37_gshared)(method);
}
// UnityEngine.Camera Microsoft.MixedReality.Toolkit.Utilities.CameraCache::get_Main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * CameraCache_get_Main_m9B531633C9F0EEFE28FE56E6F9609A91E0EC3F5A (const RuntimeMethod* method);
// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::get_SettingsProfile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * OculusXRSDKDeviceManager_get_SettingsProfile_m48D2E91D64F9DB2A7D4C48E65E0C48D426111007 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method);
// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_OVRCameraRigPrefab()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * OculusXRSDKDeviceManagerProfile_get_OVRCameraRigPrefab_m9605894F5836881F8C1D4A45CA15642470E32F1B_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m4039C8E65629D33E1EC84D2505BF1D5DDC936622_gshared)(___original0, method);
}
// !!0 UnityEngine.GameObject::GetComponent<OVRCameraRig>()
inline OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * GameObject_GetComponent_TisOVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517_m6146EEB03DE9A96D5EEC6C066EB360E747B2C297 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.Transform OVRCameraRig::get_trackingSpace()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * OVRCameraRig_get_trackingSpace_mB0190AF5A79E489DB09B4C61D4E3BE8FA9AD8D9E_inline (OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_Find_mB1687901A4FB0D562C44A93CC67CD35DCFCAABA1 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, String_t* ___n0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___p0, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// UnityEngine.Transform Microsoft.MixedReality.Toolkit.MixedRealityPlayspace::get_Transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * MixedRealityPlayspace_get_Transform_mCCE29FC5A6A15F7E4BFE9AE45BC18C9A11F6BAFA (const RuntimeMethod* method);
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_RenderAvatarHandsInsteadOfController()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool OculusXRSDKDeviceManagerProfile_get_RenderAvatarHandsInsteadOfController_mF6CB8DFE7E8E4CE0E933C52DB78FE62681A05FE8_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<OVRControllerHelper>()
inline OVRControllerHelperU5BU5D_tC1041712E5487B448EA158DCEAE5D8D1F8996DD9* GameObject_GetComponentsInChildren_TisOVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6_mFADE4B7CBB98CF80A59C147FAD9C274B3118265F (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  OVRControllerHelperU5BU5D_tC1041712E5487B448EA158DCEAE5D8D1F8996DD9* (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m6662AE3C936281A25097CCBD9098A9F85C69279A_gshared)(__this, method);
}
// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_LocalAvatarPrefab()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * OculusXRSDKDeviceManagerProfile_get_LocalAvatarPrefab_m108B0B52B77100E76CC4CF0B6CAA60CA809A40C7_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent1, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mD211EB15E9E128684605B4CC7277F10840F8E8CF_gshared)(___original0, ___parent1, method);
}
// !!0[] UnityEngine.Component::GetComponentsInChildren<OVRHand>()
inline OVRHandU5BU5D_tDE7CB5D0BAC901AB98623D26B8A07D4DAA25C6DF* Component_GetComponentsInChildren_TisOVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A_m5D03A208D3426756262B26B81993BB82D62D1CBA (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  OVRHandU5BU5D_tDE7CB5D0BAC901AB98623D26B8A07D4DAA25C6DF* (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_mCA5B356D4B0824C6DE60A8E90E6A6D4188C56C2F_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<OVRSkeleton>()
inline OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * Component_GetComponent_TisOVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC_mDA071F49B497F5BEA47FB7193E04836BAD026E1A (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::ApplyConfiguredPerformanceSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_ApplyConfiguredPerformanceSettings_m3CFB10570072E9928FC7449925A335CF849FCD72 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::UpdateHand(OVRHand,OVRSkeleton,Microsoft.MixedReality.Toolkit.Utilities.Handedness)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_UpdateHand_m1F64190BDFAD74AA9ED0C14927C1F4D551DD51A0 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * ___ovrHand0, OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * ___ovrSkeleton1, uint8_t ___handedness2, const RuntimeMethod* method);
// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::GetOrAddHand(Microsoft.MixedReality.Toolkit.Utilities.Handedness,OVRHand)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * OculusXRSDKDeviceManager_GetOrAddHand_m6FD299BDE1329B287E6DE2A20624FAC07BD0ED07 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, uint8_t ___handedness0, OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * ___ovrHand1, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::UpdateController(OVRHand,OVRSkeleton,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_UpdateController_m2DA65146BC019D090B00475D83712BC096E623F7 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * ___hand0, OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * ___ovrSkeleton1, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___trackingOrigin2, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::RemoveHandDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_RemoveHandDevice_mC666E6FB87AA40E9664C85CE53FA1B0BBFA0C59B (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, uint8_t ___handedness0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m0620BD3B7D5DE1DB5488BA648F10C4AFF085DA44 (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * __this, uint8_t ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 *, uint8_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m18463BD56578964BC10206CA83C202E5D51FD502_gshared)(__this, ___key0, method);
}
// !1 System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::get_Item(!0)
inline OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * Dictionary_2_get_Item_m130BCA3A6BF1FB071A48981A972E9E42C03BB173 (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * __this, uint8_t ___key0, const RuntimeMethod* method)
{
	return ((  OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * (*) (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 *, uint8_t, const RuntimeMethod*))Dictionary_2_get_Item_mEA5665B69C064636B283B6E324FAEA99DDC82A18_gshared)(__this, ___key0, method);
}
// !0 Microsoft.MixedReality.Toolkit.BaseDataProvider`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem>::get_Service()
inline RuntimeObject* BaseDataProvider_1_get_Service_mF32AD96A346149DFD23B70B4300F9D47A232E778_inline (BaseDataProvider_1_t1D6E5ED6946EB7AF9582569B8384D8800F0BBE34 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (BaseDataProvider_1_t1D6E5ED6946EB7AF9582569B8384D8800F0BBE34 *, const RuntimeMethod*))BaseDataProvider_1_get_Service_m041BFCBFB9FCF6AD8183DE3BE1736CEAA9048863_gshared_inline)(__this, method);
}
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17 (String_t* ___format0, RuntimeObject * ___arg01, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand__ctor_mC784743C8DAED2DE69F8CDA1EAD5AD25ECD7C59D (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, int32_t ___trackingState0, uint8_t ___controllerHandedness1, RuntimeObject* ___inputSource2, MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* ___interactions3, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::InitializeHand(OVRHand,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_InitializeHand_mF0675D6506634F5F72B12A385BC305A5E25F1ADD (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * ___ovrHand0, OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * ___deviceManagerSettings1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::Add(!0,!1)
inline void Dictionary_2_Add_mBF9F9FCB9B086F9959CD47F46FEF82DB67C01A98 (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * __this, uint8_t ___key0, OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 *, uint8_t, OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 *, const RuntimeMethod*))Dictionary_2_Add_m0ED5FEF3A5E5E1AFCE9C2F5CAB746A29C7730059_gshared)(__this, ___key0, ___value1, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_mE4FFA1E538E65BD642AE31936056434FE06E5306 (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * __this, uint8_t ___key0, OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 ** ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 *, uint8_t, OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 **, const RuntimeMethod*))Dictionary_2_TryGetValue_mEFC0DB89DF161BD8BEF226C8150B5216A5665F6A_gshared)(__this, ___key0, ___value1, method);
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::RemoveHandDevice(Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_RemoveHandDevice_mBF1373373E760324796F80DC8B67CE9EE1DDF598 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * ___handDevice0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::get_Count()
inline int32_t Dictionary_2_get_Count_mB586D4F1BB640E1344BE62A6FAFC9EBFEBA058F2 (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 *, const RuntimeMethod*))Dictionary_2_get_Count_m20EE44B3BEDA1B90B843AE95B79715F1DEE3B537_gshared)(__this, method);
}
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::get_Values()
inline ValueCollection_t676F47F68B8CA40F279CA8C5502E572600166614 * Dictionary_2_get_Values_mC6AAA65393E9DC180F2A7E33AE230C3C0B3043CB (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * __this, const RuntimeMethod* method)
{
	return ((  ValueCollection_t676F47F68B8CA40F279CA8C5502E572600166614 * (*) (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 *, const RuntimeMethod*))Dictionary_2_get_Values_mFEBA424428F76B6060D7B261B41DDA8927275FB0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_m757D094B08518BB620126F91954AE3B3F54918B5 (List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m1D864B65CCD0498EC4BFFBDA8F8D04AE5333195A_gshared)(__this, ___collection0, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::GetEnumerator()
inline Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6  List_1_GetEnumerator_mABE9872DD40A96E02EB4939FFF85A51EA450C6A2 (List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6  (*) (List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::get_Current()
inline OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * Enumerator_get_Current_m6C1D5E2616E3F0772514421F05D8507D5316FCB1_inline (Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6 * __this, const RuntimeMethod* method)
{
	return ((  OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * (*) (Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::MoveNext()
inline bool Enumerator_MoveNext_mF796BF6B5102FFD669E3C1AB8837158CE4269E47 (Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::Dispose()
inline void Enumerator_Dispose_m3E5463E197FDA243351F081A7D6B6E56A1DF8746 (Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::Clear()
inline void Dictionary_2_Clear_mA0FFB84584221F5822A86A7C30FAAC00546DE437 (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 *, const RuntimeMethod*))Dictionary_2_Clear_m0C5D3DDD1E23EBEFEBF57B1A58EBAED4359418C1_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand>::Remove(!0)
inline bool Dictionary_2_Remove_m533F4D4B5A32EB3E23F19ADD7C3FB48775D61675 (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * __this, uint8_t ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 *, uint8_t, const RuntimeMethod*))Dictionary_2_Remove_mF0624214F2AE94824B5955D3CD09527AA94D536F_gshared)(__this, ___key0, method);
}
// System.Void UnityEngine.XR.XRSettings::set_eyeTextureResolutionScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XRSettings_set_eyeTextureResolutionScale_mBABE036EB59B20AF02A26771F34BB35453C806D2 (float ___value0, const RuntimeMethod* method);
// System.Int32 Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_CPULevel()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManagerProfile_get_CPULevel_m0D2BC7B50B1D2BD9D65F926DD0FD082A991C2C21_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method);
// System.Void OVRManager::set_cpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OVRManager_set_cpuLevel_m732E7AD28186E2026DFF38D4BDA99D61D836C1AB (int32_t ___value0, const RuntimeMethod* method);
// System.Int32 Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_GPULevel()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManagerProfile_get_GPULevel_mC49905ABE184F2D0EE61F625F0406A416D11393E_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method);
// System.Void OVRManager::set_gpuLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OVRManager_set_gpuLevel_mED660E0DACAFFA9412BA1D4FD3ACEA0472A45CC5 (int32_t ___value0, const RuntimeMethod* method);
// System.Boolean OVRManager::get_fixedFoveatedRenderingSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OVRManager_get_fixedFoveatedRenderingSupported_mECE4C8001356DD64815603FF7BDDD2B093F61D21 (const RuntimeMethod* method);
// System.Void OVRManager::set_fixedFoveatedRenderingLevel(OVRManager/FixedFoveatedRenderingLevel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OVRManager_set_fixedFoveatedRenderingLevel_mD408E6CA2F65DB79BE618CA27E7AB19A8227A3C5 (int32_t ___value0, const RuntimeMethod* method);
// System.Void OVRManager::set_useDynamicFixedFoveatedRendering(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OVRManager_set_useDynamicFixedFoveatedRendering_m6F1C8157EA92866C6CE9C412A1F14E946494C2FC (bool ___value0, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMixedRealityProfile__ctor_m508EE2B2B8F00BE219CEAF7B02813DC1F9C7F45F (BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3 * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.Input.OculusTouchControllerDefinition::.ctor(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusTouchControllerDefinition__ctor_mF17722C0CC08F770C1A128FEF0373DCA3B6538FA (OculusTouchControllerDefinition_t9C81F5CDBD5908C5D1CE58FB90A4375457C7F831 * __this, uint8_t ___handedness0, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[],Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSourceDefinition)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GenericXRSDKController__ctor_m377D0A746594421F94E21527260B09F6789AFE1F (GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4 * __this, int32_t ___trackingState0, uint8_t ___controllerHandedness1, RuntimeObject* ___inputSource2, MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* ___interactions3, RuntimeObject* ___definition4, const RuntimeMethod* method);
// Unity.Profiling.ProfilerMarker/AutoScope Unity.Profiling.ProfilerMarker::Auto()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D  ProfilerMarker_Auto_m136C2D8B4232A186FA4C8866BB389A5DCF69E0E5_inline (ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * __this, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Input.GenericXRSDKController::UpdateButtonData(Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping,UnityEngine.XR.InputDevice)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GenericXRSDKController_UpdateButtonData_m00755CCC7473D162B8594CA0AECD67CB29E06121 (GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4 * __this, MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * ___interactionMapping0, InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  ___inputDevice1, const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.InputDevice::TryGetFeatureValue(UnityEngine.XR.InputFeatureUsage`1<System.Boolean>,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputDevice_TryGetFeatureValue_mF70AB4F4BC703E9242A757D0A6D84A4DA48BBBBE (InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E * __this, InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  ___usage0, bool* ___value1, const RuntimeMethod* method);
// System.Void Unity.Profiling.ProfilerMarker/AutoScope::Dispose()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AutoScope_Dispose_m5CDDCDA2B8769738BB695661EC4AC55DD7A0D7CA_inline (AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D * __this, const RuntimeMethod* method);
// System.Void Unity.Profiling.ProfilerMarker::.ctor(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ProfilerMarker__ctor_mCE8D10CF2D2B2C4E51BF1BB66D75FDDE5BDA4A41_inline (ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
// System.Void Unity.Profiling.ProfilerMarker/AutoScope::.ctor(System.IntPtr)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AutoScope__ctor_m4131730A501F687FF95B2963EABAC7844C6B9859_inline (AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D * __this, intptr_t ___markerPtr0, const RuntimeMethod* method);
// System.Void Unity.Profiling.LowLevel.Unsafe.ProfilerUnsafeUtility::EndSample(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProfilerUnsafeUtility_EndSample_m0435B2EE7963614F3D154A83D44269FE4D1A85B0 (intptr_t ___markerPtr0, const RuntimeMethod* method);
// System.IntPtr Unity.Profiling.LowLevel.Unsafe.ProfilerUnsafeUtility::CreateMarker(System.String,System.UInt16,Unity.Profiling.LowLevel.MarkerFlags,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t ProfilerUnsafeUtility_CreateMarker_m419027084C68545B765B9345949D8BFCB96C51AD (String_t* ___name0, uint16_t ___categoryId1, uint16_t ___flags2, int32_t ___metadataCount3, const RuntimeMethod* method);
// System.Void Unity.Profiling.LowLevel.Unsafe.ProfilerUnsafeUtility::BeginSample(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProfilerUnsafeUtility_BeginSample_m1B2CAD1BC7C7C390514317A8D51FB798D4622AE4 (intptr_t ___markerPtr0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::get_HandPointerPose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  OculusHand_get_HandPointerPose_m211D5240993C90CCDD33ED2D2516D25C4B0B4E7C (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method)
{
	{
		// public MixedRealityPose HandPointerPose => currentPointerPose;
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_0 = __this->get_currentPointerPose_20();
		return L_0;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand__ctor_mC784743C8DAED2DE69F8CDA1EAD5AD25ECD7C59D (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, int32_t ___trackingState0, uint8_t ___controllerHandedness1, RuntimeObject* ___inputSource2, MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* ___interactions3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m11C00E81E12AE2761EDA2BB312BBE57AA4E8C696_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m8A8D33B5A8E9011511505D1AB8698F63E2622E51_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private MixedRealityPose currentPointerPose = MixedRealityPose.ZeroIdentity;
		IL2CPP_RUNTIME_CLASS_INIT(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669_il2cpp_TypeInfo_var);
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_0;
		L_0 = MixedRealityPose_get_ZeroIdentity_m0668ACB9F881F2046B81685B02D13BFD78338998_inline(/*hidden argument*/NULL);
		__this->set_currentPointerPose_20(L_0);
		// private MixedRealityPose currentGripPose = MixedRealityPose.ZeroIdentity;
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_1;
		L_1 = MixedRealityPose_get_ZeroIdentity_m0668ACB9F881F2046B81685B02D13BFD78338998_inline(/*hidden argument*/NULL);
		__this->set_currentGripPose_21(L_1);
		// protected readonly Dictionary<TrackedHandJoint, MixedRealityPose> jointPoses = new Dictionary<TrackedHandJoint, MixedRealityPose>();
		Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * L_2 = (Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 *)il2cpp_codegen_object_new(Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m11C00E81E12AE2761EDA2BB312BBE57AA4E8C696(L_2, /*hidden argument*/Dictionary_2__ctor_m11C00E81E12AE2761EDA2BB312BBE57AA4E8C696_RuntimeMethod_var);
		__this->set_jointPoses_26(L_2);
		// protected readonly Dictionary<BoneId, TrackedHandJoint> boneJointMapping = new Dictionary<BoneId, TrackedHandJoint>()
		// {
		//     { BoneId.Hand_Thumb1, TrackedHandJoint.ThumbMetacarpalJoint },
		//     { BoneId.Hand_Thumb2, TrackedHandJoint.ThumbProximalJoint },
		//     { BoneId.Hand_Thumb3, TrackedHandJoint.ThumbDistalJoint },
		//     { BoneId.Hand_ThumbTip, TrackedHandJoint.ThumbTip },
		//     { BoneId.Hand_Index1, TrackedHandJoint.IndexKnuckle },
		//     { BoneId.Hand_Index2, TrackedHandJoint.IndexMiddleJoint },
		//     { BoneId.Hand_Index3, TrackedHandJoint.IndexDistalJoint },
		//     { BoneId.Hand_IndexTip, TrackedHandJoint.IndexTip },
		//     { BoneId.Hand_Middle1, TrackedHandJoint.MiddleKnuckle },
		//     { BoneId.Hand_Middle2, TrackedHandJoint.MiddleMiddleJoint },
		//     { BoneId.Hand_Middle3, TrackedHandJoint.MiddleDistalJoint },
		//     { BoneId.Hand_MiddleTip, TrackedHandJoint.MiddleTip },
		//     { BoneId.Hand_Ring1, TrackedHandJoint.RingKnuckle },
		//     { BoneId.Hand_Ring2, TrackedHandJoint.RingMiddleJoint },
		//     { BoneId.Hand_Ring3, TrackedHandJoint.RingDistalJoint },
		//     { BoneId.Hand_RingTip, TrackedHandJoint.RingTip },
		//     { BoneId.Hand_Pinky1, TrackedHandJoint.PinkyKnuckle },
		//     { BoneId.Hand_Pinky2, TrackedHandJoint.PinkyMiddleJoint },
		//     { BoneId.Hand_Pinky3, TrackedHandJoint.PinkyDistalJoint },
		//     { BoneId.Hand_PinkyTip, TrackedHandJoint.PinkyTip },
		//     { BoneId.Hand_WristRoot, TrackedHandJoint.Wrist },
		// };
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_3 = (Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B *)il2cpp_codegen_object_new(Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m8A8D33B5A8E9011511505D1AB8698F63E2622E51(L_3, /*hidden argument*/Dictionary_2__ctor_m8A8D33B5A8E9011511505D1AB8698F63E2622E51_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_4 = L_3;
		NullCheck(L_4);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_4, 3, 3, /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_5 = L_4;
		NullCheck(L_5);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_5, 4, 4, /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_6 = L_5;
		NullCheck(L_6);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_6, 5, 5, /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_7 = L_6;
		NullCheck(L_7);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_7, ((int32_t)19), 6, /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_8 = L_7;
		NullCheck(L_8);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_8, 6, 8, /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_9 = L_8;
		NullCheck(L_9);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_9, 7, ((int32_t)9), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_10 = L_9;
		NullCheck(L_10);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_10, 8, ((int32_t)10), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_11 = L_10;
		NullCheck(L_11);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_11, ((int32_t)20), ((int32_t)11), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_12 = L_11;
		NullCheck(L_12);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_12, ((int32_t)9), ((int32_t)13), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_13 = L_12;
		NullCheck(L_13);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_13, ((int32_t)10), ((int32_t)14), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_14 = L_13;
		NullCheck(L_14);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_14, ((int32_t)11), ((int32_t)15), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_15 = L_14;
		NullCheck(L_15);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_15, ((int32_t)21), ((int32_t)16), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_16 = L_15;
		NullCheck(L_16);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_16, ((int32_t)12), ((int32_t)18), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_17 = L_16;
		NullCheck(L_17);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_17, ((int32_t)13), ((int32_t)19), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_18 = L_17;
		NullCheck(L_18);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_18, ((int32_t)14), ((int32_t)20), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_19 = L_18;
		NullCheck(L_19);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_19, ((int32_t)22), ((int32_t)21), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_20 = L_19;
		NullCheck(L_20);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_20, ((int32_t)16), ((int32_t)23), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_21 = L_20;
		NullCheck(L_21);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_21, ((int32_t)17), ((int32_t)24), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_22 = L_21;
		NullCheck(L_22);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_22, ((int32_t)18), ((int32_t)25), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_23 = L_22;
		NullCheck(L_23);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_23, ((int32_t)23), ((int32_t)26), /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_24 = L_23;
		NullCheck(L_24);
		Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8(L_24, 0, 1, /*hidden argument*/Dictionary_2_Add_m84E5466CE0E56CD95931D869F89BC82BF60875A8_RuntimeMethod_var);
		__this->set_boneJointMapping_30(L_24);
		// : base(trackingState, controllerHandedness, inputSource, interactions, new ArticulatedHandDefinition(inputSource, controllerHandedness))
		int32_t L_25 = ___trackingState0;
		uint8_t L_26 = ___controllerHandedness1;
		RuntimeObject* L_27 = ___inputSource2;
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_28 = ___interactions3;
		RuntimeObject* L_29 = ___inputSource2;
		uint8_t L_30 = ___controllerHandedness1;
		ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * L_31 = (ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 *)il2cpp_codegen_object_new(ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5_il2cpp_TypeInfo_var);
		ArticulatedHandDefinition__ctor_m502F18A61CCBE95F3FDE120D47C7A12619A83087(L_31, L_29, L_30, /*hidden argument*/NULL);
		BaseHand__ctor_mC039ECD3265CBEBD569E8AB59C4A96EB0AD97DEB(__this, L_25, L_26, L_27, L_28, L_31, /*hidden argument*/NULL);
		// { }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::SetupDefaultInteractions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_SetupDefaultInteractions_m305745C67748F950813D226DADDD5C655BF54929 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method)
{
	{
		// AssignControllerMappings(DefaultInteractions);
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_0;
		L_0 = VirtFuncInvoker0< MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* >::Invoke(17 /* Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[] Microsoft.MixedReality.Toolkit.Input.BaseController::get_DefaultInteractions() */, __this);
		BaseController_AssignControllerMappings_mE2C0913573EEB9BE3396880A642A2B8419138B43(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::TryGetJoint(Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OculusHand_TryGetJoint_m490A0CB44B4D683F6C806A266549E3FB250F070C (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, int32_t ___joint0, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * ___pose1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m91A28CD3479611BC6DE4868B185BC381A2114D78_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return jointPoses.TryGetValue(joint, out pose);
		Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * L_0 = __this->get_jointPoses_26();
		int32_t L_1 = ___joint0;
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * L_2 = ___pose1;
		NullCheck(L_0);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m91A28CD3479611BC6DE4868B185BC381A2114D78(L_0, L_1, (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)L_2, /*hidden argument*/Dictionary_2_TryGetValue_m91A28CD3479611BC6DE4868B185BC381A2114D78_RuntimeMethod_var);
		return L_3;
	}
}
// Microsoft.MixedReality.Toolkit.Input.ArticulatedHandDefinition Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::get_HandDefinition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * OculusHand_get_HandDefinition_mF576EE597AFC69C901F66DA59822582FFAAC1958 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * V_0 = NULL;
	ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * G_B2_0 = NULL;
	ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * G_B1_0 = NULL;
	{
		// private ArticulatedHandDefinition HandDefinition => handDefinition ?? (handDefinition = Definition as ArticulatedHandDefinition);
		ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * L_0 = __this->get_handDefinition_27();
		ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001e;
		}
	}
	{
		RuntimeObject* L_2;
		L_2 = VirtFuncInvoker0< RuntimeObject* >::Invoke(20 /* Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSourceDefinition Microsoft.MixedReality.Toolkit.Input.BaseController::get_Definition() */, __this);
		ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * L_3 = ((ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 *)IsInstClass((RuntimeObject*)L_2, ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5_il2cpp_TypeInfo_var));
		V_0 = L_3;
		__this->set_handDefinition_27(L_3);
		ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_001e:
	{
		return G_B2_0;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::InitializeHand(OVRHand,Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_InitializeHand_mF0675D6506634F5F72B12A385BC305A5E25F1ADD (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * ___ovrHand0, OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * ___deviceManagerSettings1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* G_B2_0 = NULL;
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * G_B2_1 = NULL;
	RuntimeObject* G_B1_0 = NULL;
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * G_B1_1 = NULL;
	MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 * G_B3_0 = NULL;
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * G_B3_1 = NULL;
	{
		// settingsProfile = deviceManagerSettings;
		OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * L_0 = ___deviceManagerSettings1;
		__this->set_settingsProfile_24(L_0);
		// handTrackingProfile = CoreServices.InputSystem?.InputSystemProfile.HandTrackingProfile;
		IL2CPP_RUNTIME_CLASS_INIT(CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		RuntimeObject* L_1;
		L_1 = CoreServices_get_InputSystem_m03F66B157660C21D67B5A6A4D132BF889CBFE440(/*hidden argument*/NULL);
		RuntimeObject* L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = __this;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = __this;
			goto IL_0014;
		}
	}
	{
		G_B3_0 = ((MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 *)(NULL));
		G_B3_1 = G_B1_1;
		goto IL_001e;
	}

IL_0014:
	{
		NullCheck(G_B2_0);
		MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C * L_3;
		L_3 = InterfaceFuncInvoker0< MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C * >::Invoke(6 /* Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::get_InputSystemProfile() */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B2_0);
		NullCheck(L_3);
		MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 * L_4;
		L_4 = MixedRealityInputSystemProfile_get_HandTrackingProfile_m68AB045F05D5B7E5723E8D3331E837CAC058CE7A_inline(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B2_1;
	}

IL_001e:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_handTrackingProfile_25(G_B3_0);
		// }
		return;
	}
}
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::get_IsInPointingPose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OculusHand_get_IsInPointingPose_mA8B926791FBE0D59DD7C0A7BEF96F64FFDC77399 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method)
{
	{
		// public override bool IsInPointingPose => HandDefinition.IsInPointingPose;
		ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * L_0;
		L_0 = OculusHand_get_HandDefinition_mF576EE597AFC69C901F66DA59822582FFAAC1958(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1;
		L_1 = ArticulatedHandDefinition_get_IsInPointingPose_m1591492D993E0A019C98DA078CA267648C7E4BBE(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::set_IsPinching(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_set_IsPinching_mAAEFC3C08B379B80AF3411AC98531581C90B1B4A (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// protected bool IsPinching { set; get; }
		bool L_0 = ___value0;
		__this->set_U3CIsPinchingU3Ek__BackingField_28(L_0);
		return;
	}
}
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::get_IsPinching()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OculusHand_get_IsPinching_m1DEFD901E5E5FE10261629E06E5941C2DEF0F707 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method)
{
	{
		// protected bool IsPinching { set; get; }
		bool L_0 = __this->get_U3CIsPinchingU3Ek__BackingField_28();
		return L_0;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::set_IsGrabbing(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_set_IsGrabbing_mD4B8A213D51B07938ED3B39C75D37BC0BF5C6872 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// protected bool IsGrabbing { set; get; }
		bool L_0 = ___value0;
		__this->set_U3CIsGrabbingU3Ek__BackingField_29(L_0);
		return;
	}
}
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::get_IsGrabbing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OculusHand_get_IsGrabbing_m461FECC082EDF20D3F788A5754B7231DC93EEA0A (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method)
{
	{
		// protected bool IsGrabbing { set; get; }
		bool L_0 = __this->get_U3CIsGrabbingU3Ek__BackingField_29();
		return L_0;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::UpdateController(OVRHand,OVRSkeleton,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_UpdateController_m2DA65146BC019D090B00475D83712BC096E623F7 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * ___hand0, OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * ___ovrSkeleton1, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___trackingOrigin2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m5A745DC8015EA0953A57CDB2866344D2AF45CC9E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_get_HasValue_m7455E879CFAAE682AE3786D4D2B1F65C8AA23921_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  V_7;
	memset((&V_7), 0, sizeof(V_7));
	RuntimeObject* G_B7_0 = NULL;
	RuntimeObject* G_B6_0 = NULL;
	RuntimeObject* G_B20_0 = NULL;
	RuntimeObject* G_B19_0 = NULL;
	RuntimeObject* G_B24_0 = NULL;
	RuntimeObject* G_B23_0 = NULL;
	MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * G_B27_0 = NULL;
	MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * G_B26_0 = NULL;
	int32_t G_B28_0 = 0;
	MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * G_B28_1 = NULL;
	RuntimeObject* G_B32_0 = NULL;
	RuntimeObject* G_B31_0 = NULL;
	RuntimeObject* G_B35_0 = NULL;
	RuntimeObject* G_B34_0 = NULL;
	MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * G_B38_0 = NULL;
	MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * G_B37_0 = NULL;
	int32_t G_B39_0 = 0;
	MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * G_B39_1 = NULL;
	RuntimeObject* G_B43_0 = NULL;
	RuntimeObject* G_B42_0 = NULL;
	RuntimeObject* G_B46_0 = NULL;
	RuntimeObject* G_B45_0 = NULL;
	MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* G_B52_0 = NULL;
	int32_t G_B52_1 = 0;
	MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* G_B51_0 = NULL;
	int32_t G_B51_1 = 0;
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  G_B53_0;
	memset((&G_B53_0), 0, sizeof(G_B53_0));
	int32_t G_B53_1 = 0;
	{
		// if (!Enabled || hand == null || ovrSkeleton == null)
		bool L_0;
		L_0 = BaseController_get_Enabled_m23BE305FD0E3643AB6B26D80F0F0FBB33CF285D4_inline(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_1 = ___hand0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * L_3 = ___ovrSkeleton1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001b;
		}
	}

IL_001a:
	{
		// return;
		return;
	}

IL_001b:
	{
		// bool isTracked = UpdateHandData(hand, ovrSkeleton);
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_5 = ___hand0;
		OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * L_6 = ___ovrSkeleton1;
		bool L_7;
		L_7 = OculusHand_UpdateHandData_m1A965734213BE9AB52AE8B439AF9FD9C58222105(__this, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		// IsPositionAvailable = IsRotationAvailable = isTracked;
		bool L_8 = V_0;
		bool L_9 = L_8;
		V_1 = L_9;
		BaseController_set_IsRotationAvailable_m36835958007BE5F83FC443D7837E00F38D31E7D8_inline(__this, L_9, /*hidden argument*/NULL);
		bool L_10 = V_1;
		BaseController_set_IsPositionAvailable_m8762BC7C636F247BED1DB49EE4509475DA072F44_inline(__this, L_10, /*hidden argument*/NULL);
		// if (isTracked)
		bool L_11 = V_0;
		if (!L_11)
		{
			goto IL_00c1;
		}
	}
	{
		// currentPointerPose.Position = trackingOrigin.TransformPoint(hand.PointerPose.position);
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * L_12 = __this->get_address_of_currentPointerPose_20();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13 = ___trackingOrigin2;
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_14 = ___hand0;
		NullCheck(L_14);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = OVRHand_get_PointerPose_m5FD5EE4BC344C1F3D2F85B1F8C6E3D0E518DF7A2_inline(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Transform_TransformPoint_m68AF95765A9279192E601208A9C5170027A5F0D2(L_13, L_16, /*hidden argument*/NULL);
		MixedRealityPose_set_Position_m119B5345F1415E482F83D6BD4E7B1F4951B7375A_inline((MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)L_12, L_17, /*hidden argument*/NULL);
		// Vector3 pointerForward = trackingOrigin.TransformDirection(hand.PointerPose.forward);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18 = ___trackingOrigin2;
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_19 = ___hand0;
		NullCheck(L_19);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = OVRHand_get_PointerPose_m5FD5EE4BC344C1F3D2F85B1F8C6E3D0E518DF7A2_inline(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Transform_TransformDirection_m6B5E3F0A7C6323159DEC6D9BC035FB53ADD96E91(L_18, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		// Vector3 pointerUp = trackingOrigin.TransformDirection(hand.PointerPose.up);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23 = ___trackingOrigin2;
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_24 = ___hand0;
		NullCheck(L_24);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_25;
		L_25 = OVRHand_get_PointerPose_m5FD5EE4BC344C1F3D2F85B1F8C6E3D0E518DF7A2_inline(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26;
		L_26 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_25, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Transform_TransformDirection_m6B5E3F0A7C6323159DEC6D9BC035FB53ADD96E91(L_23, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		// currentPointerPose.Rotation = Quaternion.LookRotation(pointerForward, pointerUp);
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * L_28 = __this->get_address_of_currentPointerPose_20();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29 = V_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30 = V_3;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_31;
		L_31 = Quaternion_LookRotation_m8A7DB5BDBC361586191AB67ACF857F46160EE3F1(L_29, L_30, /*hidden argument*/NULL);
		MixedRealityPose_set_Rotation_mF14187710EDE143DFC85AB2A0892FD72B793DF77_inline((MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)L_28, L_31, /*hidden argument*/NULL);
		// currentGripPose = jointPoses[TrackedHandJoint.Palm];
		Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * L_32 = __this->get_jointPoses_26();
		NullCheck(L_32);
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_33;
		L_33 = Dictionary_2_get_Item_m5A745DC8015EA0953A57CDB2866344D2AF45CC9E(L_32, 2, /*hidden argument*/Dictionary_2_get_Item_m5A745DC8015EA0953A57CDB2866344D2AF45CC9E_RuntimeMethod_var);
		__this->set_currentGripPose_21(L_33);
		// CoreServices.InputSystem?.RaiseSourcePoseChanged(InputSource, this, currentGripPose);
		IL2CPP_RUNTIME_CLASS_INIT(CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		RuntimeObject* L_34;
		L_34 = CoreServices_get_InputSystem_m03F66B157660C21D67B5A6A4D132BF889CBFE440(/*hidden argument*/NULL);
		RuntimeObject* L_35 = L_34;
		G_B6_0 = L_35;
		if (L_35)
		{
			G_B7_0 = L_35;
			goto IL_00a9;
		}
	}
	{
		goto IL_00bb;
	}

IL_00a9:
	{
		RuntimeObject* L_36;
		L_36 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(__this, /*hidden argument*/NULL);
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_37 = __this->get_currentGripPose_21();
		NullCheck(G_B7_0);
		InterfaceActionInvoker3< RuntimeObject*, RuntimeObject*, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  >::Invoke(29 /* System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::RaiseSourcePoseChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose) */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B7_0, L_36, __this, L_37);
	}

IL_00bb:
	{
		// UpdateVelocity();
		BaseHand_UpdateVelocity_mD2D4D7FC66091E4061F281E9351A696B6FC8D356(__this, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		// for (int i = 0; i < Interactions?.Length; i++)
		V_4 = 0;
		goto IL_034c;
	}

IL_00c9:
	{
		// switch (Interactions[i].InputType)
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_38;
		L_38 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_39 = V_4;
		NullCheck(L_38);
		int32_t L_40 = L_39;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		NullCheck(L_41);
		int32_t L_42;
		L_42 = MixedRealityInteractionMapping_get_InputType_mF467F74CF7AAA7B1B3C3EB6BE950BEABA6F1A435_inline(L_41, /*hidden argument*/NULL);
		V_5 = L_42;
		int32_t L_43 = V_5;
		if ((((int32_t)L_43) > ((int32_t)((int32_t)17))))
		{
			goto IL_0107;
		}
	}
	{
		int32_t L_44 = V_5;
		if ((((int32_t)L_44) == ((int32_t)3)))
		{
			goto IL_011e;
		}
	}
	{
		int32_t L_45 = V_5;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_45, (int32_t)((int32_t)13))))
		{
			case 0:
			{
				goto IL_0282;
			}
			case 1:
			{
				goto IL_017d;
			}
			case 2:
			{
				goto IL_0346;
			}
			case 3:
			{
				goto IL_0346;
			}
			case 4:
			{
				goto IL_0332;
			}
		}
	}
	{
		goto IL_0346;
	}

IL_0107:
	{
		int32_t L_46 = V_5;
		if ((((int32_t)L_46) == ((int32_t)((int32_t)25))))
		{
			goto IL_01dc;
		}
	}
	{
		int32_t L_47 = V_5;
		if ((((int32_t)L_47) == ((int32_t)((int32_t)33))))
		{
			goto IL_031c;
		}
	}
	{
		goto IL_0346;
	}

IL_011e:
	{
		// Interactions[i].PoseData = currentPointerPose;
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_48;
		L_48 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_49 = V_4;
		NullCheck(L_48);
		int32_t L_50 = L_49;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_52 = __this->get_currentPointerPose_20();
		NullCheck(L_51);
		MixedRealityInteractionMapping_set_PoseData_m79D33F56E91FB9CE5BE2A86A78F9F2492AA558F1(L_51, L_52, /*hidden argument*/NULL);
		// if (Interactions[i].Changed)
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_53;
		L_53 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_54 = V_4;
		NullCheck(L_53);
		int32_t L_55 = L_54;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		NullCheck(L_56);
		bool L_57;
		L_57 = MixedRealityInteractionMapping_get_Changed_mA0C90E795316CFE4F8E911F7371D6E47A433FEDF(L_56, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_0346;
		}
	}
	{
		// CoreServices.InputSystem?.RaisePoseInputChanged(InputSource, ControllerHandedness, Interactions[i].MixedRealityInputAction, currentPointerPose);
		IL2CPP_RUNTIME_CLASS_INIT(CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		RuntimeObject* L_58;
		L_58 = CoreServices_get_InputSystem_m03F66B157660C21D67B5A6A4D132BF889CBFE440(/*hidden argument*/NULL);
		RuntimeObject* L_59 = L_58;
		G_B19_0 = L_59;
		if (L_59)
		{
			G_B20_0 = L_59;
			goto IL_0153;
		}
	}
	{
		goto IL_0346;
	}

IL_0153:
	{
		RuntimeObject* L_60;
		L_60 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(__this, /*hidden argument*/NULL);
		uint8_t L_61;
		L_61 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_62;
		L_62 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_63 = V_4;
		NullCheck(L_62);
		int32_t L_64 = L_63;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_65);
		MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  L_66;
		L_66 = MixedRealityInteractionMapping_get_MixedRealityInputAction_m65770DAB10749381CC2E06AE4F4D4756AA65424A_inline(L_65, /*hidden argument*/NULL);
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_67 = __this->get_currentPointerPose_20();
		NullCheck(G_B20_0);
		InterfaceActionInvoker4< RuntimeObject*, uint8_t, MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE , MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  >::Invoke(44 /* System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::RaisePoseInputChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose) */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B20_0, L_60, L_61, L_66, L_67);
		// break;
		goto IL_0346;
	}

IL_017d:
	{
		// Interactions[i].PoseData = currentGripPose;
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_68;
		L_68 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_69 = V_4;
		NullCheck(L_68);
		int32_t L_70 = L_69;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_71 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_72 = __this->get_currentGripPose_21();
		NullCheck(L_71);
		MixedRealityInteractionMapping_set_PoseData_m79D33F56E91FB9CE5BE2A86A78F9F2492AA558F1(L_71, L_72, /*hidden argument*/NULL);
		// if (Interactions[i].Changed)
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_73;
		L_73 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_74 = V_4;
		NullCheck(L_73);
		int32_t L_75 = L_74;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_76 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_75));
		NullCheck(L_76);
		bool L_77;
		L_77 = MixedRealityInteractionMapping_get_Changed_mA0C90E795316CFE4F8E911F7371D6E47A433FEDF(L_76, /*hidden argument*/NULL);
		if (!L_77)
		{
			goto IL_0346;
		}
	}
	{
		// CoreServices.InputSystem?.RaisePoseInputChanged(InputSource, ControllerHandedness, Interactions[i].MixedRealityInputAction, currentGripPose);
		IL2CPP_RUNTIME_CLASS_INIT(CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		RuntimeObject* L_78;
		L_78 = CoreServices_get_InputSystem_m03F66B157660C21D67B5A6A4D132BF889CBFE440(/*hidden argument*/NULL);
		RuntimeObject* L_79 = L_78;
		G_B23_0 = L_79;
		if (L_79)
		{
			G_B24_0 = L_79;
			goto IL_01b2;
		}
	}
	{
		goto IL_0346;
	}

IL_01b2:
	{
		RuntimeObject* L_80;
		L_80 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(__this, /*hidden argument*/NULL);
		uint8_t L_81;
		L_81 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_82;
		L_82 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_83 = V_4;
		NullCheck(L_82);
		int32_t L_84 = L_83;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_85 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		NullCheck(L_85);
		MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  L_86;
		L_86 = MixedRealityInteractionMapping_get_MixedRealityInputAction_m65770DAB10749381CC2E06AE4F4D4756AA65424A_inline(L_85, /*hidden argument*/NULL);
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_87 = __this->get_currentGripPose_21();
		NullCheck(G_B24_0);
		InterfaceActionInvoker4< RuntimeObject*, uint8_t, MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE , MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  >::Invoke(44 /* System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::RaisePoseInputChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose) */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B24_0, L_80, L_81, L_86, L_87);
		// break;
		goto IL_0346;
	}

IL_01dc:
	{
		// Interactions[i].BoolData = IsPinching || IsGrabbing;
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_88;
		L_88 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_89 = V_4;
		NullCheck(L_88);
		int32_t L_90 = L_89;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_91 = (L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_90));
		bool L_92;
		L_92 = OculusHand_get_IsPinching_m1DEFD901E5E5FE10261629E06E5941C2DEF0F707_inline(__this, /*hidden argument*/NULL);
		G_B26_0 = L_91;
		if (L_92)
		{
			G_B27_0 = L_91;
			goto IL_01f5;
		}
	}
	{
		bool L_93;
		L_93 = OculusHand_get_IsGrabbing_m461FECC082EDF20D3F788A5754B7231DC93EEA0A_inline(__this, /*hidden argument*/NULL);
		G_B28_0 = ((int32_t)(L_93));
		G_B28_1 = G_B26_0;
		goto IL_01f6;
	}

IL_01f5:
	{
		G_B28_0 = 1;
		G_B28_1 = G_B27_0;
	}

IL_01f6:
	{
		NullCheck(G_B28_1);
		MixedRealityInteractionMapping_set_BoolData_mB9E1F1E8CC1CCC3ADFDFB08A564FA528A6399068(G_B28_1, (bool)G_B28_0, /*hidden argument*/NULL);
		// if (Interactions[i].Changed)
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_94;
		L_94 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_95 = V_4;
		NullCheck(L_94);
		int32_t L_96 = L_95;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_97 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		NullCheck(L_97);
		bool L_98;
		L_98 = MixedRealityInteractionMapping_get_Changed_mA0C90E795316CFE4F8E911F7371D6E47A433FEDF(L_97, /*hidden argument*/NULL);
		if (!L_98)
		{
			goto IL_0346;
		}
	}
	{
		// if (Interactions[i].BoolData)
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_99;
		L_99 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_100 = V_4;
		NullCheck(L_99);
		int32_t L_101 = L_100;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_102 = (L_99)->GetAt(static_cast<il2cpp_array_size_t>(L_101));
		NullCheck(L_102);
		bool L_103;
		L_103 = MixedRealityInteractionMapping_get_BoolData_m002148D894B01A0998785DE1E8149B433174A79A_inline(L_102, /*hidden argument*/NULL);
		if (!L_103)
		{
			goto IL_0250;
		}
	}
	{
		// CoreServices.InputSystem?.RaiseOnInputDown(InputSource, ControllerHandedness, Interactions[i].MixedRealityInputAction);
		IL2CPP_RUNTIME_CLASS_INIT(CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		RuntimeObject* L_104;
		L_104 = CoreServices_get_InputSystem_m03F66B157660C21D67B5A6A4D132BF889CBFE440(/*hidden argument*/NULL);
		RuntimeObject* L_105 = L_104;
		G_B31_0 = L_105;
		if (L_105)
		{
			G_B32_0 = L_105;
			goto IL_022c;
		}
	}
	{
		goto IL_0346;
	}

IL_022c:
	{
		RuntimeObject* L_106;
		L_106 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(__this, /*hidden argument*/NULL);
		uint8_t L_107;
		L_107 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_108;
		L_108 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_109 = V_4;
		NullCheck(L_108);
		int32_t L_110 = L_109;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_111 = (L_108)->GetAt(static_cast<il2cpp_array_size_t>(L_110));
		NullCheck(L_111);
		MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  L_112;
		L_112 = MixedRealityInteractionMapping_get_MixedRealityInputAction_m65770DAB10749381CC2E06AE4F4D4756AA65424A_inline(L_111, /*hidden argument*/NULL);
		NullCheck(G_B32_0);
		InterfaceActionInvoker3< RuntimeObject*, uint8_t, MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  >::Invoke(38 /* System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::RaiseOnInputDown(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction) */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B32_0, L_106, L_107, L_112);
		// }
		goto IL_0346;
	}

IL_0250:
	{
		// CoreServices.InputSystem?.RaiseOnInputUp(InputSource, ControllerHandedness, Interactions[i].MixedRealityInputAction);
		IL2CPP_RUNTIME_CLASS_INIT(CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		RuntimeObject* L_113;
		L_113 = CoreServices_get_InputSystem_m03F66B157660C21D67B5A6A4D132BF889CBFE440(/*hidden argument*/NULL);
		RuntimeObject* L_114 = L_113;
		G_B34_0 = L_114;
		if (L_114)
		{
			G_B35_0 = L_114;
			goto IL_025e;
		}
	}
	{
		goto IL_0346;
	}

IL_025e:
	{
		RuntimeObject* L_115;
		L_115 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(__this, /*hidden argument*/NULL);
		uint8_t L_116;
		L_116 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_117;
		L_117 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_118 = V_4;
		NullCheck(L_117);
		int32_t L_119 = L_118;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_120 = (L_117)->GetAt(static_cast<il2cpp_array_size_t>(L_119));
		NullCheck(L_120);
		MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  L_121;
		L_121 = MixedRealityInteractionMapping_get_MixedRealityInputAction_m65770DAB10749381CC2E06AE4F4D4756AA65424A_inline(L_120, /*hidden argument*/NULL);
		NullCheck(G_B35_0);
		InterfaceActionInvoker3< RuntimeObject*, uint8_t, MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  >::Invoke(39 /* System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::RaiseOnInputUp(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction) */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B35_0, L_115, L_116, L_121);
		// break;
		goto IL_0346;
	}

IL_0282:
	{
		// Interactions[i].BoolData = IsPinching || IsGrabbing;
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_122;
		L_122 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_123 = V_4;
		NullCheck(L_122);
		int32_t L_124 = L_123;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_125 = (L_122)->GetAt(static_cast<il2cpp_array_size_t>(L_124));
		bool L_126;
		L_126 = OculusHand_get_IsPinching_m1DEFD901E5E5FE10261629E06E5941C2DEF0F707_inline(__this, /*hidden argument*/NULL);
		G_B37_0 = L_125;
		if (L_126)
		{
			G_B38_0 = L_125;
			goto IL_029b;
		}
	}
	{
		bool L_127;
		L_127 = OculusHand_get_IsGrabbing_m461FECC082EDF20D3F788A5754B7231DC93EEA0A_inline(__this, /*hidden argument*/NULL);
		G_B39_0 = ((int32_t)(L_127));
		G_B39_1 = G_B37_0;
		goto IL_029c;
	}

IL_029b:
	{
		G_B39_0 = 1;
		G_B39_1 = G_B38_0;
	}

IL_029c:
	{
		NullCheck(G_B39_1);
		MixedRealityInteractionMapping_set_BoolData_mB9E1F1E8CC1CCC3ADFDFB08A564FA528A6399068(G_B39_1, (bool)G_B39_0, /*hidden argument*/NULL);
		// if (Interactions[i].Changed)
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_128;
		L_128 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_129 = V_4;
		NullCheck(L_128);
		int32_t L_130 = L_129;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_131 = (L_128)->GetAt(static_cast<il2cpp_array_size_t>(L_130));
		NullCheck(L_131);
		bool L_132;
		L_132 = MixedRealityInteractionMapping_get_Changed_mA0C90E795316CFE4F8E911F7371D6E47A433FEDF(L_131, /*hidden argument*/NULL);
		if (!L_132)
		{
			goto IL_0346;
		}
	}
	{
		// if (Interactions[i].BoolData)
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_133;
		L_133 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_134 = V_4;
		NullCheck(L_133);
		int32_t L_135 = L_134;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_136 = (L_133)->GetAt(static_cast<il2cpp_array_size_t>(L_135));
		NullCheck(L_136);
		bool L_137;
		L_137 = MixedRealityInteractionMapping_get_BoolData_m002148D894B01A0998785DE1E8149B433174A79A_inline(L_136, /*hidden argument*/NULL);
		if (!L_137)
		{
			goto IL_02f0;
		}
	}
	{
		// CoreServices.InputSystem?.RaiseOnInputDown(InputSource, ControllerHandedness, Interactions[i].MixedRealityInputAction);
		IL2CPP_RUNTIME_CLASS_INIT(CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		RuntimeObject* L_138;
		L_138 = CoreServices_get_InputSystem_m03F66B157660C21D67B5A6A4D132BF889CBFE440(/*hidden argument*/NULL);
		RuntimeObject* L_139 = L_138;
		G_B42_0 = L_139;
		if (L_139)
		{
			G_B43_0 = L_139;
			goto IL_02cf;
		}
	}
	{
		goto IL_0346;
	}

IL_02cf:
	{
		RuntimeObject* L_140;
		L_140 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(__this, /*hidden argument*/NULL);
		uint8_t L_141;
		L_141 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_142;
		L_142 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_143 = V_4;
		NullCheck(L_142);
		int32_t L_144 = L_143;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_145 = (L_142)->GetAt(static_cast<il2cpp_array_size_t>(L_144));
		NullCheck(L_145);
		MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  L_146;
		L_146 = MixedRealityInteractionMapping_get_MixedRealityInputAction_m65770DAB10749381CC2E06AE4F4D4756AA65424A_inline(L_145, /*hidden argument*/NULL);
		NullCheck(G_B43_0);
		InterfaceActionInvoker3< RuntimeObject*, uint8_t, MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  >::Invoke(38 /* System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::RaiseOnInputDown(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction) */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B43_0, L_140, L_141, L_146);
		// }
		goto IL_0346;
	}

IL_02f0:
	{
		// CoreServices.InputSystem?.RaiseOnInputUp(InputSource, ControllerHandedness, Interactions[i].MixedRealityInputAction);
		IL2CPP_RUNTIME_CLASS_INIT(CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		RuntimeObject* L_147;
		L_147 = CoreServices_get_InputSystem_m03F66B157660C21D67B5A6A4D132BF889CBFE440(/*hidden argument*/NULL);
		RuntimeObject* L_148 = L_147;
		G_B45_0 = L_148;
		if (L_148)
		{
			G_B46_0 = L_148;
			goto IL_02fb;
		}
	}
	{
		goto IL_0346;
	}

IL_02fb:
	{
		RuntimeObject* L_149;
		L_149 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(__this, /*hidden argument*/NULL);
		uint8_t L_150;
		L_150 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_151;
		L_151 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_152 = V_4;
		NullCheck(L_151);
		int32_t L_153 = L_152;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_154 = (L_151)->GetAt(static_cast<il2cpp_array_size_t>(L_153));
		NullCheck(L_154);
		MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  L_155;
		L_155 = MixedRealityInteractionMapping_get_MixedRealityInputAction_m65770DAB10749381CC2E06AE4F4D4756AA65424A_inline(L_154, /*hidden argument*/NULL);
		NullCheck(G_B46_0);
		InterfaceActionInvoker3< RuntimeObject*, uint8_t, MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  >::Invoke(39 /* System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::RaiseOnInputUp(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction) */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B46_0, L_149, L_150, L_155);
		// break;
		goto IL_0346;
	}

IL_031c:
	{
		// HandDefinition.UpdateCurrentIndexPose(Interactions[i]);
		ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * L_156;
		L_156 = OculusHand_get_HandDefinition_mF576EE597AFC69C901F66DA59822582FFAAC1958(__this, /*hidden argument*/NULL);
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_157;
		L_157 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_158 = V_4;
		NullCheck(L_157);
		int32_t L_159 = L_158;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_160 = (L_157)->GetAt(static_cast<il2cpp_array_size_t>(L_159));
		NullCheck(L_156);
		ArticulatedHandDefinition_UpdateCurrentIndexPose_mD2E582B958E504018BE34CC26AE420555D8DE754(L_156, L_160, /*hidden argument*/NULL);
		// break;
		goto IL_0346;
	}

IL_0332:
	{
		// HandDefinition.UpdateCurrentTeleportPose(Interactions[i]);
		ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * L_161;
		L_161 = OculusHand_get_HandDefinition_mF576EE597AFC69C901F66DA59822582FFAAC1958(__this, /*hidden argument*/NULL);
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_162;
		L_162 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		int32_t L_163 = V_4;
		NullCheck(L_162);
		int32_t L_164 = L_163;
		MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_165 = (L_162)->GetAt(static_cast<il2cpp_array_size_t>(L_164));
		NullCheck(L_161);
		ArticulatedHandDefinition_UpdateCurrentTeleportPose_m2DC840B71B67204E4D7A1221AD489FF55643462A(L_161, L_165, /*hidden argument*/NULL);
	}

IL_0346:
	{
		// for (int i = 0; i < Interactions?.Length; i++)
		int32_t L_166 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_166, (int32_t)1));
	}

IL_034c:
	{
		// for (int i = 0; i < Interactions?.Length; i++)
		int32_t L_167 = V_4;
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_168;
		L_168 = BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline(__this, /*hidden argument*/NULL);
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_169 = L_168;
		G_B51_0 = L_169;
		G_B51_1 = L_167;
		if (L_169)
		{
			G_B52_0 = L_169;
			G_B52_1 = L_167;
			goto IL_0364;
		}
	}
	{
		il2cpp_codegen_initobj((&V_7), sizeof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 ));
		Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  L_170 = V_7;
		G_B53_0 = L_170;
		G_B53_1 = G_B51_1;
		goto IL_036b;
	}

IL_0364:
	{
		NullCheck(G_B52_0);
		Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  L_171;
		memset((&L_171), 0, sizeof(L_171));
		Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184((&L_171), ((int32_t)((int32_t)(((RuntimeArray*)G_B52_0)->max_length))), /*hidden argument*/Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184_RuntimeMethod_var);
		G_B53_0 = L_171;
		G_B53_1 = G_B52_1;
	}

IL_036b:
	{
		V_6 = G_B53_0;
		int32_t L_172;
		L_172 = Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_inline((Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 *)(&V_6), /*hidden argument*/Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_RuntimeMethod_var);
		bool L_173;
		L_173 = Nullable_1_get_HasValue_m7455E879CFAAE682AE3786D4D2B1F65C8AA23921_inline((Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 *)(&V_6), /*hidden argument*/Nullable_1_get_HasValue_m7455E879CFAAE682AE3786D4D2B1F65C8AA23921_RuntimeMethod_var);
		if (((int32_t)((int32_t)((((int32_t)G_B53_1) < ((int32_t)L_172))? 1 : 0)&(int32_t)L_173)))
		{
			goto IL_00c9;
		}
	}
	{
		// }
		return;
	}
}
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::UpdateHandData(OVRHand,OVRSkeleton)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OculusHand_UpdateHandData_m1A965734213BE9AB52AE8B439AF9FD9C58222105 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * ___ovrHand0, OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * ___ovrSkeleton1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_1_t897D4F5EF6A9A5E4C3A8253F1BCE615BD0BBC9C1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t3933D6BE2BD9674342F4135CE97CF489D5D1A47D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	RuntimeObject* V_3 = NULL;
	OVRBone_tF9C031559703607035E455CE642549C722BBF081 * V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * G_B23_0 = NULL;
	ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * G_B22_0 = NULL;
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * G_B30_0 = NULL;
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * G_B29_0 = NULL;
	int32_t G_B31_0 = 0;
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * G_B31_1 = NULL;
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * G_B35_0 = NULL;
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * G_B34_0 = NULL;
	int32_t G_B36_0 = 0;
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * G_B36_1 = NULL;
	{
		// bool isTracked = ovrHand.IsTracked;
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_0 = ___ovrHand0;
		NullCheck(L_0);
		bool L_1;
		L_1 = OVRHand_get_IsTracked_m1B1B95C54623252AD41089F461ACC3669CE2AE7C_inline(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// if (ovrHand.HandConfidence == OVRHand.TrackingConfidence.High)
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_2 = ___ovrHand0;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = OVRHand_get_HandConfidence_mE06ACA8BA1C4D00BA5F196AC3CA3DCD46C9801FE_inline(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)1065353216)))))
		{
			goto IL_001f;
		}
	}
	{
		// _lastHighConfidenceTime = Time.unscaledTime;
		float L_4;
		L_4 = Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258(/*hidden argument*/NULL);
		__this->set__lastHighConfidenceTime_31(L_4);
	}

IL_001f:
	{
		// if (ovrHand.HandConfidence == OVRHand.TrackingConfidence.Low)
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_5 = ___ovrHand0;
		NullCheck(L_5);
		int32_t L_6;
		L_6 = OVRHand_get_HandConfidence_mE06ACA8BA1C4D00BA5F196AC3CA3DCD46C9801FE_inline(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_006c;
		}
	}
	{
		// if (settingsProfile.MinimumHandConfidence == OVRHand.TrackingConfidence.High)
		OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * L_7 = __this->get_settingsProfile_24();
		NullCheck(L_7);
		int32_t L_8;
		L_8 = OculusXRSDKDeviceManagerProfile_get_MinimumHandConfidence_m219D821F8CEA93A8D563B5F88F46C5E3A2BFD20D_inline(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)1065353216)))))
		{
			goto IL_003d;
		}
	}
	{
		// isTracked = false;
		V_0 = (bool)0;
		// }
		goto IL_006c;
	}

IL_003d:
	{
		// float lowConfidenceTime = Time.time - _lastHighConfidenceTime;
		float L_9;
		L_9 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_10 = __this->get__lastHighConfidenceTime_31();
		V_2 = ((float)il2cpp_codegen_subtract((float)L_9, (float)L_10));
		// if (settingsProfile.LowConfidenceTimeThreshold > 0 &&
		//     settingsProfile.LowConfidenceTimeThreshold < lowConfidenceTime)
		OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * L_11 = __this->get_settingsProfile_24();
		NullCheck(L_11);
		float L_12;
		L_12 = OculusXRSDKDeviceManagerProfile_get_LowConfidenceTimeThreshold_mA6197D5CFC9C78F61EB6B040A43E17277A26A743_inline(L_11, /*hidden argument*/NULL);
		if ((!(((float)L_12) > ((float)(0.0f)))))
		{
			goto IL_006c;
		}
	}
	{
		OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * L_13 = __this->get_settingsProfile_24();
		NullCheck(L_13);
		float L_14;
		L_14 = OculusXRSDKDeviceManagerProfile_get_LowConfidenceTimeThreshold_mA6197D5CFC9C78F61EB6B040A43E17277A26A743_inline(L_13, /*hidden argument*/NULL);
		float L_15 = V_2;
		if ((!(((float)L_14) < ((float)L_15))))
		{
			goto IL_006c;
		}
	}
	{
		// isTracked = false;
		V_0 = (bool)0;
	}

IL_006c:
	{
		// if (ControllerHandedness == Handedness.Left)
		uint8_t L_16;
		L_16 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)1))))
		{
			goto IL_0088;
		}
	}
	{
		// settingsProfile.CurrentLeftHandTrackingConfidence = ovrHand.HandConfidence;
		OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * L_17 = __this->get_settingsProfile_24();
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_18 = ___ovrHand0;
		NullCheck(L_18);
		int32_t L_19;
		L_19 = OVRHand_get_HandConfidence_mE06ACA8BA1C4D00BA5F196AC3CA3DCD46C9801FE_inline(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		OculusXRSDKDeviceManagerProfile_set_CurrentLeftHandTrackingConfidence_m36B081D88A939B6F1D83987B46ACFD4C5AE1F10D_inline(L_17, L_19, /*hidden argument*/NULL);
		// }
		goto IL_0099;
	}

IL_0088:
	{
		// settingsProfile.CurrentRightHandTrackingConfidence = ovrHand.HandConfidence;
		OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * L_20 = __this->get_settingsProfile_24();
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_21 = ___ovrHand0;
		NullCheck(L_21);
		int32_t L_22;
		L_22 = OVRHand_get_HandConfidence_mE06ACA8BA1C4D00BA5F196AC3CA3DCD46C9801FE_inline(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		OculusXRSDKDeviceManagerProfile_set_CurrentRightHandTrackingConfidence_mDEE11B9CDA4CE2DD9C1262D2DCBC72CF04CAD57B_inline(L_20, L_22, /*hidden argument*/NULL);
	}

IL_0099:
	{
		// if (ovrSkeleton != null)
		OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * L_23 = ___ovrSkeleton1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_24;
		L_24 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_23, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00da;
		}
	}
	{
		// var bones = ovrSkeleton.Bones;
		OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * L_25 = ___ovrSkeleton1;
		NullCheck(L_25);
		RuntimeObject* L_26;
		L_26 = OVRSkeleton_get_Bones_mC8D26C6EF0253FBC0EF8B350EEBBE84C97045C0F_inline(L_25, /*hidden argument*/NULL);
		// foreach (var bone in bones)
		NullCheck(L_26);
		RuntimeObject* L_27;
		L_27 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<OVRBone>::GetEnumerator() */, IEnumerable_1_t897D4F5EF6A9A5E4C3A8253F1BCE615BD0BBC9C1_il2cpp_TypeInfo_var, L_26);
		V_3 = L_27;
	}

IL_00ae:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c0;
		}

IL_00b0:
		{
			// foreach (var bone in bones)
			RuntimeObject* L_28 = V_3;
			NullCheck(L_28);
			OVRBone_tF9C031559703607035E455CE642549C722BBF081 * L_29;
			L_29 = InterfaceFuncInvoker0< OVRBone_tF9C031559703607035E455CE642549C722BBF081 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<OVRBone>::get_Current() */, IEnumerator_1_t3933D6BE2BD9674342F4135CE97CF489D5D1A47D_il2cpp_TypeInfo_var, L_28);
			V_4 = L_29;
			// UpdateBone(bone);
			OVRBone_tF9C031559703607035E455CE642549C722BBF081 * L_30 = V_4;
			OculusHand_UpdateBone_mC448C0E93B90FB31A0C34C5B9E3BEB0A758E8E62(__this, L_30, /*hidden argument*/NULL);
		}

IL_00c0:
		{
			// foreach (var bone in bones)
			RuntimeObject* L_31 = V_3;
			NullCheck(L_31);
			bool L_32;
			L_32 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_31);
			if (L_32)
			{
				goto IL_00b0;
			}
		}

IL_00c8:
		{
			IL2CPP_LEAVE(0xD4, FINALLY_00ca);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00ca;
	}

FINALLY_00ca:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_33 = V_3;
			if (!L_33)
			{
				goto IL_00d3;
			}
		}

IL_00cd:
		{
			RuntimeObject* L_34 = V_3;
			NullCheck(L_34);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_34);
		}

IL_00d3:
		{
			IL2CPP_END_FINALLY(202)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(202)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xD4, IL_00d4)
	}

IL_00d4:
	{
		// UpdatePalm();
		OculusHand_UpdatePalm_m8584D7681FB7E16CCEFB42F5EC5E972320397301(__this, /*hidden argument*/NULL);
	}

IL_00da:
	{
		// HandDefinition?.UpdateHandJoints(jointPoses);
		ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * L_35;
		L_35 = OculusHand_get_HandDefinition_mF576EE597AFC69C901F66DA59822582FFAAC1958(__this, /*hidden argument*/NULL);
		ArticulatedHandDefinition_tB00E59E5D6C03BB299623466CF960FFBD2770BC5 * L_36 = L_35;
		G_B22_0 = L_36;
		if (L_36)
		{
			G_B23_0 = L_36;
			goto IL_00e6;
		}
	}
	{
		goto IL_00f1;
	}

IL_00e6:
	{
		Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * L_37 = __this->get_jointPoses_26();
		NullCheck(G_B23_0);
		ArticulatedHandDefinition_UpdateHandJoints_m23EE90FF8F0A03C3FF55929B3E1418AF1468670B(G_B23_0, L_37, /*hidden argument*/NULL);
	}

IL_00f1:
	{
		// float pinchStrength = HandPoseUtils.CalculateIndexPinch(ControllerHandedness);
		uint8_t L_38;
		L_38 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
		float L_39;
		L_39 = HandPoseUtils_CalculateIndexPinch_mD8E7EDEBE7490F26B53A4C7A9DA64D43579BCD45(L_38, /*hidden argument*/NULL);
		V_1 = L_39;
		// if (pinchStrength == 0.0f)
		float L_40 = V_1;
		if ((!(((float)L_40) == ((float)(0.0f)))))
		{
			goto IL_010e;
		}
	}
	{
		// IsPinching = false;
		OculusHand_set_IsPinching_mAAEFC3C08B379B80AF3411AC98531581C90B1B4A_inline(__this, (bool)0, /*hidden argument*/NULL);
		// }
		goto IL_0145;
	}

IL_010e:
	{
		// if (IsPinching)
		bool L_41;
		L_41 = OculusHand_get_IsPinching_m1DEFD901E5E5FE10261629E06E5941C2DEF0F707_inline(__this, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0126;
		}
	}
	{
		// IsPinching = pinchStrength > 0.85f;
		float L_42 = V_1;
		OculusHand_set_IsPinching_mAAEFC3C08B379B80AF3411AC98531581C90B1B4A_inline(__this, (bool)((((float)L_42) > ((float)(0.850000024f)))? 1 : 0), /*hidden argument*/NULL);
		// }
		goto IL_0145;
	}

IL_0126:
	{
		// IsPinching = pinchStrength > 0.5f && ovrHand.GetFingerConfidence(OVRHand.HandFinger.Index) == OVRHand.TrackingConfidence.High;
		float L_43 = V_1;
		G_B29_0 = __this;
		if ((!(((float)L_43) > ((float)(0.5f)))))
		{
			G_B30_0 = __this;
			goto IL_013f;
		}
	}
	{
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_44 = ___ovrHand0;
		NullCheck(L_44);
		int32_t L_45;
		L_45 = OVRHand_GetFingerConfidence_m50B0F85F1FF69DF3DFF61CD1EC846D95213E4C96(L_44, 1, /*hidden argument*/NULL);
		G_B31_0 = ((((int32_t)L_45) == ((int32_t)((int32_t)1065353216)))? 1 : 0);
		G_B31_1 = G_B29_0;
		goto IL_0140;
	}

IL_013f:
	{
		G_B31_0 = 0;
		G_B31_1 = G_B30_0;
	}

IL_0140:
	{
		NullCheck(G_B31_1);
		OculusHand_set_IsPinching_mAAEFC3C08B379B80AF3411AC98531581C90B1B4A_inline(G_B31_1, (bool)G_B31_0, /*hidden argument*/NULL);
	}

IL_0145:
	{
		// isIndexGrabbing = HandPoseUtils.IsIndexGrabbing(ControllerHandedness);
		uint8_t L_46;
		L_46 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
		bool L_47;
		L_47 = HandPoseUtils_IsIndexGrabbing_m56746CC4DA7560A5E4DDDF47D412AC070FDB783E(L_46, /*hidden argument*/NULL);
		__this->set_isIndexGrabbing_22(L_47);
		// isMiddleGrabbing = HandPoseUtils.IsMiddleGrabbing(ControllerHandedness);
		uint8_t L_48;
		L_48 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
		bool L_49;
		L_49 = HandPoseUtils_IsMiddleGrabbing_mB2B428983A4A54AEE69434B739327C31F4092479(L_48, /*hidden argument*/NULL);
		__this->set_isMiddleGrabbing_23(L_49);
		// if (isTracked)
		bool L_50 = V_0;
		if (!L_50)
		{
			goto IL_0181;
		}
	}
	{
		// IsGrabbing = isIndexGrabbing && isMiddleGrabbing;
		bool L_51 = __this->get_isIndexGrabbing_22();
		G_B34_0 = __this;
		if (!L_51)
		{
			G_B35_0 = __this;
			goto IL_017b;
		}
	}
	{
		bool L_52 = __this->get_isMiddleGrabbing_23();
		G_B36_0 = ((int32_t)(L_52));
		G_B36_1 = G_B34_0;
		goto IL_017c;
	}

IL_017b:
	{
		G_B36_0 = 0;
		G_B36_1 = G_B35_0;
	}

IL_017c:
	{
		NullCheck(G_B36_1);
		OculusHand_set_IsGrabbing_mD4B8A213D51B07938ED3B39C75D37BC0BF5C6872_inline(G_B36_1, (bool)G_B36_0, /*hidden argument*/NULL);
	}

IL_0181:
	{
		// return isTracked;
		bool L_53 = V_0;
		return L_53;
	}
}
// System.Single Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::IndexThumbSqrMagnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float OculusHand_IndexThumbSqrMagnitude_mADA1BD0B8AB70668EE5C38D7B36F2BA0B352A7EF (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  V_0;
	memset((&V_0), 0, sizeof(V_0));
	MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// MixedRealityPose indexPose = MixedRealityPose.ZeroIdentity;
		IL2CPP_RUNTIME_CLASS_INIT(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669_il2cpp_TypeInfo_var);
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_0;
		L_0 = MixedRealityPose_get_ZeroIdentity_m0668ACB9F881F2046B81685B02D13BFD78338998_inline(/*hidden argument*/NULL);
		V_0 = L_0;
		// TryGetJoint(TrackedHandJoint.IndexTip, out indexPose);
		bool L_1;
		L_1 = VirtFuncInvoker2< bool, int32_t, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * >::Invoke(27 /* System.Boolean Microsoft.MixedReality.Toolkit.Input.BaseHand::TryGetJoint(Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose&) */, __this, ((int32_t)11), (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)(&V_0));
		// MixedRealityPose thumbPose = MixedRealityPose.ZeroIdentity;
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_2;
		L_2 = MixedRealityPose_get_ZeroIdentity_m0668ACB9F881F2046B81685B02D13BFD78338998_inline(/*hidden argument*/NULL);
		V_1 = L_2;
		// TryGetJoint(TrackedHandJoint.ThumbTip, out thumbPose);
		bool L_3;
		L_3 = VirtFuncInvoker2< bool, int32_t, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * >::Invoke(27 /* System.Boolean Microsoft.MixedReality.Toolkit.Input.BaseHand::TryGetJoint(Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose&) */, __this, 6, (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)(&V_1));
		// Vector3 distanceVector = indexPose.Position - thumbPose.Position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = MixedRealityPose_get_Position_m34E0E9859576E00CEB1CC3D1D31F18C45B1712A3_inline((MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)(&V_0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = MixedRealityPose_get_Position_m34E0E9859576E00CEB1CC3D1D31F18C45B1712A3_inline((MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)(&V_1), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		// return distanceVector.sqrMagnitude;
		float L_7;
		L_7 = Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_2), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::UpdateBone(OVRBone)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_UpdateBone_mC448C0E93B90FB31A0C34C5B9E3BEB0A758E8E62 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, OVRBone_tF9C031559703607035E455CE642549C722BBF081 * ___bone0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m6CC999BE14BA489631964C386680A89AEEB456DA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_1 = NULL;
	int32_t V_2 = 0;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		// var boneId = bone.Id;
		OVRBone_tF9C031559703607035E455CE642549C722BBF081 * L_0 = ___bone0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = OVRBone_get_Id_m60B032C7EB5D597C9F5D3D7911A241F31EE8E12E_inline(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// var boneTransform = bone.Transform;
		OVRBone_tF9C031559703607035E455CE642549C722BBF081 * L_2 = ___bone0;
		NullCheck(L_2);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = OVRBone_get_Transform_m0E8C60FF2CF31336D654B3319EFFB04BDC964FCB_inline(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// if (boneJointMapping.TryGetValue(boneId, out var joint))
		Dictionary_2_t63CDC98E995E005A8B96DCC249E4BD40CAEDA20B * L_4 = __this->get_boneJointMapping_30();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		bool L_6;
		L_6 = Dictionary_2_TryGetValue_m6CC999BE14BA489631964C386680A89AEEB456DA(L_4, L_5, (int32_t*)(&V_2), /*hidden argument*/Dictionary_2_TryGetValue_m6CC999BE14BA489631964C386680A89AEEB456DA_RuntimeMethod_var);
		if (!L_6)
		{
			goto IL_0094;
		}
	}
	{
		// Quaternion boneRotation = bone.Transform.rotation;
		OVRBone_tF9C031559703607035E455CE642549C722BBF081 * L_7 = ___bone0;
		NullCheck(L_7);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = OVRBone_get_Transform_m0E8C60FF2CF31336D654B3319EFFB04BDC964FCB_inline(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_9;
		L_9 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		// if (ControllerHandedness == Handedness.Left)
		uint8_t L_10;
		L_10 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		// boneRotation *= Quaternion.Euler(180f, 0f, 0f);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_11 = V_3;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_12;
		L_12 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3((180.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_13;
		L_13 = Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0(L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		// boneRotation *= Quaternion.Euler(0f, -90, 0f);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_14 = V_3;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_15;
		L_15 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3((0.0f), (-90.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_16;
		L_16 = Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0(L_14, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		// }
		goto IL_0086;
	}

IL_006b:
	{
		// boneRotation *= Quaternion.Euler(0f, 90f, 0f);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_17 = V_3;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_18;
		L_18 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3((0.0f), (90.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_19;
		L_19 = Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0(L_17, L_18, /*hidden argument*/NULL);
		V_3 = L_19;
	}

IL_0086:
	{
		// UpdateJointPose(joint, boneTransform.position, boneRotation);
		int32_t L_20 = V_2;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_21 = V_1;
		NullCheck(L_21);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_21, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_23 = V_3;
		OculusHand_UpdateJointPose_mF3900F3A833A0A62400A8EEF683E6C764F47FBD8(__this, L_20, L_22, L_23, /*hidden argument*/NULL);
	}

IL_0094:
	{
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::UpdatePalm()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_UpdatePalm_m8584D7681FB7E16CCEFB42F5EC5E972320397301 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method)
{
	MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_4;
	memset((&V_4), 0, sizeof(V_4));
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_5;
	memset((&V_5), 0, sizeof(V_5));
	{
		// bool hasMiddleKnuckle = TryGetJoint(TrackedHandJoint.MiddleKnuckle, out var middleKnucklePose);
		bool L_0;
		L_0 = VirtFuncInvoker2< bool, int32_t, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * >::Invoke(27 /* System.Boolean Microsoft.MixedReality.Toolkit.Input.BaseHand::TryGetJoint(Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose&) */, __this, ((int32_t)13), (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)(&V_0));
		// bool hasWrist = TryGetJoint(TrackedHandJoint.Wrist, out var wristPose);
		bool L_1;
		L_1 = VirtFuncInvoker2< bool, int32_t, MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * >::Invoke(27 /* System.Boolean Microsoft.MixedReality.Toolkit.Input.BaseHand::TryGetJoint(Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose&) */, __this, 1, (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)(&V_2));
		V_1 = L_1;
		// if (hasMiddleKnuckle && hasWrist)
		bool L_2 = V_1;
		if (!((int32_t)((int32_t)L_0&(int32_t)L_2)))
		{
			goto IL_0048;
		}
	}
	{
		// Vector3 wristRootPosition = wristPose.Position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = MixedRealityPose_get_Position_m34E0E9859576E00CEB1CC3D1D31F18C45B1712A3_inline((MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)(&V_2), /*hidden argument*/NULL);
		// Vector3 middle3Position = middleKnucklePose.Position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = MixedRealityPose_get_Position_m34E0E9859576E00CEB1CC3D1D31F18C45B1712A3_inline((MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)(&V_0), /*hidden argument*/NULL);
		V_3 = L_4;
		// Vector3 palmPosition = Vector3.Lerp(wristRootPosition, middle3Position, 0.5f);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_3, L_5, (0.5f), /*hidden argument*/NULL);
		V_4 = L_6;
		// Quaternion palmRotation = wristPose.Rotation;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_7;
		L_7 = MixedRealityPose_get_Rotation_m884C69734F7CD693912ECF2ACC36904CB18BE907_inline((MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)(&V_2), /*hidden argument*/NULL);
		V_5 = L_7;
		// UpdateJointPose(TrackedHandJoint.Palm, palmPosition, palmRotation);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = V_4;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_9 = V_5;
		OculusHand_UpdateJointPose_mF3900F3A833A0A62400A8EEF683E6C764F47FBD8(__this, 2, L_8, L_9, /*hidden argument*/NULL);
	}

IL_0048:
	{
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand::UpdateJointPose(Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusHand_UpdateJointPose_mF3900F3A833A0A62400A8EEF683E6C764F47FBD8 (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, int32_t ___joint0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mD183155545F86332FE7DF071F13B274F6D514344_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m4DC00BAE7DFA4CF7ECDD2E576E31E5FA31F36DE7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m899DD9BD808EDEA1D10982AD411594DEC9DE218A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Vector3 jointPosition = position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___position1;
		V_0 = L_0;
		// MixedRealityPose pose = new MixedRealityPose(jointPosition, rotation);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = V_0;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_2 = ___rotation2;
		MixedRealityPose__ctor_m8B1A703ACB7F2BA742D3960612891435AD5D4D28((MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 *)(&V_1), L_1, L_2, /*hidden argument*/NULL);
		// if (!jointPoses.ContainsKey(joint))
		Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * L_3 = __this->get_jointPoses_26();
		int32_t L_4 = ___joint0;
		NullCheck(L_3);
		bool L_5;
		L_5 = Dictionary_2_ContainsKey_m4DC00BAE7DFA4CF7ECDD2E576E31E5FA31F36DE7(L_3, L_4, /*hidden argument*/Dictionary_2_ContainsKey_m4DC00BAE7DFA4CF7ECDD2E576E31E5FA31F36DE7_RuntimeMethod_var);
		if (L_5)
		{
			goto IL_0027;
		}
	}
	{
		// jointPoses.Add(joint, pose);
		Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * L_6 = __this->get_jointPoses_26();
		int32_t L_7 = ___joint0;
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_8 = V_1;
		NullCheck(L_6);
		Dictionary_2_Add_mD183155545F86332FE7DF071F13B274F6D514344(L_6, L_7, L_8, /*hidden argument*/Dictionary_2_Add_mD183155545F86332FE7DF071F13B274F6D514344_RuntimeMethod_var);
		// }
		return;
	}

IL_0027:
	{
		// jointPoses[joint] = pose;
		Dictionary_2_t220D987859A5CBC4A4A36F8BF07927E3372E43A9 * L_9 = __this->get_jointPoses_26();
		int32_t L_10 = ___joint0;
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_11 = V_1;
		NullCheck(L_9);
		Dictionary_2_set_Item_m899DD9BD808EDEA1D10982AD411594DEC9DE218A(L_9, L_10, L_11, /*hidden argument*/Dictionary_2_set_Item_m899DD9BD808EDEA1D10982AD411594DEC9DE218A_RuntimeMethod_var);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager__ctor_mB07E4E7FF0A24C2EA6C3B60AAB05E929E39A5374 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, RuntimeObject* ___inputSystem0, String_t* ___name1, uint32_t ___priority2, BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3 * ___profile3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mB3D1F54DCBB3BAAA711763CA6512DEE2A954228C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private readonly Dictionary<Handedness, OculusHand> trackedHands = new Dictionary<Handedness, OculusHand>();
		Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * L_0 = (Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 *)il2cpp_codegen_object_new(Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mB3D1F54DCBB3BAAA711763CA6512DEE2A954228C(L_0, /*hidden argument*/Dictionary_2__ctor_mB3D1F54DCBB3BAAA711763CA6512DEE2A954228C_RuntimeMethod_var);
		__this->set_trackedHands_25(L_0);
		// BaseMixedRealityProfile profile = null) : base(inputSystem, name, priority, profile) { }
		RuntimeObject* L_1 = ___inputSystem0;
		String_t* L_2 = ___name1;
		uint32_t L_3 = ___priority2;
		BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3 * L_4 = ___profile3;
		IL2CPP_RUNTIME_CLASS_INIT(XRSDKDeviceManager_t0D558B1155DA9B7FBF7BAA28785F000454EDBAA7_il2cpp_TypeInfo_var);
		XRSDKDeviceManager__ctor_mABAC44F39E97E651B7EDE61E11058320828012A7(__this, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		// BaseMixedRealityProfile profile = null) : base(inputSystem, name, priority, profile) { }
		return;
	}
}
// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::get_SettingsProfile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * OculusXRSDKDeviceManager_get_SettingsProfile_m48D2E91D64F9DB2A7D4C48E65E0C48D426111007 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private OculusXRSDKDeviceManagerProfile SettingsProfile => ConfigurationProfile as OculusXRSDKDeviceManagerProfile;
		BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3 * L_0;
		L_0 = VirtFuncInvoker0< BaseMixedRealityProfile_tE425D8D64B2631D5C8C82A49805613264FA9D9A3 * >::Invoke(22 /* Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile Microsoft.MixedReality.Toolkit.BaseService::get_ConfigurationProfile() */, __this);
		return ((OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 *)IsInstClass((RuntimeObject*)L_0, OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7_il2cpp_TypeInfo_var));
	}
}
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::CheckCapability(Microsoft.MixedReality.Toolkit.MixedRealityCapability)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OculusXRSDKDeviceManager_CheckCapability_m03FDDB9BC771EF36ADEBEA61DA96F281A6796C1E (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, int32_t ___capability0, const RuntimeMethod* method)
{
	{
		// if (capability == MixedRealityCapability.ArticulatedHand)
		int32_t L_0 = ___capability0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0005:
	{
		// return capability == MixedRealityCapability.MotionController;
		int32_t L_1 = ___capability0;
		return (bool)((((int32_t)L_1) == ((int32_t)2))? 1 : 0);
	}
}
// System.Type Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::GetControllerType(Microsoft.MixedReality.Toolkit.Input.SupportedControllerType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * OculusXRSDKDeviceManager_GetControllerType_m4DF6F9A7B73C0FABC597786B2C832458A09939B7 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, int32_t ___supportedControllerType0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___supportedControllerType0;
		if ((((int32_t)L_0) == ((int32_t)8)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = ___supportedControllerType0;
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)1024)))))
		{
			goto IL_0022;
		}
	}
	{
		// return typeof(OculusHand);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_2 = { reinterpret_cast<intptr_t> (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3;
		L_3 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0017:
	{
		// return typeof(OculusXRSDKTouchController);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_4 = { reinterpret_cast<intptr_t> (OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5;
		L_5 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0022:
	{
		// return base.GetControllerType(supportedControllerType);
		int32_t L_6 = ___supportedControllerType0;
		Type_t * L_7;
		L_7 = XRSDKDeviceManager_GetControllerType_mA71B0AFA13ED0D198F6B878E3773BF8734F21671(__this, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// Microsoft.MixedReality.Toolkit.Input.InputSourceType Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::GetInputSourceType(Microsoft.MixedReality.Toolkit.Input.SupportedControllerType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManager_GetInputSourceType_m6FCF06B1A75842DB0EF95CBE681DC51FE16F59B7 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, int32_t ___supportedControllerType0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___supportedControllerType0;
		if ((((int32_t)L_0) == ((int32_t)8)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___supportedControllerType0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)1024))))
		{
			goto IL_0010;
		}
	}
	{
		goto IL_0012;
	}

IL_000e:
	{
		// return InputSourceType.Controller;
		return (int32_t)(2);
	}

IL_0010:
	{
		// return InputSourceType.Hand;
		return (int32_t)(1);
	}

IL_0012:
	{
		// return base.GetInputSourceType(supportedControllerType);
		int32_t L_2 = ___supportedControllerType0;
		int32_t L_3;
		L_3 = XRSDKDeviceManager_GetInputSourceType_m2BD4DEA82E00D7CFEF0954425C67B6232E04D6E1(__this, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// Microsoft.MixedReality.Toolkit.Input.SupportedControllerType Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::GetCurrentControllerType(UnityEngine.XR.InputDevice)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManager_GetCurrentControllerType_m104379C4D1027E57DE8CD73C69475F433CAB1B2B (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  ___inputDevice0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (inputDevice.characteristics.HasFlag(InputDeviceCharacteristics.HandTracking))
		uint32_t L_0;
		L_0 = InputDevice_get_characteristics_mF7A1F32CD82CA6EE97AD113089F063109322C63A((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&___inputDevice0), /*hidden argument*/NULL);
		uint32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64_il2cpp_TypeInfo_var, &L_1);
		uint32_t L_3 = 8;
		RuntimeObject * L_4 = Box(InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)L_2);
		bool L_5;
		L_5 = Enum_HasFlag_mF1BCDA3744B0CC82C7A3D7313C0858E82010151E((Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)L_2, (Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0059;
		}
	}
	{
		// if (inputDevice.characteristics.HasFlag(InputDeviceCharacteristics.Left) ||
		//     inputDevice.characteristics.HasFlag(InputDeviceCharacteristics.Right))
		uint32_t L_6;
		L_6 = InputDevice_get_characteristics_mF7A1F32CD82CA6EE97AD113089F063109322C63A((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&___inputDevice0), /*hidden argument*/NULL);
		uint32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64_il2cpp_TypeInfo_var, &L_7);
		uint32_t L_9 = ((int32_t)256);
		RuntimeObject * L_10 = Box(InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64_il2cpp_TypeInfo_var, &L_9);
		NullCheck((Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)L_8);
		bool L_11;
		L_11 = Enum_HasFlag_mF1BCDA3744B0CC82C7A3D7313C0858E82010151E((Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)L_8, (Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0053;
		}
	}
	{
		uint32_t L_12;
		L_12 = InputDevice_get_characteristics_mF7A1F32CD82CA6EE97AD113089F063109322C63A((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&___inputDevice0), /*hidden argument*/NULL);
		uint32_t L_13 = L_12;
		RuntimeObject * L_14 = Box(InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64_il2cpp_TypeInfo_var, &L_13);
		uint32_t L_15 = ((int32_t)512);
		RuntimeObject * L_16 = Box(InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64_il2cpp_TypeInfo_var, &L_15);
		NullCheck((Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)L_14);
		bool L_17;
		L_17 = Enum_HasFlag_mF1BCDA3744B0CC82C7A3D7313C0858E82010151E((Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)L_14, (Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0059;
		}
	}

IL_0053:
	{
		// return SupportedControllerType.ArticulatedHand;
		return (int32_t)(((int32_t)1024));
	}

IL_0059:
	{
		// if (inputDevice.characteristics.HasFlag(InputDeviceCharacteristics.Controller))
		uint32_t L_18;
		L_18 = InputDevice_get_characteristics_mF7A1F32CD82CA6EE97AD113089F063109322C63A((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&___inputDevice0), /*hidden argument*/NULL);
		uint32_t L_19 = L_18;
		RuntimeObject * L_20 = Box(InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64_il2cpp_TypeInfo_var, &L_19);
		uint32_t L_21 = ((int32_t)64);
		RuntimeObject * L_22 = Box(InputDeviceCharacteristics_t0C34BAC0C6F661161E2DA1677CD590273F1C9C64_il2cpp_TypeInfo_var, &L_21);
		NullCheck((Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)L_20);
		bool L_23;
		L_23 = Enum_HasFlag_mF1BCDA3744B0CC82C7A3D7313C0858E82010151E((Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)L_20, (Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0075;
		}
	}
	{
		// return SupportedControllerType.OculusTouch;
		return (int32_t)(8);
	}

IL_0075:
	{
		// return base.GetCurrentControllerType(inputDevice);
		InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  L_24 = ___inputDevice0;
		int32_t L_25;
		L_25 = XRSDKDeviceManager_GetCurrentControllerType_m28138A9FA1C6A4ADBBA48FFFEF22BAD47AA0FB19(__this, L_24, /*hidden argument*/NULL);
		return L_25;
	}
}
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::get_IsActiveLoader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OculusXRSDKDeviceManager_get_IsActiveLoader_m3AA564057D5F1D64A3E5F98D8F4191BF726E37E8 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_GetValueOrDefault_mBECC58FEFD1088EC070D9F9A892ECD1D8BBF2A0F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1__ctor_m402A94AC9070B345C95919DCFFFF627252B3E67A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_get_HasValue_m52F33C6963C9A76A14F54B4B2C30CEF580DB1EB7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&XRSDKDeviceManager_IsLoaderActive_TisOculusLoader_tD15B17A95554127ED764AFC39FE3D0E720C8BFEB_m7749693AFBB25485FF31EB6A35889D936236F91A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!isActiveLoader.HasValue)
		Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * L_0 = __this->get_address_of_isActiveLoader_31();
		bool L_1;
		L_1 = Nullable_1_get_HasValue_m52F33C6963C9A76A14F54B4B2C30CEF580DB1EB7_inline((Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 *)L_0, /*hidden argument*/Nullable_1_get_HasValue_m52F33C6963C9A76A14F54B4B2C30CEF580DB1EB7_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		// isActiveLoader = IsLoaderActive<OculusLoader>();
		bool L_2;
		L_2 = GenericVirtFuncInvoker0< bool >::Invoke(XRSDKDeviceManager_IsLoaderActive_TisOculusLoader_tD15B17A95554127ED764AFC39FE3D0E720C8BFEB_m7749693AFBB25485FF31EB6A35889D936236F91A_RuntimeMethod_var, __this);
		Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Nullable_1__ctor_m402A94AC9070B345C95919DCFFFF627252B3E67A((&L_3), L_2, /*hidden argument*/Nullable_1__ctor_m402A94AC9070B345C95919DCFFFF627252B3E67A_RuntimeMethod_var);
		__this->set_isActiveLoader_31(L_3);
	}

IL_001e:
	{
		// return isActiveLoader ?? false;
		Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * L_4 = __this->get_address_of_isActiveLoader_31();
		bool L_5;
		L_5 = Nullable_1_GetValueOrDefault_mBECC58FEFD1088EC070D9F9A892ECD1D8BBF2A0F_inline((Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 *)L_4, /*hidden argument*/Nullable_1_GetValueOrDefault_mBECC58FEFD1088EC070D9F9A892ECD1D8BBF2A0F_RuntimeMethod_var);
		return L_5;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::Enable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_Enable_mC962113BD1C1D674ACB216DE9564F4D3F6E08ABD (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method)
{
	{
		// if (!IsActiveLoader)
		bool L_0;
		L_0 = OculusXRSDKDeviceManager_get_IsActiveLoader_m3AA564057D5F1D64A3E5F98D8F4191BF726E37E8(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		// IsEnabled = false;
		VirtActionInvoker1< bool >::Invoke(34 /* System.Void Microsoft.MixedReality.Toolkit.BaseService::set_IsEnabled(System.Boolean) */, __this, (bool)0);
		// return;
		return;
	}

IL_0010:
	{
		// base.Enable();
		BaseService_Enable_m60768260471BD4A6E1EF8D6A9CD661CB90E445B3(__this, /*hidden argument*/NULL);
		// SetupInput();
		OculusXRSDKDeviceManager_SetupInput_m1E47F82D6614002FF0D875E7A6BEEB69C8AAB03D(__this, /*hidden argument*/NULL);
		// ConfigurePerformancePreferences();
		OculusXRSDKDeviceManager_ConfigurePerformancePreferences_m977EEF04FE8EBB33DD433802997E9796CAD201B0(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_Update_m21CAFE30A8F39609E1A2115970F9C2B1AF0EE081 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OVRPlugin_t33529050E1318B8BC645C7451E1A3C8C4E500DCF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!IsEnabled)
		bool L_0;
		L_0 = VirtFuncInvoker0< bool >::Invoke(33 /* System.Boolean Microsoft.MixedReality.Toolkit.BaseService::get_IsEnabled() */, __this);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// base.Update();
		XRSDKDeviceManager_Update_m7DD58B2925107ADF7A0903F107C63D0092DE9C02(__this, /*hidden argument*/NULL);
		// if (OVRPlugin.GetHandTrackingEnabled())
		IL2CPP_RUNTIME_CLASS_INIT(OVRPlugin_t33529050E1318B8BC645C7451E1A3C8C4E500DCF_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = OVRPlugin_GetHandTrackingEnabled_m36DCF981657D16F798B7D880C6480FD0D1429FFF(/*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		// UpdateHands();
		OculusXRSDKDeviceManager_UpdateHands_m957A3B4619FDE131C5537B4F19537D802CB27989(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_001d:
	{
		// RemoveAllHandDevices();
		OculusXRSDKDeviceManager_RemoveAllHandDevices_m6BF093E29DECA10A3CF59199E2CAB0031992AE55(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::SetupInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_SetupInput_m1E47F82D6614002FF0D875E7A6BEEB69C8AAB03D (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisOVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC_mDA071F49B497F5BEA47FB7193E04836BAD026E1A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentsInChildren_TisOVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A_m5D03A208D3426756262B26B81993BB82D62D1CBA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisOVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517_m6146EEB03DE9A96D5EEC6C066EB360E747B2C297_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentsInChildren_TisOVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6_mFADE4B7CBB98CF80A59C147FAD9C274B3118265F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IOVRSkeletonDataProvider_tC7337A6B5BA23BEAF1294F2F00EAD10801B046BA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MixedRealityPlayspace_tEEDF7A044A9E8E252CF39E2F7C51AB77FC70367A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisOVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517_mCD0D2DA6EF2EE7BAF6D2611A748177C0E8894A31_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5E59A9F9DA37A05B58E7A1FCC2DE7D33725F73CA);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * V_1 = NULL;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_2 = NULL;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_3 = NULL;
	OVRControllerHelperU5BU5D_tC1041712E5487B448EA158DCEAE5D8D1F8996DD9* V_4 = NULL;
	int32_t V_5 = 0;
	OVRHandU5BU5D_tDE7CB5D0BAC901AB98623D26B8A07D4DAA25C6DF* V_6 = NULL;
	OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * V_7 = NULL;
	int32_t V_8 = 0;
	OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * V_9 = NULL;
	{
		// cameraRig = GameObject.FindObjectOfType<OVRCameraRig>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * L_0;
		L_0 = Object_FindObjectOfType_TisOVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517_mCD0D2DA6EF2EE7BAF6D2611A748177C0E8894A31(/*hidden argument*/Object_FindObjectOfType_TisOVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517_mCD0D2DA6EF2EE7BAF6D2611A748177C0E8894A31_RuntimeMethod_var);
		__this->set_cameraRig_26(L_0);
		// if (cameraRig == null)
		OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * L_1 = __this->get_cameraRig_26();
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00b9;
		}
	}
	{
		// var mainCamera = CameraCache.Main;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_3;
		L_3 = CameraCache_get_Main_m9B531633C9F0EEFE28FE56E6F9609A91E0EC3F5A(/*hidden argument*/NULL);
		V_1 = L_3;
		// var cameraRigObject = GameObject.Instantiate(SettingsProfile.OVRCameraRigPrefab);
		OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * L_4;
		L_4 = OculusXRSDKDeviceManager_get_SettingsProfile_m48D2E91D64F9DB2A7D4C48E65E0C48D426111007(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = OculusXRSDKDeviceManagerProfile_get_OVRCameraRigPrefab_m9605894F5836881F8C1D4A45CA15642470E32F1B_inline(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33(L_5, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m26431AC51B9B7A43FBABD10B4923B72B0C578F33_RuntimeMethod_var);
		V_2 = L_6;
		// cameraRig = cameraRigObject.GetComponent<OVRCameraRig>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = V_2;
		NullCheck(L_7);
		OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * L_8;
		L_8 = GameObject_GetComponent_TisOVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517_m6146EEB03DE9A96D5EEC6C066EB360E747B2C297(L_7, /*hidden argument*/GameObject_GetComponent_TisOVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517_m6146EEB03DE9A96D5EEC6C066EB360E747B2C297_RuntimeMethod_var);
		__this->set_cameraRig_26(L_8);
		// cameraRig.EnsureGameObjectIntegrity();
		OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * L_9 = __this->get_cameraRig_26();
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(12 /* System.Void OVRCameraRig::EnsureGameObjectIntegrity() */, L_9);
		// if (mainCamera != null)
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_10 = V_1;
		bool L_11;
		L_11 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_10, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00a2;
		}
	}
	{
		// GameObject prefabMainCamera = cameraRig.trackingSpace.Find("CenterEyeAnchor").gameObject;
		OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * L_12 = __this->get_cameraRig_26();
		NullCheck(L_12);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13;
		L_13 = OVRCameraRig_get_trackingSpace_mB0190AF5A79E489DB09B4C61D4E3BE8FA9AD8D9E_inline(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = Transform_Find_mB1687901A4FB0D562C44A93CC67CD35DCFCAABA1(L_13, _stringLiteral5E59A9F9DA37A05B58E7A1FCC2DE7D33725F73CA, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15;
		L_15 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		// prefabMainCamera.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16 = V_3;
		NullCheck(L_16);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_16, (bool)0, /*hidden argument*/NULL);
		// mainCamera.transform.SetParent(cameraRig.trackingSpace.transform);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_17 = V_1;
		NullCheck(L_17);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_17, /*hidden argument*/NULL);
		OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * L_19 = __this->get_cameraRig_26();
		NullCheck(L_19);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = OVRCameraRig_get_trackingSpace_mB0190AF5A79E489DB09B4C61D4E3BE8FA9AD8D9E_inline(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_21;
		L_21 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_18, L_21, /*hidden argument*/NULL);
		// mainCamera.name = prefabMainCamera.name;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_22 = V_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_23 = V_3;
		NullCheck(L_23);
		String_t* L_24;
		L_24 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_22, L_24, /*hidden argument*/NULL);
		// GameObject.Destroy(prefabMainCamera);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_25, /*hidden argument*/NULL);
	}

IL_00a2:
	{
		// cameraRig.transform.SetParent(MixedRealityPlayspace.Transform);
		OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * L_26 = __this->get_cameraRig_26();
		NullCheck(L_26);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_27;
		L_27 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MixedRealityPlayspace_tEEDF7A044A9E8E252CF39E2F7C51AB77FC70367A_il2cpp_TypeInfo_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = MixedRealityPlayspace_get_Transform_mCCE29FC5A6A15F7E4BFE9AE45BC18C9A11F6BAFA(/*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_27, L_28, /*hidden argument*/NULL);
		// }
		goto IL_00c4;
	}

IL_00b9:
	{
		// cameraRig.EnsureGameObjectIntegrity();
		OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * L_29 = __this->get_cameraRig_26();
		NullCheck(L_29);
		VirtActionInvoker0::Invoke(12 /* System.Void OVRCameraRig::EnsureGameObjectIntegrity() */, L_29);
	}

IL_00c4:
	{
		// bool useAvatarHands = SettingsProfile.RenderAvatarHandsInsteadOfController;
		OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * L_30;
		L_30 = OculusXRSDKDeviceManager_get_SettingsProfile_m48D2E91D64F9DB2A7D4C48E65E0C48D426111007(__this, /*hidden argument*/NULL);
		NullCheck(L_30);
		bool L_31;
		L_31 = OculusXRSDKDeviceManagerProfile_get_RenderAvatarHandsInsteadOfController_mF6CB8DFE7E8E4CE0E933C52DB78FE62681A05FE8_inline(L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		// foreach (var controllerHelper in cameraRig.gameObject.GetComponentsInChildren<OVRControllerHelper>())
		OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * L_32 = __this->get_cameraRig_26();
		NullCheck(L_32);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_33;
		L_33 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		OVRControllerHelperU5BU5D_tC1041712E5487B448EA158DCEAE5D8D1F8996DD9* L_34;
		L_34 = GameObject_GetComponentsInChildren_TisOVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6_mFADE4B7CBB98CF80A59C147FAD9C274B3118265F(L_33, /*hidden argument*/GameObject_GetComponentsInChildren_TisOVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6_mFADE4B7CBB98CF80A59C147FAD9C274B3118265F_RuntimeMethod_var);
		V_4 = L_34;
		V_5 = 0;
		goto IL_0100;
	}

IL_00e7:
	{
		// foreach (var controllerHelper in cameraRig.gameObject.GetComponentsInChildren<OVRControllerHelper>())
		OVRControllerHelperU5BU5D_tC1041712E5487B448EA158DCEAE5D8D1F8996DD9* L_35 = V_4;
		int32_t L_36 = V_5;
		NullCheck(L_35);
		int32_t L_37 = L_36;
		OVRControllerHelper_tD17EFBADB4AC8B80A0DA8D49801EE0472E3BF2B6 * L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		// controllerHelper.gameObject.SetActive(!useAvatarHands);
		NullCheck(L_38);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_39;
		L_39 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_38, /*hidden argument*/NULL);
		bool L_40 = V_0;
		NullCheck(L_39);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_39, (bool)((((int32_t)L_40) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_41 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_41, (int32_t)1));
	}

IL_0100:
	{
		// foreach (var controllerHelper in cameraRig.gameObject.GetComponentsInChildren<OVRControllerHelper>())
		int32_t L_42 = V_5;
		OVRControllerHelperU5BU5D_tC1041712E5487B448EA158DCEAE5D8D1F8996DD9* L_43 = V_4;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_43)->max_length))))))
		{
			goto IL_00e7;
		}
	}
	{
		// if (useAvatarHands)
		bool L_44 = V_0;
		if (!L_44)
		{
			goto IL_0127;
		}
	}
	{
		// GameObject.Instantiate(SettingsProfile.LocalAvatarPrefab, cameraRig.trackingSpace);
		OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * L_45;
		L_45 = OculusXRSDKDeviceManager_get_SettingsProfile_m48D2E91D64F9DB2A7D4C48E65E0C48D426111007(__this, /*hidden argument*/NULL);
		NullCheck(L_45);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_46;
		L_46 = OculusXRSDKDeviceManagerProfile_get_LocalAvatarPrefab_m108B0B52B77100E76CC4CF0B6CAA60CA809A40C7_inline(L_45, /*hidden argument*/NULL);
		OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * L_47 = __this->get_cameraRig_26();
		NullCheck(L_47);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_48;
		L_48 = OVRCameraRig_get_trackingSpace_mB0190AF5A79E489DB09B4C61D4E3BE8FA9AD8D9E_inline(L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_49;
		L_49 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8(L_46, L_48, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8_RuntimeMethod_var);
	}

IL_0127:
	{
		// var ovrHands = cameraRig.GetComponentsInChildren<OVRHand>();
		OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * L_50 = __this->get_cameraRig_26();
		NullCheck(L_50);
		OVRHandU5BU5D_tDE7CB5D0BAC901AB98623D26B8A07D4DAA25C6DF* L_51;
		L_51 = Component_GetComponentsInChildren_TisOVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A_m5D03A208D3426756262B26B81993BB82D62D1CBA(L_50, /*hidden argument*/Component_GetComponentsInChildren_TisOVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A_m5D03A208D3426756262B26B81993BB82D62D1CBA_RuntimeMethod_var);
		// foreach (var ovrHand in ovrHands)
		V_6 = L_51;
		V_5 = 0;
		goto IL_018f;
	}

IL_0139:
	{
		// foreach (var ovrHand in ovrHands)
		OVRHandU5BU5D_tDE7CB5D0BAC901AB98623D26B8A07D4DAA25C6DF* L_52 = V_6;
		int32_t L_53 = V_5;
		NullCheck(L_52);
		int32_t L_54 = L_53;
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		V_7 = L_55;
		// var skeletonDataProvider = ovrHand as OVRSkeleton.IOVRSkeletonDataProvider;
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_56 = V_7;
		// var skeletonType = skeletonDataProvider.GetSkeletonType();
		NullCheck(L_56);
		int32_t L_57;
		L_57 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* OVRSkeleton/SkeletonType OVRSkeleton/IOVRSkeletonDataProvider::GetSkeletonType() */, IOVRSkeletonDataProvider_tC7337A6B5BA23BEAF1294F2F00EAD10801B046BA_il2cpp_TypeInfo_var, L_56);
		V_8 = L_57;
		// var ovrSkeleton = ovrHand.GetComponent<OVRSkeleton>();
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_58 = V_7;
		NullCheck(L_58);
		OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * L_59;
		L_59 = Component_GetComponent_TisOVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC_mDA071F49B497F5BEA47FB7193E04836BAD026E1A(L_58, /*hidden argument*/Component_GetComponent_TisOVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC_mDA071F49B497F5BEA47FB7193E04836BAD026E1A_RuntimeMethod_var);
		V_9 = L_59;
		// if (ovrSkeleton == null)
		OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * L_60 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_61;
		L_61 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_60, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_61)
		{
			goto IL_0189;
		}
	}
	{
		int32_t L_62 = V_8;
		if (!L_62)
		{
			goto IL_0167;
		}
	}
	{
		int32_t L_63 = V_8;
		if ((((int32_t)L_63) == ((int32_t)1)))
		{
			goto IL_0179;
		}
	}
	{
		goto IL_0189;
	}

IL_0167:
	{
		// leftHand = ovrHand;
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_64 = V_7;
		__this->set_leftHand_29(L_64);
		// leftSkeleton = ovrSkeleton;
		OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * L_65 = V_9;
		__this->set_leftSkeleton_30(L_65);
		// break;
		goto IL_0189;
	}

IL_0179:
	{
		// rightHand = ovrHand;
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_66 = V_7;
		__this->set_rightHand_27(L_66);
		// rightSkeleton = ovrSkeleton;
		OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * L_67 = V_9;
		__this->set_rightSkeleton_28(L_67);
	}

IL_0189:
	{
		int32_t L_68 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_68, (int32_t)1));
	}

IL_018f:
	{
		// foreach (var ovrHand in ovrHands)
		int32_t L_69 = V_5;
		OVRHandU5BU5D_tDE7CB5D0BAC901AB98623D26B8A07D4DAA25C6DF* L_70 = V_6;
		NullCheck(L_70);
		if ((((int32_t)L_69) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_70)->max_length))))))
		{
			goto IL_0139;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::ConfigurePerformancePreferences()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_ConfigurePerformancePreferences_m977EEF04FE8EBB33DD433802997E9796CAD201B0 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method)
{
	{
		// SettingsProfile.ApplyConfiguredPerformanceSettings();
		OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * L_0;
		L_0 = OculusXRSDKDeviceManager_get_SettingsProfile_m48D2E91D64F9DB2A7D4C48E65E0C48D426111007(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		OculusXRSDKDeviceManagerProfile_ApplyConfiguredPerformanceSettings_m3CFB10570072E9928FC7449925A335CF849FCD72(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::UpdateHands()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_UpdateHands_m957A3B4619FDE131C5537B4F19537D802CB27989 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method)
{
	{
		// UpdateHand(rightHand, rightSkeleton, Handedness.Right);
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_0 = __this->get_rightHand_27();
		OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * L_1 = __this->get_rightSkeleton_28();
		OculusXRSDKDeviceManager_UpdateHand_m1F64190BDFAD74AA9ED0C14927C1F4D551DD51A0(__this, L_0, L_1, 2, /*hidden argument*/NULL);
		// UpdateHand(leftHand, leftSkeleton, Handedness.Left);
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_2 = __this->get_leftHand_29();
		OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * L_3 = __this->get_leftSkeleton_30();
		OculusXRSDKDeviceManager_UpdateHand_m1F64190BDFAD74AA9ED0C14927C1F4D551DD51A0(__this, L_2, L_3, 1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::UpdateHand(OVRHand,OVRSkeleton,Microsoft.MixedReality.Toolkit.Utilities.Handedness)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_UpdateHand_m1F64190BDFAD74AA9ED0C14927C1F4D551DD51A0 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * ___ovrHand0, OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * ___ovrSkeleton1, uint8_t ___handedness2, const RuntimeMethod* method)
{
	{
		// if (ovrHand.IsTracked)
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_0 = ___ovrHand0;
		NullCheck(L_0);
		bool L_1;
		L_1 = OVRHand_get_IsTracked_m1B1B95C54623252AD41089F461ACC3669CE2AE7C_inline(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		// var hand = GetOrAddHand(handedness, ovrHand);
		uint8_t L_2 = ___handedness2;
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_3 = ___ovrHand0;
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_4;
		L_4 = OculusXRSDKDeviceManager_GetOrAddHand_m6FD299BDE1329B287E6DE2A20624FAC07BD0ED07(__this, L_2, L_3, /*hidden argument*/NULL);
		// hand.UpdateController(ovrHand, ovrSkeleton, cameraRig.trackingSpace);
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_5 = ___ovrHand0;
		OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * L_6 = ___ovrSkeleton1;
		OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * L_7 = __this->get_cameraRig_26();
		NullCheck(L_7);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = OVRCameraRig_get_trackingSpace_mB0190AF5A79E489DB09B4C61D4E3BE8FA9AD8D9E_inline(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		OculusHand_UpdateController_m2DA65146BC019D090B00475D83712BC096E623F7(L_4, L_5, L_6, L_8, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0023:
	{
		// RemoveHandDevice(handedness);
		uint8_t L_9 = ___handedness2;
		OculusXRSDKDeviceManager_RemoveHandDevice_mC666E6FB87AA40E9664C85CE53FA1B0BBFA0C59B(__this, L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::GetOrAddHand(Microsoft.MixedReality.Toolkit.Utilities.Handedness,OVRHand)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * OculusXRSDKDeviceManager_GetOrAddHand_m6FD299BDE1329B287E6DE2A20624FAC07BD0ED07 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, uint8_t ___handedness0, OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * ___ovrHand1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseDataProvider_1_get_Service_mF32AD96A346149DFD23B70B4300F9D47A232E778_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mBF9F9FCB9B086F9959CD47F46FEF82DB67C01A98_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m0620BD3B7D5DE1DB5488BA648F10C4AFF085DA44_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m130BCA3A6BF1FB071A48981A972E9E42C03BB173_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Handedness_t144F166D3615E0DB2843B937FF620ED9F92680BD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMixedRealityInputSource_t64F47F8276A5C458499BF997886819249AA1EFD0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMixedRealityPointer_tDDEF9E208B33248BDF4F101FCD2C1ED7C48919CE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_get_HasValue_m7455E879CFAAE682AE3786D4D2B1F65C8AA23921_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5694A94BF0FB5DA2B661CD06A0F34DF40E5BE995);
		s_Il2CppMethodInitialized = true;
	}
	IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7* V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject* V_2 = NULL;
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * V_3 = NULL;
	int32_t V_4 = 0;
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  V_6;
	memset((&V_6), 0, sizeof(V_6));
	RuntimeObject* G_B4_0 = NULL;
	RuntimeObject* G_B3_0 = NULL;
	RuntimeObject* G_B5_0 = NULL;
	RuntimeObject* G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	RuntimeObject* G_B8_0 = NULL;
	int32_t G_B8_1 = 0;
	Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  G_B12_0;
	memset((&G_B12_0), 0, sizeof(G_B12_0));
	int32_t G_B12_1 = 0;
	IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7* G_B11_0 = NULL;
	int32_t G_B11_1 = 0;
	IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7* G_B10_0 = NULL;
	int32_t G_B10_1 = 0;
	RuntimeObject* G_B15_0 = NULL;
	RuntimeObject* G_B14_0 = NULL;
	{
		// if (trackedHands.ContainsKey(handedness))
		Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * L_0 = __this->get_trackedHands_25();
		uint8_t L_1 = ___handedness0;
		NullCheck(L_0);
		bool L_2;
		L_2 = Dictionary_2_ContainsKey_m0620BD3B7D5DE1DB5488BA648F10C4AFF085DA44(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m0620BD3B7D5DE1DB5488BA648F10C4AFF085DA44_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		// return trackedHands[handedness];
		Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * L_3 = __this->get_trackedHands_25();
		uint8_t L_4 = ___handedness0;
		NullCheck(L_3);
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_5;
		L_5 = Dictionary_2_get_Item_m130BCA3A6BF1FB071A48981A972E9E42C03BB173(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m130BCA3A6BF1FB071A48981A972E9E42C03BB173_RuntimeMethod_var);
		return L_5;
	}

IL_001b:
	{
		// var pointers = RequestPointers(SupportedControllerType.ArticulatedHand, handedness);
		uint8_t L_6 = ___handedness0;
		IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7* L_7;
		L_7 = VirtFuncInvoker2< IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7*, int32_t, uint8_t >::Invoke(40 /* Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer[] Microsoft.MixedReality.Toolkit.Input.BaseInputDeviceManager::RequestPointers(Microsoft.MixedReality.Toolkit.Input.SupportedControllerType,Microsoft.MixedReality.Toolkit.Utilities.Handedness) */, __this, ((int32_t)1024), L_6);
		V_0 = L_7;
		// var inputSourceType = InputSourceType.Hand;
		V_1 = 1;
		// var inputSource = Service?.RequestNewGenericInputSource($"Oculus Quest {handedness} Hand", pointers, inputSourceType);
		RuntimeObject* L_8;
		L_8 = BaseDataProvider_1_get_Service_mF32AD96A346149DFD23B70B4300F9D47A232E778_inline(__this, /*hidden argument*/BaseDataProvider_1_get_Service_mF32AD96A346149DFD23B70B4300F9D47A232E778_RuntimeMethod_var);
		RuntimeObject* L_9 = L_8;
		G_B3_0 = L_9;
		if (L_9)
		{
			G_B4_0 = L_9;
			goto IL_0037;
		}
	}
	{
		G_B5_0 = ((RuntimeObject*)(NULL));
		goto IL_004e;
	}

IL_0037:
	{
		uint8_t L_10 = ___handedness0;
		uint8_t L_11 = L_10;
		RuntimeObject * L_12 = Box(Handedness_t144F166D3615E0DB2843B937FF620ED9F92680BD_il2cpp_TypeInfo_var, &L_11);
		String_t* L_13;
		L_13 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral5694A94BF0FB5DA2B661CD06A0F34DF40E5BE995, L_12, /*hidden argument*/NULL);
		IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7* L_14 = V_0;
		int32_t L_15 = V_1;
		NullCheck(G_B4_0);
		RuntimeObject* L_16;
		L_16 = InterfaceFuncInvoker3< RuntimeObject*, String_t*, IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7*, int32_t >::Invoke(22 /* Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::RequestNewGenericInputSource(System.String,Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer[],Microsoft.MixedReality.Toolkit.Input.InputSourceType) */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B4_0, L_13, L_14, L_15);
		G_B5_0 = L_16;
	}

IL_004e:
	{
		V_2 = G_B5_0;
		// OculusHand handDevice = new OculusHand(TrackingState.Tracked, handedness, inputSource);
		uint8_t L_17 = ___handedness0;
		RuntimeObject* L_18 = V_2;
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_19 = (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 *)il2cpp_codegen_object_new(OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4_il2cpp_TypeInfo_var);
		OculusHand__ctor_mC784743C8DAED2DE69F8CDA1EAD5AD25ECD7C59D(L_19, 2, L_17, L_18, (MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375*)(MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375*)NULL, /*hidden argument*/NULL);
		V_3 = L_19;
		// handDevice.InitializeHand(ovrHand, SettingsProfile);
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_20 = V_3;
		OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * L_21 = ___ovrHand1;
		OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * L_22;
		L_22 = OculusXRSDKDeviceManager_get_SettingsProfile_m48D2E91D64F9DB2A7D4C48E65E0C48D426111007(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		OculusHand_InitializeHand_mF0675D6506634F5F72B12A385BC305A5E25F1ADD(L_20, L_21, L_22, /*hidden argument*/NULL);
		// for (int i = 0; i < handDevice.InputSource?.Pointers?.Length; i++)
		V_4 = 0;
		goto IL_0085;
	}

IL_006b:
	{
		// handDevice.InputSource.Pointers[i].Controller = handDevice;
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_23 = V_3;
		NullCheck(L_23);
		RuntimeObject* L_24;
		L_24 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7* L_25;
		L_25 = InterfaceFuncInvoker0< IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7* >::Invoke(0 /* Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer[] Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource::get_Pointers() */, IMixedRealityInputSource_t64F47F8276A5C458499BF997886819249AA1EFD0_il2cpp_TypeInfo_var, L_24);
		int32_t L_26 = V_4;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		RuntimeObject* L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_29 = V_3;
		NullCheck(L_28);
		InterfaceActionInvoker1< RuntimeObject* >::Invoke(1 /* System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer::set_Controller(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController) */, IMixedRealityPointer_tDDEF9E208B33248BDF4F101FCD2C1ED7C48919CE_il2cpp_TypeInfo_var, L_28, L_29);
		// for (int i = 0; i < handDevice.InputSource?.Pointers?.Length; i++)
		int32_t L_30 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
	}

IL_0085:
	{
		// for (int i = 0; i < handDevice.InputSource?.Pointers?.Length; i++)
		int32_t L_31 = V_4;
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_32 = V_3;
		NullCheck(L_32);
		RuntimeObject* L_33;
		L_33 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(L_32, /*hidden argument*/NULL);
		RuntimeObject* L_34 = L_33;
		G_B8_0 = L_34;
		G_B8_1 = L_31;
		if (L_34)
		{
			G_B9_0 = L_34;
			G_B9_1 = L_31;
			goto IL_009d;
		}
	}
	{
		il2cpp_codegen_initobj((&V_6), sizeof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 ));
		Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  L_35 = V_6;
		G_B12_0 = L_35;
		G_B12_1 = G_B8_1;
		goto IL_00b9;
	}

IL_009d:
	{
		NullCheck(G_B9_0);
		IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7* L_36;
		L_36 = InterfaceFuncInvoker0< IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7* >::Invoke(0 /* Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer[] Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource::get_Pointers() */, IMixedRealityInputSource_t64F47F8276A5C458499BF997886819249AA1EFD0_il2cpp_TypeInfo_var, G_B9_0);
		IMixedRealityPointerU5BU5D_t3C232AAF03D1FB4C24746D4A839AAA71E07F1FD7* L_37 = L_36;
		G_B10_0 = L_37;
		G_B10_1 = G_B9_1;
		if (L_37)
		{
			G_B11_0 = L_37;
			G_B11_1 = G_B9_1;
			goto IL_00b2;
		}
	}
	{
		il2cpp_codegen_initobj((&V_6), sizeof(Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 ));
		Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  L_38 = V_6;
		G_B12_0 = L_38;
		G_B12_1 = G_B10_1;
		goto IL_00b9;
	}

IL_00b2:
	{
		NullCheck(G_B11_0);
		Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103  L_39;
		memset((&L_39), 0, sizeof(L_39));
		Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184((&L_39), ((int32_t)((int32_t)(((RuntimeArray*)G_B11_0)->max_length))), /*hidden argument*/Nullable_1__ctor_m9D7EBA0DE2F89A891507EC35157C70FC4DC81184_RuntimeMethod_var);
		G_B12_0 = L_39;
		G_B12_1 = G_B11_1;
	}

IL_00b9:
	{
		V_5 = G_B12_0;
		int32_t L_40;
		L_40 = Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_inline((Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 *)(&V_5), /*hidden argument*/Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_RuntimeMethod_var);
		bool L_41;
		L_41 = Nullable_1_get_HasValue_m7455E879CFAAE682AE3786D4D2B1F65C8AA23921_inline((Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 *)(&V_5), /*hidden argument*/Nullable_1_get_HasValue_m7455E879CFAAE682AE3786D4D2B1F65C8AA23921_RuntimeMethod_var);
		if (((int32_t)((int32_t)((((int32_t)G_B12_1) < ((int32_t)L_40))? 1 : 0)&(int32_t)L_41)))
		{
			goto IL_006b;
		}
	}
	{
		// Service?.RaiseSourceDetected(handDevice.InputSource, handDevice);
		RuntimeObject* L_42;
		L_42 = BaseDataProvider_1_get_Service_mF32AD96A346149DFD23B70B4300F9D47A232E778_inline(__this, /*hidden argument*/BaseDataProvider_1_get_Service_mF32AD96A346149DFD23B70B4300F9D47A232E778_RuntimeMethod_var);
		RuntimeObject* L_43 = L_42;
		G_B14_0 = L_43;
		if (L_43)
		{
			G_B15_0 = L_43;
			goto IL_00da;
		}
	}
	{
		goto IL_00e6;
	}

IL_00da:
	{
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_44 = V_3;
		NullCheck(L_44);
		RuntimeObject* L_45;
		L_45 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(L_44, /*hidden argument*/NULL);
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_46 = V_3;
		NullCheck(G_B15_0);
		InterfaceActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(23 /* System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::RaiseSourceDetected(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController) */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B15_0, L_45, L_46);
	}

IL_00e6:
	{
		// trackedHands.Add(handedness, handDevice);
		Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * L_47 = __this->get_trackedHands_25();
		uint8_t L_48 = ___handedness0;
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_49 = V_3;
		NullCheck(L_47);
		Dictionary_2_Add_mBF9F9FCB9B086F9959CD47F46FEF82DB67C01A98(L_47, L_48, L_49, /*hidden argument*/Dictionary_2_Add_mBF9F9FCB9B086F9959CD47F46FEF82DB67C01A98_RuntimeMethod_var);
		// return handDevice;
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_50 = V_3;
		return L_50;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::RemoveHandDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_RemoveHandDevice_mC666E6FB87AA40E9664C85CE53FA1B0BBFA0C59B (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, uint8_t ___handedness0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_mE4FFA1E538E65BD642AE31936056434FE06E5306_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * V_0 = NULL;
	{
		// if (trackedHands.TryGetValue(handedness, out OculusHand hand))
		Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * L_0 = __this->get_trackedHands_25();
		uint8_t L_1 = ___handedness0;
		NullCheck(L_0);
		bool L_2;
		L_2 = Dictionary_2_TryGetValue_mE4FFA1E538E65BD642AE31936056434FE06E5306(L_0, L_1, (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_mE4FFA1E538E65BD642AE31936056434FE06E5306_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		// RemoveHandDevice(hand);
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_3 = V_0;
		OculusXRSDKDeviceManager_RemoveHandDevice_mBF1373373E760324796F80DC8B67CE9EE1DDF598(__this, L_3, /*hidden argument*/NULL);
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::RemoveAllHandDevices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_RemoveAllHandDevices_m6BF093E29DECA10A3CF59199E2CAB0031992AE55 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Clear_mA0FFB84584221F5822A86A7C30FAAC00546DE437_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Count_mB586D4F1BB640E1344BE62A6FAFC9EBFEBA058F2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Values_mC6AAA65393E9DC180F2A7E33AE230C3C0B3043CB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m3E5463E197FDA243351F081A7D6B6E56A1DF8746_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mF796BF6B5102FFD669E3C1AB8837158CE4269E47_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m6C1D5E2616E3F0772514421F05D8507D5316FCB1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mABE9872DD40A96E02EB4939FFF85A51EA450C6A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m757D094B08518BB620126F91954AE3B3F54918B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6  V_0;
	memset((&V_0), 0, sizeof(V_0));
	OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// if (trackedHands.Count == 0) return;
		Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * L_0 = __this->get_trackedHands_25();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = Dictionary_2_get_Count_mB586D4F1BB640E1344BE62A6FAFC9EBFEBA058F2(L_0, /*hidden argument*/Dictionary_2_get_Count_mB586D4F1BB640E1344BE62A6FAFC9EBFEBA058F2_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		// if (trackedHands.Count == 0) return;
		return;
	}

IL_000e:
	{
		// foreach (var hand in new List<OculusHand>(trackedHands.Values))
		Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * L_2 = __this->get_trackedHands_25();
		NullCheck(L_2);
		ValueCollection_t676F47F68B8CA40F279CA8C5502E572600166614 * L_3;
		L_3 = Dictionary_2_get_Values_mC6AAA65393E9DC180F2A7E33AE230C3C0B3043CB(L_2, /*hidden argument*/Dictionary_2_get_Values_mC6AAA65393E9DC180F2A7E33AE230C3C0B3043CB_RuntimeMethod_var);
		List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7 * L_4 = (List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7 *)il2cpp_codegen_object_new(List_1_t2645669B5FEDABFEACC9753ADCB1B39ECB6DDDF7_il2cpp_TypeInfo_var);
		List_1__ctor_m757D094B08518BB620126F91954AE3B3F54918B5(L_4, L_3, /*hidden argument*/List_1__ctor_m757D094B08518BB620126F91954AE3B3F54918B5_RuntimeMethod_var);
		NullCheck(L_4);
		Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6  L_5;
		L_5 = List_1_GetEnumerator_mABE9872DD40A96E02EB4939FFF85A51EA450C6A2(L_4, /*hidden argument*/List_1_GetEnumerator_mABE9872DD40A96E02EB4939FFF85A51EA450C6A2_RuntimeMethod_var);
		V_0 = L_5;
	}

IL_0024:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_0026:
		{
			// foreach (var hand in new List<OculusHand>(trackedHands.Values))
			OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_6;
			L_6 = Enumerator_get_Current_m6C1D5E2616E3F0772514421F05D8507D5316FCB1_inline((Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m6C1D5E2616E3F0772514421F05D8507D5316FCB1_RuntimeMethod_var);
			V_1 = L_6;
			// RemoveHandDevice(hand);
			OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_7 = V_1;
			OculusXRSDKDeviceManager_RemoveHandDevice_mBF1373373E760324796F80DC8B67CE9EE1DDF598(__this, L_7, /*hidden argument*/NULL);
		}

IL_0035:
		{
			// foreach (var hand in new List<OculusHand>(trackedHands.Values))
			bool L_8;
			L_8 = Enumerator_MoveNext_mF796BF6B5102FFD669E3C1AB8837158CE4269E47((Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mF796BF6B5102FFD669E3C1AB8837158CE4269E47_RuntimeMethod_var);
			if (L_8)
			{
				goto IL_0026;
			}
		}

IL_003e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3E5463E197FDA243351F081A7D6B6E56A1DF8746((Enumerator_t8E2F3AA476AD49179B224A1DDD7082A823F2AAA6 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m3E5463E197FDA243351F081A7D6B6E56A1DF8746_RuntimeMethod_var);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
	}

IL_004e:
	{
		// trackedHands.Clear();
		Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * L_9 = __this->get_trackedHands_25();
		NullCheck(L_9);
		Dictionary_2_Clear_mA0FFB84584221F5822A86A7C30FAAC00546DE437(L_9, /*hidden argument*/Dictionary_2_Clear_mA0FFB84584221F5822A86A7C30FAAC00546DE437_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManager::RemoveHandDevice(Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusHand)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManager_RemoveHandDevice_mBF1373373E760324796F80DC8B67CE9EE1DDF598 (OculusXRSDKDeviceManager_t431444DAD3ADB55082098421BEDF564ADE9A487C * __this, OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * ___handDevice0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Remove_m533F4D4B5A32EB3E23F19ADD7C3FB48775D61675_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* G_B4_0 = NULL;
	RuntimeObject* G_B3_0 = NULL;
	{
		// if (handDevice == null) return;
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_0 = ___handDevice0;
		if (L_0)
		{
			goto IL_0004;
		}
	}
	{
		// if (handDevice == null) return;
		return;
	}

IL_0004:
	{
		// CoreServices.InputSystem?.RaiseSourceLost(handDevice.InputSource, handDevice);
		IL2CPP_RUNTIME_CLASS_INIT(CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		RuntimeObject* L_1;
		L_1 = CoreServices_get_InputSystem_m03F66B157660C21D67B5A6A4D132BF889CBFE440(/*hidden argument*/NULL);
		RuntimeObject* L_2 = L_1;
		G_B3_0 = L_2;
		if (L_2)
		{
			G_B4_0 = L_2;
			goto IL_000f;
		}
	}
	{
		goto IL_001b;
	}

IL_000f:
	{
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_3 = ___handDevice0;
		NullCheck(L_3);
		RuntimeObject* L_4;
		L_4 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(L_3, /*hidden argument*/NULL);
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_5 = ___handDevice0;
		NullCheck(G_B4_0);
		InterfaceActionInvoker2< RuntimeObject*, RuntimeObject* >::Invoke(24 /* System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::RaiseSourceLost(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController) */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B4_0, L_4, L_5);
	}

IL_001b:
	{
		// trackedHands.Remove(handDevice.ControllerHandedness);
		Dictionary_2_t56977068E00DC8BD1C1459B14AAB01170F466E11 * L_6 = __this->get_trackedHands_25();
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_7 = ___handDevice0;
		NullCheck(L_7);
		uint8_t L_8;
		L_8 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_9;
		L_9 = Dictionary_2_Remove_m533F4D4B5A32EB3E23F19ADD7C3FB48775D61675(L_6, L_8, /*hidden argument*/Dictionary_2_Remove_m533F4D4B5A32EB3E23F19ADD7C3FB48775D61675_RuntimeMethod_var);
		// RecyclePointers(handDevice.InputSource);
		OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * L_10 = ___handDevice0;
		NullCheck(L_10);
		RuntimeObject* L_11;
		L_11 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(L_10, /*hidden argument*/NULL);
		VirtActionInvoker1< RuntimeObject* >::Invoke(41 /* System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputDeviceManager::RecyclePointers(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource) */, __this, L_11);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_OVRCameraRigPrefab()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * OculusXRSDKDeviceManagerProfile_get_OVRCameraRigPrefab_m9605894F5836881F8C1D4A45CA15642470E32F1B (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// get { return ovrCameraRigPrefab; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_ovrCameraRigPrefab_5();
		return L_0;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::set_OVRCameraRigPrefab(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_set_OVRCameraRigPrefab_m789F03D98FFBE52885FED3616FA4FC3D529A715F (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___value0, const RuntimeMethod* method)
{
	{
		// set { ovrCameraRigPrefab = value; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___value0;
		__this->set_ovrCameraRigPrefab_5(L_0);
		// set { ovrCameraRigPrefab = value; }
		return;
	}
}
// System.Boolean Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_RenderAvatarHandsInsteadOfController()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OculusXRSDKDeviceManagerProfile_get_RenderAvatarHandsInsteadOfController_mF6CB8DFE7E8E4CE0E933C52DB78FE62681A05FE8 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// public bool RenderAvatarHandsInsteadOfController => renderAvatarHandsInsteadOfControllers;
		bool L_0 = __this->get_renderAvatarHandsInsteadOfControllers_6();
		return L_0;
	}
}
// UnityEngine.GameObject Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_LocalAvatarPrefab()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * OculusXRSDKDeviceManagerProfile_get_LocalAvatarPrefab_m108B0B52B77100E76CC4CF0B6CAA60CA809A40C7 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// get { return localAvatarPrefab; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_localAvatarPrefab_7();
		return L_0;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::set_LocalAvatarPrefab(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_set_LocalAvatarPrefab_m177FA3915621E769889EE7297AB2D57EEF737291 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___value0, const RuntimeMethod* method)
{
	{
		// set { localAvatarPrefab = value; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___value0;
		__this->set_localAvatarPrefab_7(L_0);
		// set { localAvatarPrefab = value; }
		return;
	}
}
// OVRHand/TrackingConfidence Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_MinimumHandConfidence()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManagerProfile_get_MinimumHandConfidence_m219D821F8CEA93A8D563B5F88F46C5E3A2BFD20D (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// get => minimumHandConfidence;
		int32_t L_0 = __this->get_minimumHandConfidence_8();
		return L_0;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::set_MinimumHandConfidence(OVRHand/TrackingConfidence)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_set_MinimumHandConfidence_m21CFBF34E57482B8E828C4D3A862FA5799655A40 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// set => minimumHandConfidence = value;
		int32_t L_0 = ___value0;
		__this->set_minimumHandConfidence_8(L_0);
		return;
	}
}
// OVRHand/TrackingConfidence Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_CurrentLeftHandTrackingConfidence()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManagerProfile_get_CurrentLeftHandTrackingConfidence_m81C836C0BE65E512ACDACDA074670BB870653E5C (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// public OVRHand.TrackingConfidence CurrentLeftHandTrackingConfidence { get; set; }
		int32_t L_0 = __this->get_U3CCurrentLeftHandTrackingConfidenceU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::set_CurrentLeftHandTrackingConfidence(OVRHand/TrackingConfidence)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_set_CurrentLeftHandTrackingConfidence_m36B081D88A939B6F1D83987B46ACFD4C5AE1F10D (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public OVRHand.TrackingConfidence CurrentLeftHandTrackingConfidence { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentLeftHandTrackingConfidenceU3Ek__BackingField_9(L_0);
		return;
	}
}
// OVRHand/TrackingConfidence Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_CurrentRightHandTrackingConfidence()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManagerProfile_get_CurrentRightHandTrackingConfidence_m1C59DAFAA13B20393A28FF4EFF56AD98E7A77AD2 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// public OVRHand.TrackingConfidence CurrentRightHandTrackingConfidence { get; set; }
		int32_t L_0 = __this->get_U3CCurrentRightHandTrackingConfidenceU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::set_CurrentRightHandTrackingConfidence(OVRHand/TrackingConfidence)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_set_CurrentRightHandTrackingConfidence_mDEE11B9CDA4CE2DD9C1262D2DCBC72CF04CAD57B (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public OVRHand.TrackingConfidence CurrentRightHandTrackingConfidence { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentRightHandTrackingConfidenceU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Single Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_LowConfidenceTimeThreshold()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float OculusXRSDKDeviceManagerProfile_get_LowConfidenceTimeThreshold_mA6197D5CFC9C78F61EB6B040A43E17277A26A743 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// get => lowConfidenceTimeThreshold;
		float L_0 = __this->get_lowConfidenceTimeThreshold_11();
		return L_0;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::set_LowConfidenceTimeThreshold(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_set_LowConfidenceTimeThreshold_m9928A38EB0D7D54892C22B48E4CCDA59BC796952 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// set => lowConfidenceTimeThreshold = value;
		float L_0 = ___value0;
		__this->set_lowConfidenceTimeThreshold_11(L_0);
		return;
	}
}
// System.Int32 Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_CPULevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManagerProfile_get_CPULevel_m0D2BC7B50B1D2BD9D65F926DD0FD082A991C2C21 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// get => defaultCpuLevel;
		int32_t L_0 = __this->get_defaultCpuLevel_12();
		return L_0;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::set_CPULevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_set_CPULevel_m6376B863E8D0075DA4E8DAE950102F0167E66666 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// defaultCpuLevel = value;
		int32_t L_0 = ___value0;
		__this->set_defaultCpuLevel_12(L_0);
		// ApplyConfiguredPerformanceSettings();
		OculusXRSDKDeviceManagerProfile_ApplyConfiguredPerformanceSettings_m3CFB10570072E9928FC7449925A335CF849FCD72(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Int32 Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::get_GPULevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManagerProfile_get_GPULevel_mC49905ABE184F2D0EE61F625F0406A416D11393E (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// get => defaultGpuLevel;
		int32_t L_0 = __this->get_defaultGpuLevel_13();
		return L_0;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::set_GPULevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_set_GPULevel_mD60CDA491B2A2BAC84576B0CA135F095FC046D32 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// defaultGpuLevel = value;
		int32_t L_0 = ___value0;
		__this->set_defaultGpuLevel_13(L_0);
		// ApplyConfiguredPerformanceSettings();
		OculusXRSDKDeviceManagerProfile_ApplyConfiguredPerformanceSettings_m3CFB10570072E9928FC7449925A335CF849FCD72(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::ApplyConfiguredPerformanceSettings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_ApplyConfiguredPerformanceSettings_m3CFB10570072E9928FC7449925A335CF849FCD72 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OVRManager_t6088CD544814E5A64EEB830D130A6816E79D0036_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// XRSettings.eyeTextureResolutionScale = resolutionScale;
		float L_0 = __this->get_resolutionScale_14();
		XRSettings_set_eyeTextureResolutionScale_mBABE036EB59B20AF02A26771F34BB35453C806D2(L_0, /*hidden argument*/NULL);
		// OVRManager.cpuLevel = CPULevel;
		int32_t L_1;
		L_1 = OculusXRSDKDeviceManagerProfile_get_CPULevel_m0D2BC7B50B1D2BD9D65F926DD0FD082A991C2C21_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(OVRManager_t6088CD544814E5A64EEB830D130A6816E79D0036_il2cpp_TypeInfo_var);
		OVRManager_set_cpuLevel_m732E7AD28186E2026DFF38D4BDA99D61D836C1AB(L_1, /*hidden argument*/NULL);
		// OVRManager.gpuLevel = GPULevel;
		int32_t L_2;
		L_2 = OculusXRSDKDeviceManagerProfile_get_GPULevel_mC49905ABE184F2D0EE61F625F0406A416D11393E_inline(__this, /*hidden argument*/NULL);
		OVRManager_set_gpuLevel_mED660E0DACAFFA9412BA1D4FD3ACEA0472A45CC5(L_2, /*hidden argument*/NULL);
		// if (OVRManager.fixedFoveatedRenderingSupported)
		bool L_3;
		L_3 = OVRManager_get_fixedFoveatedRenderingSupported_mECE4C8001356DD64815603FF7BDDD2B093F61D21(/*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		// OVRManager.fixedFoveatedRenderingLevel = fixedFoveatedRenderingLevel;
		int32_t L_4 = __this->get_fixedFoveatedRenderingLevel_16();
		IL2CPP_RUNTIME_CLASS_INIT(OVRManager_t6088CD544814E5A64EEB830D130A6816E79D0036_il2cpp_TypeInfo_var);
		OVRManager_set_fixedFoveatedRenderingLevel_mD408E6CA2F65DB79BE618CA27E7AB19A8227A3C5(L_4, /*hidden argument*/NULL);
		// OVRManager.useDynamicFixedFoveatedRendering = useDynamicFixedFoveatedRendering;
		bool L_5 = __this->get_useDynamicFixedFoveatedRendering_15();
		OVRManager_set_useDynamicFixedFoveatedRendering_m6F1C8157EA92866C6CE9C412A1F14E946494C2FC(L_5, /*hidden argument*/NULL);
	}

IL_003e:
	{
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKDeviceManagerProfile::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile__ctor_m7D844E8E3A5F8A5E898DE6A004FC53C020F53392 (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// private bool renderAvatarHandsInsteadOfControllers = true;
		__this->set_renderAvatarHandsInsteadOfControllers_6((bool)1);
		// private float lowConfidenceTimeThreshold = 0.2f;
		__this->set_lowConfidenceTimeThreshold_11((0.200000003f));
		// private int defaultCpuLevel = 2;
		__this->set_defaultCpuLevel_12(2);
		// private int defaultGpuLevel = 2;
		__this->set_defaultGpuLevel_13(2);
		// float resolutionScale = 1.25f;
		__this->set_resolutionScale_14((1.25f));
		// bool useDynamicFixedFoveatedRendering = true;
		__this->set_useDynamicFixedFoveatedRendering_15((bool)1);
		// OVRManager.FixedFoveatedRenderingLevel fixedFoveatedRenderingLevel = OVRManager.FixedFoveatedRenderingLevel.High;
		__this->set_fixedFoveatedRenderingLevel_16(3);
		BaseMixedRealityProfile__ctor_m508EE2B2B8F00BE219CEAF7B02813DC1F9C7F45F(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKTouchController::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKTouchController__ctor_m24201B666BB933900BA9A3171944DFF853B90401 (OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED * __this, int32_t ___trackingState0, uint8_t ___controllerHandedness1, RuntimeObject* ___inputSource2, MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* ___interactions3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OculusTouchControllerDefinition_t9C81F5CDBD5908C5D1CE58FB90A4375457C7F831_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// : base(trackingState, controllerHandedness, inputSource, interactions, new OculusTouchControllerDefinition(controllerHandedness))
		int32_t L_0 = ___trackingState0;
		uint8_t L_1 = ___controllerHandedness1;
		RuntimeObject* L_2 = ___inputSource2;
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_3 = ___interactions3;
		uint8_t L_4 = ___controllerHandedness1;
		OculusTouchControllerDefinition_t9C81F5CDBD5908C5D1CE58FB90A4375457C7F831 * L_5 = (OculusTouchControllerDefinition_t9C81F5CDBD5908C5D1CE58FB90A4375457C7F831 *)il2cpp_codegen_object_new(OculusTouchControllerDefinition_t9C81F5CDBD5908C5D1CE58FB90A4375457C7F831_il2cpp_TypeInfo_var);
		OculusTouchControllerDefinition__ctor_mF17722C0CC08F770C1A128FEF0373DCA3B6538FA(L_5, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GenericXRSDKController_t37204ED3C514A275F5EFAEFAE842F0819A392CA4_il2cpp_TypeInfo_var);
		GenericXRSDKController__ctor_m377D0A746594421F94E21527260B09F6789AFE1F(__this, L_0, L_1, L_2, L_3, L_5, /*hidden argument*/NULL);
		// { }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKTouchController::UpdateButtonData(Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping,UnityEngine.XR.InputDevice)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKTouchController_UpdateButtonData_m43A72EEAEFF14694FF828772049C5C69E1312C2A (OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED * __this, MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * ___interactionMapping0, InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  ___inputDevice1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  V_1;
	memset((&V_1), 0, sizeof(V_1));
	InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  V_2;
	memset((&V_2), 0, sizeof(V_2));
	bool V_3 = false;
	int32_t V_4 = 0;
	bool V_5 = false;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 5> __leave_targets;
	RuntimeObject* G_B16_0 = NULL;
	RuntimeObject* G_B15_0 = NULL;
	RuntimeObject* G_B19_0 = NULL;
	RuntimeObject* G_B18_0 = NULL;
	{
		// using (UpdateButtonDataPerfMarker.Auto())
		IL2CPP_RUNTIME_CLASS_INIT(OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_il2cpp_TypeInfo_var);
		ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  L_0 = ((OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_StaticFields*)il2cpp_codegen_static_fields_for(OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_il2cpp_TypeInfo_var))->get_UpdateButtonDataPerfMarker_21();
		V_1 = L_0;
		AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D  L_1;
		L_1 = ProfilerMarker_Auto_m136C2D8B4232A186FA4C8866BB389A5DCF69E0E5_inline((ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 *)(&V_1), /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			// bool usingOculusButtonData = false;
			V_3 = (bool)0;
			// switch (interactionMapping.InputType)
			MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_2 = ___interactionMapping0;
			NullCheck(L_2);
			int32_t L_3;
			L_3 = MixedRealityInteractionMapping_get_InputType_mF467F74CF7AAA7B1B3C3EB6BE950BEABA6F1A435_inline(L_2, /*hidden argument*/NULL);
			V_4 = L_3;
			int32_t L_4 = V_4;
			if ((((int32_t)L_4) == ((int32_t)((int32_t)11))))
			{
				goto IL_002e;
			}
		}

IL_001e:
		{
			int32_t L_5 = V_4;
			if ((((int32_t)L_5) == ((int32_t)((int32_t)12))))
			{
				goto IL_0038;
			}
		}

IL_0024:
		{
			int32_t L_6 = V_4;
			if ((!(((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)((int32_t)30)))) > ((uint32_t)1))))
			{
				goto IL_0042;
			}
		}

IL_002c:
		{
			goto IL_004a;
		}

IL_002e:
		{
			// buttonUsage = OculusUsages.indexTouch;
			IL2CPP_RUNTIME_CLASS_INIT(OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_il2cpp_TypeInfo_var);
			InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  L_7 = ((OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_StaticFields*)il2cpp_codegen_static_fields_for(OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_il2cpp_TypeInfo_var))->get_indexTouch_3();
			V_2 = L_7;
			// usingOculusButtonData = true;
			V_3 = (bool)1;
			// break;
			goto IL_004a;
		}

IL_0038:
		{
			// buttonUsage = OculusUsages.indexTouch;
			IL2CPP_RUNTIME_CLASS_INIT(OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_il2cpp_TypeInfo_var);
			InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  L_8 = ((OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_StaticFields*)il2cpp_codegen_static_fields_for(OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_il2cpp_TypeInfo_var))->get_indexTouch_3();
			V_2 = L_8;
			// usingOculusButtonData = true;
			V_3 = (bool)1;
			// break;
			goto IL_004a;
		}

IL_0042:
		{
			// buttonUsage = OculusUsages.thumbrest;
			IL2CPP_RUNTIME_CLASS_INIT(OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_il2cpp_TypeInfo_var);
			InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  L_9 = ((OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_StaticFields*)il2cpp_codegen_static_fields_for(OculusUsages_t0C51A60521CCF58352C176EEB791B2ED9F729B77_il2cpp_TypeInfo_var))->get_thumbrest_2();
			V_2 = L_9;
			// usingOculusButtonData = true;
			V_3 = (bool)1;
		}

IL_004a:
		{
			// if (!usingOculusButtonData)
			bool L_10 = V_3;
			if (L_10)
			{
				goto IL_0057;
			}
		}

IL_004d:
		{
			// base.UpdateButtonData(interactionMapping, inputDevice);
			MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_11 = ___interactionMapping0;
			InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E  L_12 = ___inputDevice1;
			GenericXRSDKController_UpdateButtonData_m00755CCC7473D162B8594CA0AECD67CB29E06121(__this, L_11, L_12, /*hidden argument*/NULL);
			// }
			IL2CPP_LEAVE(0xD1, FINALLY_00c3);
		}

IL_0057:
		{
			// if (inputDevice.TryGetFeatureValue(buttonUsage, out bool buttonPressed))
			InputFeatureUsage_1_t28793BE3C4ACB9F1B34C0C392EAAFB16A5FA8E40  L_13 = V_2;
			bool L_14;
			L_14 = InputDevice_TryGetFeatureValue_mF70AB4F4BC703E9242A757D0A6D84A4DA48BBBBE((InputDevice_t69B790C68145C769BA3819DE33AA94155C77207E *)(&___inputDevice1), L_13, (bool*)(&V_5), /*hidden argument*/NULL);
			if (!L_14)
			{
				goto IL_006b;
			}
		}

IL_0063:
		{
			// interactionMapping.BoolData = buttonPressed;
			MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_15 = ___interactionMapping0;
			bool L_16 = V_5;
			NullCheck(L_15);
			MixedRealityInteractionMapping_set_BoolData_mB9E1F1E8CC1CCC3ADFDFB08A564FA528A6399068(L_15, L_16, /*hidden argument*/NULL);
		}

IL_006b:
		{
			// if (interactionMapping.Changed)
			MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_17 = ___interactionMapping0;
			NullCheck(L_17);
			bool L_18;
			L_18 = MixedRealityInteractionMapping_get_Changed_mA0C90E795316CFE4F8E911F7371D6E47A433FEDF(L_17, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_00c1;
			}
		}

IL_0073:
		{
			// if (interactionMapping.BoolData)
			MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_19 = ___interactionMapping0;
			NullCheck(L_19);
			bool L_20;
			L_20 = MixedRealityInteractionMapping_get_BoolData_m002148D894B01A0998785DE1E8149B433174A79A_inline(L_19, /*hidden argument*/NULL);
			if (!L_20)
			{
				goto IL_009f;
			}
		}

IL_007b:
		{
			// CoreServices.InputSystem?.RaiseOnInputDown(InputSource, ControllerHandedness, interactionMapping.MixedRealityInputAction);
			IL2CPP_RUNTIME_CLASS_INIT(CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
			RuntimeObject* L_21;
			L_21 = CoreServices_get_InputSystem_m03F66B157660C21D67B5A6A4D132BF889CBFE440(/*hidden argument*/NULL);
			RuntimeObject* L_22 = L_21;
			G_B15_0 = L_22;
			if (L_22)
			{
				G_B16_0 = L_22;
				goto IL_0086;
			}
		}

IL_0083:
		{
			IL2CPP_LEAVE(0xD1, FINALLY_00c3);
		}

IL_0086:
		{
			RuntimeObject* L_23;
			L_23 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(__this, /*hidden argument*/NULL);
			uint8_t L_24;
			L_24 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
			MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_25 = ___interactionMapping0;
			NullCheck(L_25);
			MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  L_26;
			L_26 = MixedRealityInteractionMapping_get_MixedRealityInputAction_m65770DAB10749381CC2E06AE4F4D4756AA65424A_inline(L_25, /*hidden argument*/NULL);
			NullCheck(G_B16_0);
			InterfaceActionInvoker3< RuntimeObject*, uint8_t, MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  >::Invoke(38 /* System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::RaiseOnInputDown(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction) */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B16_0, L_23, L_24, L_26);
			// }
			IL2CPP_LEAVE(0xD1, FINALLY_00c3);
		}

IL_009f:
		{
			// CoreServices.InputSystem?.RaiseOnInputUp(InputSource, ControllerHandedness, interactionMapping.MixedRealityInputAction);
			IL2CPP_RUNTIME_CLASS_INIT(CoreServices_tA111E2393BEC7A3AF2703B80A556166363998BD2_il2cpp_TypeInfo_var);
			RuntimeObject* L_27;
			L_27 = CoreServices_get_InputSystem_m03F66B157660C21D67B5A6A4D132BF889CBFE440(/*hidden argument*/NULL);
			RuntimeObject* L_28 = L_27;
			G_B18_0 = L_28;
			if (L_28)
			{
				G_B19_0 = L_28;
				goto IL_00aa;
			}
		}

IL_00a7:
		{
			IL2CPP_LEAVE(0xD1, FINALLY_00c3);
		}

IL_00aa:
		{
			RuntimeObject* L_29;
			L_29 = BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline(__this, /*hidden argument*/NULL);
			uint8_t L_30;
			L_30 = BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline(__this, /*hidden argument*/NULL);
			MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * L_31 = ___interactionMapping0;
			NullCheck(L_31);
			MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  L_32;
			L_32 = MixedRealityInteractionMapping_get_MixedRealityInputAction_m65770DAB10749381CC2E06AE4F4D4756AA65424A_inline(L_31, /*hidden argument*/NULL);
			NullCheck(G_B19_0);
			InterfaceActionInvoker3< RuntimeObject*, uint8_t, MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  >::Invoke(39 /* System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem::RaiseOnInputUp(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction) */, IMixedRealityInputSystem_t30FEE3BCBA4B47D9BBED63BF481D811EC6A39AA1_il2cpp_TypeInfo_var, G_B19_0, L_29, L_30, L_32);
		}

IL_00c1:
		{
			// }
			IL2CPP_LEAVE(0xD1, FINALLY_00c3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00c3;
	}

FINALLY_00c3:
	{ // begin finally (depth: 1)
		AutoScope_Dispose_m5CDDCDA2B8769738BB695661EC4AC55DD7A0D7CA_inline((AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(195)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(195)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xD1, IL_00d1)
	}

IL_00d1:
	{
		// }
		return;
	}
}
// System.Void Microsoft.MixedReality.Toolkit.XRSDK.Oculus.Input.OculusXRSDKTouchController::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OculusXRSDKTouchController__cctor_mB855902D813092C27690CE4CE04CA589CE7DDEDD (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA8ABEE8078E902ABD0AEA109AB9441E0A0845281);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly ProfilerMarker UpdateButtonDataPerfMarker = new ProfilerMarker("[MRTK] OculusXRSDKController.UpdateButtonData");
		ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  L_0;
		memset((&L_0), 0, sizeof(L_0));
		ProfilerMarker__ctor_mCE8D10CF2D2B2C4E51BF1BB66D75FDDE5BDA4A41_inline((&L_0), _stringLiteralA8ABEE8078E902ABD0AEA109AB9441E0A0845281, /*hidden argument*/NULL);
		((OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_StaticFields*)il2cpp_codegen_static_fields_for(OculusXRSDKTouchController_tB126D07D200D9463FBFC60C9DC45441E13E8B4ED_il2cpp_TypeInfo_var))->set_UpdateButtonDataPerfMarker_21(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  MixedRealityPose_get_ZeroIdentity_m0668ACB9F881F2046B81685B02D13BFD78338998_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static MixedRealityPose ZeroIdentity { get; } = new MixedRealityPose(Vector3.zero, Quaternion.identity);
		IL2CPP_RUNTIME_CLASS_INIT(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669_il2cpp_TypeInfo_var);
		MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669  L_0 = ((MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669_StaticFields*)il2cpp_codegen_static_fields_for(MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669_il2cpp_TypeInfo_var))->get_U3CZeroIdentityU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 * MixedRealityInputSystemProfile_get_HandTrackingProfile_m68AB045F05D5B7E5723E8D3331E837CAC058CE7A_inline (MixedRealityInputSystemProfile_tD94F4B1680EFF39B21E58E438A8EDE6FF8B8BA0C * __this, const RuntimeMethod* method)
{
	{
		// get { return handTrackingProfile; }
		MixedRealityHandTrackingProfile_t28838B85DF712221FF796BFD4430C1C880A0A1C8 * L_0 = __this->get_handTrackingProfile_19();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool BaseController_get_Enabled_m23BE305FD0E3643AB6B26D80F0F0FBB33CF285D4_inline (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, const RuntimeMethod* method)
{
	{
		// public bool Enabled { get; set; }
		bool L_0 = __this->get_U3CEnabledU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void BaseController_set_IsRotationAvailable_m36835958007BE5F83FC443D7837E00F38D31E7D8_inline (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool IsRotationAvailable { get; protected set; }
		bool L_0 = ___value0;
		__this->set_U3CIsRotationAvailableU3Ek__BackingField_8(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void BaseController_set_IsPositionAvailable_m8762BC7C636F247BED1DB49EE4509475DA072F44_inline (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool IsPositionAvailable { get; protected set; }
		bool L_0 = ___value0;
		__this->set_U3CIsPositionAvailableU3Ek__BackingField_6(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * OVRHand_get_PointerPose_m5FD5EE4BC344C1F3D2F85B1F8C6E3D0E518DF7A2_inline (OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * __this, const RuntimeMethod* method)
{
	{
		// public Transform PointerPose { get; private set; }
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CPointerPoseU3Ek__BackingField_13();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void MixedRealityPose_set_Position_m119B5345F1415E482F83D6BD4E7B1F4951B7375A_inline (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector3 Position { get { return position; } set { position = value; } }
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___value0;
		__this->set_position_1(L_0);
		// public Vector3 Position { get { return position; } set { position = value; } }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void MixedRealityPose_set_Rotation_mF14187710EDE143DFC85AB2A0892FD72B793DF77_inline (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method)
{
	{
		// public Quaternion Rotation { get { return rotation; } set { rotation = value; } }
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_0 = ___value0;
		__this->set_rotation_2(L_0);
		// public Quaternion Rotation { get { return rotation; } set { rotation = value; } }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* BaseController_get_InputSource_mFE386AF6AF9E016E2E6F68BEAD3BD51ABAD61751_inline (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, const RuntimeMethod* method)
{
	{
		// public IMixedRealityInputSource InputSource { get; }
		RuntimeObject* L_0 = __this->get_U3CInputSourceU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* BaseController_get_Interactions_m1865C8E2491F04B496A1B3E888B2B71F223DA051_inline (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, const RuntimeMethod* method)
{
	{
		// public MixedRealityInteractionMapping[] Interactions { get; private set; } = null;
		MixedRealityInteractionMappingU5BU5D_tDEFCF792459FF55032737511E9CCBFAAB77E5375* L_0 = __this->get_U3CInteractionsU3Ek__BackingField_9();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t MixedRealityInteractionMapping_get_InputType_mF467F74CF7AAA7B1B3C3EB6BE950BEABA6F1A435_inline (MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * __this, const RuntimeMethod* method)
{
	{
		// public DeviceInputType InputType => inputType;
		int32_t L_0 = __this->get_inputType_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint8_t BaseController_get_ControllerHandedness_m9CAEC9EDBC6D4F92D184B26774ABBBE778CBA93A_inline (BaseController_t838F2F1E5179222D8747EBA84A91579901943F76 * __this, const RuntimeMethod* method)
{
	{
		// public Handedness ControllerHandedness { get; }
		uint8_t L_0 = __this->get_U3CControllerHandednessU3Ek__BackingField_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  MixedRealityInteractionMapping_get_MixedRealityInputAction_m65770DAB10749381CC2E06AE4F4D4756AA65424A_inline (MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * __this, const RuntimeMethod* method)
{
	{
		// get { return inputAction; }
		MixedRealityInputAction_tA2C94A418FF769BB00CC4E392DCE470FE1E050DE  L_0 = __this->get_inputAction_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool OculusHand_get_IsPinching_m1DEFD901E5E5FE10261629E06E5941C2DEF0F707_inline (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method)
{
	{
		// protected bool IsPinching { set; get; }
		bool L_0 = __this->get_U3CIsPinchingU3Ek__BackingField_28();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool OculusHand_get_IsGrabbing_m461FECC082EDF20D3F788A5754B7231DC93EEA0A_inline (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, const RuntimeMethod* method)
{
	{
		// protected bool IsGrabbing { set; get; }
		bool L_0 = __this->get_U3CIsGrabbingU3Ek__BackingField_29();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool MixedRealityInteractionMapping_get_BoolData_m002148D894B01A0998785DE1E8149B433174A79A_inline (MixedRealityInteractionMapping_tCF94ECD1BF3836BD8B40D0779E1B80E5F80B6505 * __this, const RuntimeMethod* method)
{
	{
		// return boolData;
		bool L_0 = __this->get_boolData_12();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool OVRHand_get_IsTracked_m1B1B95C54623252AD41089F461ACC3669CE2AE7C_inline (OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * __this, const RuntimeMethod* method)
{
	{
		// public bool IsTracked { get; private set; }
		bool L_0 = __this->get_U3CIsTrackedU3Ek__BackingField_10();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t OVRHand_get_HandConfidence_mE06ACA8BA1C4D00BA5F196AC3CA3DCD46C9801FE_inline (OVRHand_tC05A35B82243C7F008BE75311F52746E27D1566A * __this, const RuntimeMethod* method)
{
	{
		// public TrackingConfidence HandConfidence { get; private set; }
		int32_t L_0 = __this->get_U3CHandConfidenceU3Ek__BackingField_15();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManagerProfile_get_MinimumHandConfidence_m219D821F8CEA93A8D563B5F88F46C5E3A2BFD20D_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// get => minimumHandConfidence;
		int32_t L_0 = __this->get_minimumHandConfidence_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float OculusXRSDKDeviceManagerProfile_get_LowConfidenceTimeThreshold_mA6197D5CFC9C78F61EB6B040A43E17277A26A743_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// get => lowConfidenceTimeThreshold;
		float L_0 = __this->get_lowConfidenceTimeThreshold_11();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_set_CurrentLeftHandTrackingConfidence_m36B081D88A939B6F1D83987B46ACFD4C5AE1F10D_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public OVRHand.TrackingConfidence CurrentLeftHandTrackingConfidence { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentLeftHandTrackingConfidenceU3Ek__BackingField_9(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OculusXRSDKDeviceManagerProfile_set_CurrentRightHandTrackingConfidence_mDEE11B9CDA4CE2DD9C1262D2DCBC72CF04CAD57B_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public OVRHand.TrackingConfidence CurrentRightHandTrackingConfidence { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CCurrentRightHandTrackingConfidenceU3Ek__BackingField_10(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* OVRSkeleton_get_Bones_mC8D26C6EF0253FBC0EF8B350EEBBE84C97045C0F_inline (OVRSkeleton_tE727DD0833515B8FCF6E7DE4BD16859BE26404EC * __this, const RuntimeMethod* method)
{
	{
		// public IList<OVRBone> Bones { get; protected set; }
		RuntimeObject* L_0 = __this->get_U3CBonesU3Ek__BackingField_20();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OculusHand_set_IsPinching_mAAEFC3C08B379B80AF3411AC98531581C90B1B4A_inline (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// protected bool IsPinching { set; get; }
		bool L_0 = ___value0;
		__this->set_U3CIsPinchingU3Ek__BackingField_28(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OculusHand_set_IsGrabbing_mD4B8A213D51B07938ED3B39C75D37BC0BF5C6872_inline (OculusHand_tECE2FE0C7DA58AF542021BDA6768ABA9F61BDFD4 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// protected bool IsGrabbing { set; get; }
		bool L_0 = ___value0;
		__this->set_U3CIsGrabbingU3Ek__BackingField_29(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  MixedRealityPose_get_Position_m34E0E9859576E00CEB1CC3D1D31F18C45B1712A3_inline (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * __this, const RuntimeMethod* method)
{
	{
		// public Vector3 Position { get { return position; } set { position = value; } }
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = __this->get_position_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t OVRBone_get_Id_m60B032C7EB5D597C9F5D3D7911A241F31EE8E12E_inline (OVRBone_tF9C031559703607035E455CE642549C722BBF081 * __this, const RuntimeMethod* method)
{
	{
		// public OVRSkeleton.BoneId Id { get; set; }
		int32_t L_0 = __this->get_U3CIdU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * OVRBone_get_Transform_m0E8C60FF2CF31336D654B3319EFFB04BDC964FCB_inline (OVRBone_tF9C031559703607035E455CE642549C722BBF081 * __this, const RuntimeMethod* method)
{
	{
		// public Transform Transform { get; set; }
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CTransformU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___b1;
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_x_2();
		float L_8 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___a0;
		float L_10 = L_9.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___b1;
		float L_12 = L_11.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = ___a0;
		float L_14 = L_13.get_y_3();
		float L_15 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = ___a0;
		float L_17 = L_16.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = ___b1;
		float L_19 = L_18.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = ___a0;
		float L_21 = L_20.get_z_4();
		float L_22 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), ((float)il2cpp_codegen_add((float)L_17, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_21)), (float)L_22)))), /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0053;
	}

IL_0053:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		return L_24;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  MixedRealityPose_get_Rotation_m884C69734F7CD693912ECF2ACC36904CB18BE907_inline (MixedRealityPose_t7089584858BB76148AA1F83EB73B973E93554669 * __this, const RuntimeMethod* method)
{
	{
		// public Quaternion Rotation { get { return rotation; } set { rotation = value; } }
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_0 = __this->get_rotation_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * OculusXRSDKDeviceManagerProfile_get_OVRCameraRigPrefab_m9605894F5836881F8C1D4A45CA15642470E32F1B_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// get { return ovrCameraRigPrefab; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_ovrCameraRigPrefab_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * OVRCameraRig_get_trackingSpace_mB0190AF5A79E489DB09B4C61D4E3BE8FA9AD8D9E_inline (OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * __this, const RuntimeMethod* method)
{
	{
		// public Transform trackingSpace { get; private set; }
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_U3CtrackingSpaceU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool OculusXRSDKDeviceManagerProfile_get_RenderAvatarHandsInsteadOfController_mF6CB8DFE7E8E4CE0E933C52DB78FE62681A05FE8_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// public bool RenderAvatarHandsInsteadOfController => renderAvatarHandsInsteadOfControllers;
		bool L_0 = __this->get_renderAvatarHandsInsteadOfControllers_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * OculusXRSDKDeviceManagerProfile_get_LocalAvatarPrefab_m108B0B52B77100E76CC4CF0B6CAA60CA809A40C7_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// get { return localAvatarPrefab; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_localAvatarPrefab_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManagerProfile_get_CPULevel_m0D2BC7B50B1D2BD9D65F926DD0FD082A991C2C21_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// get => defaultCpuLevel;
		int32_t L_0 = __this->get_defaultCpuLevel_12();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t OculusXRSDKDeviceManagerProfile_get_GPULevel_mC49905ABE184F2D0EE61F625F0406A416D11393E_inline (OculusXRSDKDeviceManagerProfile_t28CBC9BDB27453EEFD8864203048A7EB93CDAFC7 * __this, const RuntimeMethod* method)
{
	{
		// get => defaultGpuLevel;
		int32_t L_0 = __this->get_defaultGpuLevel_13();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D  ProfilerMarker_Auto_m136C2D8B4232A186FA4C8866BB389A5DCF69E0E5_inline (ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * __this, const RuntimeMethod* method)
{
	AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		intptr_t L_0 = __this->get_m_Ptr_0();
		AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D  L_1;
		memset((&L_1), 0, sizeof(L_1));
		AutoScope__ctor_m4131730A501F687FF95B2963EABAC7844C6B9859_inline((&L_1), (intptr_t)L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D  L_2 = V_0;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AutoScope_Dispose_m5CDDCDA2B8769738BB695661EC4AC55DD7A0D7CA_inline (AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D * __this, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = __this->get_m_Ptr_0();
		ProfilerUnsafeUtility_EndSample_m0435B2EE7963614F3D154A83D44269FE4D1A85B0((intptr_t)L_0, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ProfilerMarker__ctor_mCE8D10CF2D2B2C4E51BF1BB66D75FDDE5BDA4A41_inline (ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		intptr_t L_1;
		L_1 = ProfilerUnsafeUtility_CreateMarker_m419027084C68545B765B9345949D8BFCB96C51AD(L_0, (uint16_t)1, 0, 0, /*hidden argument*/NULL);
		__this->set_m_Ptr_0((intptr_t)L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Nullable_1_GetValueOrDefault_mB4CE6E77EC85DD762FDA6C24F96EBC2A75E28546_gshared_inline (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_0();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m7455E879CFAAE682AE3786D4D2B1F65C8AA23921_gshared_inline (Nullable_1_t864FD0051A05D37F91C857AB496BFCB3FE756103 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return (bool)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m52F33C6963C9A76A14F54B4B2C30CEF580DB1EB7_gshared_inline (Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return (bool)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_GetValueOrDefault_mBECC58FEFD1088EC070D9F9A892ECD1D8BBF2A0F_gshared_inline (Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_value_0();
		return (bool)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * BaseDataProvider_1_get_Service_m041BFCBFB9FCF6AD8183DE3BE1736CEAA9048863_gshared_inline (BaseDataProvider_1_t664C3F8898C3B28F4040012FA554BEA276D3A25C * __this, const RuntimeMethod* method)
{
	{
		// protected T Service { get; set; } = default(T);
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U3CServiceU3Ek__BackingField_9();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AutoScope__ctor_m4131730A501F687FF95B2963EABAC7844C6B9859_inline (AutoScope_tEB00834B4CEE8558238837BA3A36B64020E48F8D * __this, intptr_t ___markerPtr0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___markerPtr0;
		__this->set_m_Ptr_0((intptr_t)L_0);
		intptr_t L_1 = ___markerPtr0;
		ProfilerUnsafeUtility_BeginSample_m1B2CAD1BC7C7C390514317A8D51FB798D4622AE4((intptr_t)L_1, /*hidden argument*/NULL);
		return;
	}
}
